<?php
require_once 'views/top.php';
?>
</head>
<body>
<div class="super_container">
	<!-- Header -->
	<header class="header trans_300">
		<!-- Top Navigation -->
		<?php
                    require_once 'views/top_nav.php';
                   // <!-- Main Navigation -->
                    require_once 'views/main_nav.php';
                ?>
	</header>
	<div class="fs_menu_overlay"></div>
	<?php
                require_once 'views/mobile_view.php';
        ?>


	<?php
                //slider
                require_once 'views/slider.php';
                //Banner
                require_once 'views/banner.php';
                //New Arrivals
                require_once 'views/new_arrivals.php';
                ?>
        <hr>
        <?php
                //Popular products
                require_once 'views/popular_products.php';
                //Deal of the week
                require_once 'views/weekly_deal.php';
                //Best Sellers
                require_once 'views/brands.php';
                require_once 'views/news_letter.php';
                require_once 'views/footer.php';
                
        ?>
<!--<div class="social-icon">
	<a href="https://web.facebook.com/" class="facebook" target="_blank"><i class="fa fa-facebook"></i></a>
	<a href="https://web.twitter.com/" class="twitter" target="_blank"><i class="fa fa-twitter"></i></a>
	<a href="https://web.google.com/" class="google" target="_blank"><i class="fa fa-google"></i></a>
	<a href="https://web.youtube.com/" class="youtube" target="_blank"><i class="fa fa-youtube"></i></a>
	<a href="https://web.linkedin.com/" class="linkedin" target="_blank"><i class="fa fa-linkedin"></i></a>
</div>-->
	
	

	
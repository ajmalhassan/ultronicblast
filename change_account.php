<?php
require_once 'views/top.php';
require_once 'models/product.php';
require_once 'models/user.php';
$obj_user->profile();
?>


<body>

<div class="super_container">
	<!-- Header -->
	<header class="header trans_300">
		<!-- Top Navigation -->
		<?php
                    require_once 'views/top_nav.php';
                   // <!-- Main Navigation -->
                    require_once 'views/main_nav.php';
                ?>
	</header>

	<div class="fs_menu_overlay"></div>
        <br><br><br><br><br><br><br><br>
<!--	<div class="hamburger_menu">-->
<?php
                require_once 'views/mobile_view.php';
        ?>
<div class="container">
    <div class="row">
        <?php
            require_once 'views/account_left_sidebar.php';
        ?>
<div class="col-md-9 "  id="contact_detail">
    <form action="controller/process_change_account.php" method="post" enctype="multipart/form-data">
    <div class="row">
        <div class="col-md-offset-1 col-md-8">
            <!--<h3 style="color: red;">Create account</h3>-->
            <h5 class="error-color">
                <?php
                    if(isset($_SESSION['msg']))
                    {
                        $msg = $_SESSION['msg'];
                        echo($msg);
                        unset($_SESSION['msg']);
                    }
                    if(isset($_SESSION['errors']))
                    {
                        $errors = $_SESSION['errors'];
                        unset($_SESSION['errors']);
                    }
                ?>
            </h5>
            <h4 style="border-bottom: 1px solid #ddd;">Edit account</h4>
           <div class="row">
               
            <div class="col-md-6 form-group">
               <label>First Name</label>
               <input type="text" name="first_name" value="<?php echo($obj_user->first_name);?>" class="form-control control-form focusedInput" placeholder="First Name">
              <span>
                  <?php
                    if(isset($errors['first_name']))
                    {
                        echo ($errors['first_name']);
                    }
                  ?>
              </span>
            </div>
            <div class="col-md-6 form-group">
               <label>Last Name</label>
               <input type="text" name="last_name" value="<?php echo($obj_user->last_name);?>" class="form-control control-form focusedInput" placeholder="Last Name">
              <span>
                  <?php
                  if(isset($errors['last_name']))
                  {
                      echo ($errors['last_name']);
                  }
                  ?>
              </span>
            </div>
            <div class="col-md-12 form-group">
              <label>User Name</label>
              <input type="text" name="user_name" value="<?php echo($obj_user->user_name);?>" class="form-control control-form focusedInput" placeholder="User Name">
              <span>
                  <?php
                  if(isset($errors['user_name']))
                  {
                      echo ($errors['user_name']);
                  }
                  ?>
              </span>
            </div>
              
            <div class="col-md-12 form-group">
                <label>Email  </label>&nbsp;&nbsp;
                <span class="text-danger"> <?php echo($obj_user->email);?></span>
               
            </div>
                <div class="col-md-12 form-group">
                <label>Mobile Number</label>
                <input type="text" name="mobile_number" value="<?php echo($obj_user->mobile_number);?>" class="form-control control-form focusedInput" placeholder="+92">
                <span>
                  <?php
                  if(isset($errors['mobile_number']))
                  {
                      echo ($errors['mobile_number']);
                  }
                  ?>
              </span>
            </div>
               <div class="col-md-6 form-group form-group" style="margin-bottom: 0;">
            <label for="exampleInputFile"> </label>
            <!--<input type="file" name="profile_image" value="" id="exampleInputFile">-->
            <input type="file" id="profile_image" name="profile_image" >
            <span>
                  <?php
                  if(isset($errors['profile_image']))
                  {
                      echo ($errors['profile_image']);
                  }
                  ?>
              </span>
            <p class="help-block">Only allow .png,.jpeg image</p>
            </div>
            <div class="col-md-6 form-group">
                
            </div>
            <div class="col-md-6 form-group">
                <input type="submit" class="btn btn-sm btn-danger" value="Save">
                
            </div>
        </div>
        </div>   
    </div>
  
</form>
                    
        </div>
  
    </div>
   
</div>
	

       <?php
                require_once 'views/news_letter.php';
                require_once 'views/footer.php';
        
	

	

	
<?php
require_once 'views/top.php';
require_once 'models/Cart.php';
require_once 'models/product.php';
?>
</head>

<body>

<div class="super_container">

	<!-- Header -->

	<header class="header trans_300">

		<!-- Top Navigation -->
                <?php
                    require_once 'views/top_nav.php';
                   // <!-- Main Navigation -->
                    require_once 'views/main_nav.php';
                ?>

	</header>

	<div class="fs_menu_overlay"></div>

	<!-- Hamburger Menu -->

	<?php
    require_once 'views/mobile_view.php';
?>

	<div class="container product_section_container">
		<div class="row">
			<div class="col product_section clearfix">

				<!-- Breadcrumbs -->

				<div class="breadcrumbs d-flex flex-row align-items-center">
					<ul>
						<li><a href="index.html">Home</a></li>
						<li class="active"><a href="index.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Men's</a></li>
					</ul>
				</div>

				<!-- Sidebar -->

<div class="sidebar">
<div class="sidebar_section">
        <div class="sidebar_title">
          <h5>Categories</h5>
        </div>
        <ul class="sidebar_categories ">
            <?php
            
            $categories = Category::get_cat();
            foreach ($categories as $category)
            {
                echo("
                      <li><a href='".BASE_URL."categories.php?category_id=$category->categoryID'>$category->category_name</a></li>
                     ");
            }
            ?>
        </ul>
<!--     <div class="show_more">
       <span><span>+</span>Show More</span>
     </div>-->
</div>
    <div class="sidebar_section">
        <div class="sidebar_title">
          <h5>Brands</h5>
        </div>
        <ul class="sidebar_categories checkboxes">
            <?php
            $brands = Brand::get_bra();
            foreach ($brands as $brand)
            {
                echo("
                      <li><a href='".BASE_URL."brands.php?brand_id=$brand->brandID'>$brand->brand_name</a></li>
                    ");
            }
            ?>
        </ul>
       <div class="show_more">
       <span><span>+</span>Show More</span>
     </div>
        
</div>

</div>

				<!-- Main Content -->

<div class="main_content">

<!-- Products -->

<div class="products_iso">
    <div class="row">
            <div class="col">

                    <!-- Product Sorting -->
                    <div class="product_sorting_container product_sorting_container_top">
                            <ul class="product_sorting">
                                    <li>
                                            <span class="type_sorting_text">View Products</span>
                                            <i class="fa fa-angle-down"></i>
                                            <ul class="sorting_type">
<!--                                                <li class="type_sorting_btn" data-isotope-option='{ "sortBy": "original-order" }'><span>Default Sorting</span></li>
                                                    <li class="type_sorting_btn" data-isotope-option='{ "sortBy": "price" }'><span>Price</span></li>
                                                    <li class="type_sorting_btn" data-isotope-option='{ "sortBy": "name" }'><span>Product Name</span></li>-->
                                                <li class="type_sorting_btn" data-isotope-option='{ "sortBy": "price" }'><a href='<?php echo(BASE_URL);?>shop.php'><span>Default Sorting</span></a></li>
                                                <li class="type_sorting_btn" data-isotope-option='{ "sortBy": "price" }'><a href='<?php echo(BASE_URL);?>shop.php?type=new'><span>New Products</span></a></li>
                                            </ul>
                                    </li>
                                    <li>
                                            <span>Show</span>
                                            <span class="num_sorting_text">6</span>
                                            <i class="fa fa-angle-down"></i>
                                            <ul class="sorting_num">
                                                    <li class="num_sorting_btn"><span>6</span></li>
                                                    <li class="num_sorting_btn"><span>12</span></li>
                                                    <li class="num_sorting_btn"><span>24</span></li>
                                            </ul>
                                    </li>
                            </ul>
<!--                            <div class="pages d-flex flex-row align-items-center">
                                    <div class="page_current">
                                            <span>1</span>
                                            <ul class="page_selection">
                                                    <li><a href="#">1</a></li>
                                                    <li><a href="#">2</a></li>
                                                    <li><a href="#">3</a></li>
                                            </ul>
                                    </div>
                                    <div class="page_total"><span>of</span> 3</div>
                                    <div id="next_page" class="page_next"><a href="#"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></div>
                            </div>-->

                    </div>

                    <!-- Product Grid -->

<div class="product-grid">

                            <!-- Products -->
<?php
    try {
        $type = isset($_GET['type']) ? $_GET['type']:"all";
        $start = isset($_GET['start']) ? $_GET['start'] : 0;
        $count = isset($_GET['count']) ? $_GET['count'] : ITEM_PER_PAGE;
        $products = product::get_sorting_products($start, $count, $type);
        
        foreach ($products as $p)
        {
            $product_path = pathinfo($p->product_image);
            $product_image_name=$product_path['filename'];
            ?>
       <div class='product-item men'>
    <div class='product discount product_filter' style='border-right: 2px solid red;'>
        <div class='product_image'>
        <a href="<?php echo(BASE_URL.'detail.php?product_id='.$p->productID);?>" target='_blank'>
            <img src="<?php echo(BASE_URL.'admin/products/'.$product_image_name.'/'.$p->product_image);?>" alt='$p->product_name'>
                </a>
        </div>
        <div class='favorite favorite_left'></div>
        <div class='product_bubble product_bubble_right product_bubble_red d-flex flex-column align-items-center'><span><?php echo($p->product_discount);?>%off</span></div>
        <div class='product_info'>
            <h6 class='product_name'><a href="<?php echo(BASE_URL.'detail.php?product_id='.$p->productID);?>" target='_blank'><?php echo($p->product_name);?></a></h6>
            <div class='product_price'>Rs.<?php echo($p->sale_price);?><span>Rs.<?php echo($p->unit_price);?></span></div>
        </div>
    </div>
    
    <div class='red_button add_to_cart_button  ' style='margin-left:0px;width:100%;'>
        <form action="<?php echo(BASE_URL.'controller/process_cart.php');?>" method='post'>
            <input type='hidden' name='action' value='add_to_cart'/>
            <input type='hidden' name='product_id' value="<?php echo($p->productID);?>"/>
            <input class='cart_button' type='submit' value='add to cart'/>    
        </form>
    </div>
</div> 
 ");
       <?php
       }
        ?>
         </div>
    <div class="ub-pagination">
    <?php
   $pNums = product::pagination(ITEM_PER_PAGE);
    foreach ($pNums as $pNo=>$start)
    {
       echo("<a href='" . BASE_URL . "shop.php?start=$start&type=$type'>$pNo</a>");
    }
    } catch (Exception $ex) {
        echo $ex->getMessage();
    } 
        ?>
    </div>
            </div>
    </div>
</div>
</div>
			</div>
		</div>
	</div>

	<!-- Newsletter -->

	<div class="newsletter">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<div class="newsletter_text d-flex flex-column justify-content-center align-items-lg-start align-items-md-center text-center">
						<h4>Newsletter</h4>
						<p>Subscribe to our newsletter and get 20% off your first purchase</p>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="newsletter_form d-flex flex-md-row flex-column flex-xs-column align-items-center justify-content-lg-end justify-content-center">
						<input id="newsletter_email" type="email" placeholder="Your email" required="required" data-error="Valid email is required.">
						<button id="newsletter_submit" type="submit" class="newsletter_submit_btn trans_300" value="Submit">subscribe</button>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Footer -->
    <?php
    require_once 'views/footer.php';
   



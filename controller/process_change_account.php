<?php
session_start();
require_once '../models/user.php';
//$obj_user = new User();
$obj_user = unserialize($_SESSION['obj_user']);
//    echo("<pre>");
//    print_r($_FILES);
//    echo("</pre>");
//    die;
$errors = array();
try {
    $obj_user->first_name = $_POST['first_name'];
} catch (Exception $ex) {
    $errors['first_name'] = $ex->getMessage();
}
try {
    $obj_user->last_name = $_POST['last_name'];
} catch (Exception $ex) {
    $errors['last_name'] = $ex->getMessage();
}
try {
    $obj_user->user_name = $_POST['user_name'];
} catch (Exception $ex) {
    $errors['user_name'] = $ex->getMessage();
}
try {
    $obj_user->mobile_number = $_POST['mobile_number'];
} catch (Exception $ex) {
    $errors['mobile_number'] = $ex->getMessage();
}
try {
    $obj_user->profile_image = $_FILES['profile_image'];
} catch (Exception $ex) {
    $errors['profile_image'] = $ex->getMessage();
}

if(count($errors) == 0)
{
    try {
        $obj_user->update_account();
        $msg = "Account Updated";
        $_SESSION['msg'] = $msg;
        $obj_user->upload_profile_image($_FILES['profile_image']['tmp_name']);
        header("Location:../my_account.php");
    } catch (Exception $ex) {
        $_SESSION['msg'] = $ex->getMessage();
       header("Location:../change_account.php");
    }
}
else
{
    $msg ="*Check your Errors";
    $_SESSION['msg'] = $msg;
    $_SESSION['errors'] = $errors;
    header("Location:../change_account.php");
}
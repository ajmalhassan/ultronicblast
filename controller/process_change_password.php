<?php
session_start();
require_once '../models/user.php';
$obj_user = unserialize($_SESSION['obj_user']);
$errors = array();
try {
    $obj_user->password = $_POST['password'];
} catch (Exception $ex) {
    $errors['password'] = $ex->getMessage();
}
try {
    $obj_user->verify_retype_password($_POST['password'], $_POST['password2']);
} catch (Exception $ex) {
    $errors['retype_password'] = $ex->getMessage();
}

//try {
//    $obj_user->profile_image = $_POST['profile_image'];
//} catch (Exception $ex) {
//    $errors['profile_image'] = $ex->getMessage();
//}

if(count($errors) == 0)
{
    try {
        $obj_user->update_password();
        $msg = "Change Password";
        $_SESSION['msg'] = $msg;
         header("Location:../my_account.php");
    } catch (Exception $ex) {
        $_SESSION['msg'] = $ex->getMessage();
       header("Location:../change_password.php");
    }
}
else
{
    $msg ="*Check your Errors";
    $_SESSION['msg'] = $msg;
    $_SESSION['errors'] = $errors;
    header("Location:../change_password.php");
}
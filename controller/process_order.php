<?php
session_start();
require_once '../models/Cart.php';
require_once '../models/user.php';
require_once '../models/ContactPerson.php';
require_once '../models/Order.php';

$obj_user = unserialize($_SESSION['obj_user']);
$obj_cart = unserialize($_SESSION['obj_cart']);
//$obj_user = new User();
$obj_contact = new ContactPerson();

$errors = array();
try {
    $obj_contact->user_name = $_POST['user_name'];
} catch (Exception $ex) {
   $errors['user_name'] = $ex->getMessage();
}
try {
    $obj_contact->email = $_POST['email'];
} catch (Exception $ex) {
    $errors['email'] = $ex->getMessage();
}
try {
    $obj_contact->mobile_number = $_POST['mobile_number'];
} catch (Exception $ex) {
    $errors['mobile_number'] = $ex->getMessage();
}
try {
    $obj_contact->address = $_POST['shipping_address'];
} catch (Exception $ex) {
    $errors['shipping_address'] = $ex->getMessage();
}

if(count($errors)==0)
{
    try {
        Order::place_order($obj_user, $obj_contact, $obj_cart);
        $msg = "<h3>Order Status</h3>
                <p>Your order has submitted successfully.</p>";
        
        $_SESSION['msg'] = $msg;
        header("Location:../msg.php");
    } catch (Exception $ex) {
        $_SESSION['msg'] = $ex->getMessage();
        header("Location:../check_out.php");
    }

}
else
{
    $msg = "*Check Your Errors";
    $_SESSION['msg'] = $msg;
    $_SESSION['errors'] = $errors;
    $_SESSION['obj_contact'] = serialize($obj_contact);
    header("Location:../check_out.php");
}
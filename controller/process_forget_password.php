<?php
session_start();
require_once '../models/user.php';

$obj_user = new User();

$errors = array();
try {
    $obj_user->user_name = $_POST['user_name'];
} catch (Exception $ex) {
    $errors['user_name'] = $ex->getMessage();
}
try {
    $obj_user->email = $_POST['email'];
} catch (Exception $ex) {
    $errors['email'] = $ex->getMessage();
}
try {
    $obj_user->password = $_POST['new_password'];
} catch (Exception $ex) {
    $errors['new_password'] = $ex->getMessage();
}
try {
    $obj_user->verify_retype_password($_POST['new_password'], $_POST['password2']);
} catch (Exception $ex) {
    $errors['retype_password'] = $ex->getMessage();
}

if(count($errors) == 0)
{
    
    try {
        $obj_user->update_password();
        $msg = "Password Updated";
        $_SESSION['msg'] = $msg;
         header("Location:../forgetpassword.php");
    } catch (Exception $ex) {
        $_SESSION['msg'] = $ex->getMessage();
       header("Location:../forgetpassword.php");
    }
}
else
{
    $msg ="Password has not been Updated";
    $_SESSION['msg'] = $msg;
    $_SESSION['errors'] = $errors;
    $_SESSION['obj_user'] = serialize($obj_user);
    header("Location:../forgetpassword.php");
}

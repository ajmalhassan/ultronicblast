<?php
session_start();
require_once '../models/user.php';
$obj_user = unserialize($_SESSION['obj_user']);
try {
    $obj_user->logout();
    $_SESSION['msg'] = "You have logged out";
} catch (Exception $ex) {
    $_SESSION['msg'] = $ex->getMessage();
}
header("Location:../index.php");



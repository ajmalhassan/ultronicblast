<?php
session_start();
require_once '../models/Cart.php';
if(isset($_SESSION['obj_cart']))
{
    $obj_cart = unserialize($_SESSION['obj_cart']);
}
else
{
    $obj_cart = new Cart();
}
if(isset($_POST['action']))
{
    switch ($_POST['action'])
    {
        case "add_to_cart":
            $product_id = isset($_POST['product_id']) ? $_POST['product_id'] : 0;
            $quantity = isset($_POST['qunatity']) ? $_POST['qunatity'] : 1;
            $item = new Item($product_id, $quantity);
            $obj_cart->add_to_cart($item);
            break;
        case "update_cart":

            $obj_cart->update_cart($_POST['qtys']);
            break;
            
    }
}
 else if($_GET['action']) 
     {
    switch ($_GET['action'])
    {
        case "remove_item":
             $product_id = isset($_GET['product_id']) ? $_GET['product_id'] : 0;
            $item = new Item($product_id);

            $obj_cart->remove_item($item);
            break;
        case "empty_cart":
            $obj_cart->empty_cart();
            break;
    }
    
}
$_SESSION['obj_cart'] = serialize($obj_cart);

header("Location:" . $_SERVER['HTTP_REFERER']);




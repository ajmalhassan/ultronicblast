<?php
session_start();
require_once '../models/user.php';
$obj_user = new User();
$errors = array();
try {
    $obj_user->first_name = $_POST['first_name'];
} catch (Exception $ex) {
    $errors['first_name'] = $ex->getMessage();
}
try {
    $obj_user->last_name = $_POST['last_name'];
} catch (Exception $ex) {
    $errors['last_name'] = $ex->getMessage();
}
try {
    $obj_user->user_name = $_POST['user_name'];
} catch (Exception $ex) {
    $errors['user_name'] = $ex->getMessage();
}
try {
    $obj_user->mobile_number = $_POST['mobile_number'];
} catch (Exception $ex) {
    $errors['mobile_number'] = $ex->getMessage();
}
try {
    $obj_user->email = $_POST['email'];
} catch (Exception $ex) {
    $errors['email'] = $ex->getMessage();
}
try {
    $obj_user->password = $_POST['password'];
} catch (Exception $ex) {
    $errors['password'] = $ex->getMessage();
}

try {
    $obj_user->address = $_POST['address'];
} catch (Exception $ex) {
    $errors['address'] = $ex->getMessage();
}
try {
    $obj_user->verify_retype_password($_POST['password'], $_POST['password2']);
} catch (Exception $ex) {
    $errors['retype_password'] = $ex->getMessage();
}
try {
    $obj_user->profile_image = $_FILES['profile_image'];
} catch (Exception $ex) {
    $errors['profile_image'] = $ex->getMessage();
}
try {
    $obj_user->check_user($_POST['email']);
} catch (Exception $ex) {
    echo $errors['check_email'] = $ex->getMessage();
}

if(count($errors) == 0)
{
    try {
        
        //$msg = "Successfully created account";
        
        $act_code = md5(crypt(rand(),'aa'));
        $obj_user->add_user($act_code);
        $msg_email ="Click to <a href='http://localhost:8080/ultronicblast/activate.php?act_code=$act_code&user_id=$obj_user->user_id'>ACTIVATE</a>";
        $obj_user->upload_profile_image($_FILES['profile_image']['tmp_name']);
        $_SESSION['msg_email'] = $msg_email;
        $_SESSION['msg'] = $msg;
        header("Location:../msg.php");
    } catch (Exception $ex) {
        $_SESSION['msg'] = $ex->getMessage();
       header("Location:../msg.php");
    }
}
else
{
    $msg ="*Check your Errors";
    $_SESSION['msg'] = $msg;
    $_SESSION['errors'] = $errors;
    $_SESSION['obj_user'] = serialize($obj_user);
    header("Location:../register.php");
}
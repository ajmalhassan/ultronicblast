<?php
require_once 'views/top.php';
require_once 'models/Cart.php';
require_once 'models/user.php';
?>
</head>

<body>

<div class="super_container">
	<!-- Header -->
	<header class="header trans_300">
		<!-- Top Navigation -->
		<?php
                    require_once 'views/top_nav.php';
                   // <!-- Main Navigation -->
                    require_once 'views/main_nav.php';
                ?>
	</header>
	<div class="fs_menu_overlay" ></div>
        <br><br><br><br><br><br><br><br><br>
        <div class="row">
        <div class="col-xs-12 col-md-6">
            <h3>
                <center>Shopping Cart</center>
            </h3>
            <?php
                if($obj_cart->items)
                {
                    echo("<form action='".BASE_URL."controller/process_cart.php'>"
                            . "<table class='table table-responsive-sm table-condensed'>");
                    echo("<tr>"
                            . "<th>X</th>"
                            . "<th>Item Name</th>"
                            . "<th>View Details</th>"
                            . "<th>Unit Price</th>"
                            . "<th>Quantity</th>"
                            . "<th>Total</th>"
                            . "</tr>");
                            foreach ($obj_cart->items as $item)
                            {
                                echo("<tr>"
                                . "<td><a href='" . BASE_URL . "controller/process_cart.php?action=remove_item&product_id=$item->item_id'><i class='fa fa-trash-o'></i></a></td>"
                                . "<td>$item->name</td>"
                                . "<td><a href='" . BASE_URL . "detail.php?product_id=$item->item_id' target='_blank'>View Details</a></td>"
                                . "<td>Rs $item->Unit_Price</td>"
                                . "<td><input type='text' value='$item->Quantity' name='qtys[$item->item_id]'></td>"
                                . "<td>Rs $item->Total</td>"
                                . "</tr>");
                            }
                            echo("<tr>"
                            . "<th><a class='btn btn-sm btn-danger' href='" . BASE_URL . "shop.php'>Shop More</a></th>"
                            . "<th><a class='btn btn-sm btn-danger' href='" . BASE_URL . "controller/process_cart.php?action=empty_cart'>Empty Cart</a></th>"
                            . "<th>"
                            . "<input type='hidden' name='action' value='update_cart'>"
                            . "<input class='btn btn-sm btn-danger' type='submit' value='Update Cart'>"
                            . "</th>"
                            . "<th><a class='btn btn-sm btn-danger' href='" . BASE_URL . "check_out.php'>Check Out</a></th>"
                            . "<th>TOTAL</th>"
                            . "<th>RS.$obj_cart->total</th>"
                            . "</tr>");
    
                            echo("</table></form>");
                }
                else
                {
                    echo("Your cart is empty");
                }
            ?>
                     
        </div>
        </div>

	<?php
                require_once 'views/news_letter.php';
                require_once 'views/footer.php';
                
        
	
	

	
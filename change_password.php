<?php
require_once 'views/top.php';
require_once 'models/product.php';
require_once 'models/user.php';
?>


<body>

<div class="super_container">
	<!-- Header -->
	<header class="header trans_300">
		<!-- Top Navigation -->
		<?php
                    require_once 'views/top_nav.php';
                   // <!-- Main Navigation -->
                    require_once 'views/main_nav.php';
                ?>
	</header>

	<div class="fs_menu_overlay"></div>
        <br><br><br><br><br><br><br><br>
<!--	<div class="hamburger_menu">-->
<?php
                require_once 'views/mobile_view.php';
        ?>
<div class="container">
    <div class="row">
        <div class="col-md-2 custom-menu ">
            <h5 class="border-bottom" style="border-bottom:1px solid #ddd">My Account</h5>
           <ul class="list-unstyled text-uppercase">
               <li><a id="account_setting" href="my_account.php">Account Setting</a></li>
               <li><a id="personal_info" href="change_account.php">Personal Information</a></li>
               <li><a id="my_orders" href="#">My Orders</a></li>
           </ul>
        </div>
<div class="col-md-9 "  id="contact_detail">
    <form action="controller/process_change_password.php" method="post" enctype="multipart/form-data">
    <div class="row">
        <div class="col-md-offset-1 col-md-8">
            <!--<h3 style="color: red;">Create account</h3>-->
            <h5 class="error-color">
                <?php
                $obj_user->profile();
                    if(isset($_SESSION['msg']))
                    {
                        $msg = $_SESSION['msg'];
                        echo($msg);
                        unset($_SESSION['msg']);
                    }
                    if(isset($_SESSION['errors']))
                    {
                        $errors = $_SESSION['errors'];
                        unset($_SESSION['errors']);
                    }
                ?>
            </h5>
            <h4 style="border-bottom: 1px solid #ddd;">Edit Password</h4>
           <div class="row">
               
            <div class="col-md-6 form-group">
               <label>New Password</label>
               <input type="password" name="password" value="" class="form-control control-form focusedInput" placeholder="New Password">
               <span class="error-color">
                  <?php
                    if(isset($errors['password']))
                    {
                        echo ($errors['password']);
                    }
                  ?>
              </span>
            </div>
               <div class="col-md-6"></div>
            <div class="col-md-6 form-group">
               <label>Confirm Password</label>
               <input type="password" name="password2" value="" class="form-control control-form focusedInput" placeholder="Retype Password">
               <span class="error-color">
                  <?php
                  if(isset($errors['retype_password']))
                  {
                      echo ($errors['retype_password']);
                  }
                  ?>
              </span>
            </div>
           <div class="col-md-6"></div>
            <div class="col-md-6 form-group">
                <input type="submit" class="btn btn-sm btn-danger" value="Save">
                
            </div>
        </div>
        </div>   
    </div>
  
</form>
                    
        </div>
  
    </div>
   
</div>
	

       <?php
                require_once 'views/news_letter.php';
                require_once 'views/footer.php';
        
	

	

	
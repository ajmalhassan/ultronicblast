<?php
require_once 'db_connection.php';
class User extends DB_Connection {
    protected $user_id;
    protected $first_name;
    protected $last_name;
    protected $user_name;
    protected $email;
    protected $password;
    protected $mobile_number;
    protected $address;
    protected $profile_image;
    protected $login_status;


    //Constructor
    public function __construct() {
        
    }
    
    //Setter
    public function __set($name, $value) {
        $method_name = "set_$name";
        if(!method_exists($this, $method_name))
        {
            throw new Exception("SET Property $name does not exist");
        }
        $this->$method_name($value);
    }
    
    //Getter
    public function __get($name) {
        $method_name = "get_$name";
        if(!method_exists($this, $method_name))
        {
            throw new Exception("GET Property of $name does not exist");
        }
        return $this->$method_name();
    }
    private function set_user_id($user_id)
    {
        if(!is_numeric($user_id) || $user_id <= 0)
        {
            throw new Exception("User ID must be Numaric");
        }
        $this->user_id = $user_id;
    }
    private function get_user_id()
    {
        return $this->user_id;
    }
    private function set_first_name($first_name)
    {
        $reg = "/^[a-z ][A-Z0-9 ]+$/i";
        if(!preg_match($reg, $first_name))
        {
            throw new Exception("Invalid First Name");
        }
        $this->first_name = $first_name;
    }
    private function get_first_name()
    {
        return $this->first_name;
    }
    private function set_last_name($last_name)
    {
         $reg = "/^[a-z ][A-Z0-9 ]+$/i";
         if(!preg_match($reg, $last_name))
         {
             throw new Exception("Invalid Last Name");
         }
         $this->last_name = $last_name;
    }
    private function get_last_name()
    {
        return $this->last_name;
    }

    private function set_user_name($user_name)
    {
        $reg = "/^[a-z ][A-Z0-9 ]+$/i";
        if(!preg_match($reg, $user_name))
        {
            throw new Exception("Invalid User Name");
        }
        $this->user_name = $user_name;
    }
    private function get_user_name()
    {
        return $this->user_name;
    }
    
    private function set_email($email)
    {
        $reg = "/^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zAZ]\.)+[a-zA-Z]{2,4})$/";
        if(!preg_match($reg, $email))
        {
            throw new Exception("Invalid Email");
        }
        $this->email = $email;
    }
    private function get_email()
    {
        return $this->email;
    }
    private function set_password($password)
    {
        $reg = "/^[a-z0-9 ][A-Z0-9 ]+$/i";
        if(!preg_match($reg, $password))
        {
            throw new Exception("Invalid Password");
        }
        $this->password = sha1($password);
    }
    private function get_password()
    {
        return $this->password;
    }
    private function set_mobile_number($mobile_number)
    {
        $reg = "/^((\+92))-{0,1}\d{3}-{0,1}\d{7}$|^\d{11}$|^\d{4}-\d{7}$/";
        if(!preg_match($reg, $mobile_number))
        {
            throw new Exception("Invalid Mobile Number");
        }
        $this->mobile_number = $mobile_number;
    }
    private function get_mobile_number()
    {
        return $this->mobile_number;
    }
    
    private function set_address($address)
    {
        $reg = "/^[a-z ][A-Z0-9 ]+$/i";
        if(!preg_match($reg, $address))
        {
            throw new Exception("Invalid Address");
        }
        $this->address = $address;
    }
    private function get_address()
    {
        return $this->address;
    }
    private function set_profile_image($profile_image)
    {
        if($profile_image['error'] == 4)
        {
            throw new Exception("*File Missing");
        }
        $image = getimagesize($profile_image['tmp_name']);
        if(!$image)
        {
            throw new Exception("* Not a valid Image");
        }
        if($profile_image['size'] > 5000000)
        {
            throw new Exception("*Max file size allow 500 K");
        }
        if($profile_image['type'] != "image/png" && $profile_image['type'] != "image/jpeg")
        {
            throw new Exception("*Only png allowed");
        }
        if($profile_image['type'] != $image['mime'])
        {
            throw new Exception("* Curropt image");
        }
        $path_info = pathinfo($profile_image['name']);
        extract($path_info);
        $this->profile_image = $this->email .".".$extension;
    }
    
    private function get_profile_image()
    {
         $user_image = $_SERVER['DOCUMENT_ROOT'] . "/ultronicblast/users/userimages/$this->email/$this->email.png";
        
        if(is_file($user_image)){
            $img_uri = "http://" . $_SERVER['HTTP_HOST'] . "/ultronicblast/users/userimages/$this->email/$this->email.png";
        }
        else{
            $img_uri = "http://" . $_SERVER['HTTP_HOST'] . "/ultronicblast/users/userimages/no_img.gif";            
            // $img_uri = "http://" . $_SERVER['HTTP_HOST'] . "/ultronicblast/admin/admins/$this->admin_name/$this->admin_name.png";
        }
        return $img_uri;
    }

        public function upload_profile_image($source_path)
    {
        $str_path = "../users/userimages/$this->email/$this->profile_image";
        if(!is_dir("../users/userimages"))
        {
            if(!mkdir("../users/userimages"))
            {
                throw new Exception("* Failed to create folder User");
            }
        }
        if(!is_dir("../users/userimages/$this->email"))
        {
            if(!mkdir("../users/userimages/$this->email"))
            {
                throw new Exception("* Failed to create folder User/$this->email");
            }
        }
        $result = @move_uploaded_file($source_path, $str_path);
        if(!$result)
        {
            throw new Exception("* Failed to upload file");
        }
    }
    //Add user
    public function add_user($act_code)
    {
        $reg ="/^[a-z0-9]{32}$/i";
        if(!preg_match($reg, $act_code))
        {
            throw new Exception("Invalid activation code");
        }
        $obj_db = $this->obj_db();
        $now = date("Y-m-d H:i:s");
        $query_insert = "insert into users"
                . "(user_id, first_name, last_name, user_name, mobile_number, email, "
                . " password, address, profile_image, signup_date, reset_code) "
                . " values(NULL, '$this->first_name', '$this->last_name', '$this->user_name', "
                . " $this->mobile_number, '$this->email', '$this->password', '$this->address', '$this->profile_image', '$now', '$act_code') ";
//        echo $query_insert;
//        die;
        $obj_db->query($query_insert);
        
        if($obj_db->errno)
        {
            throw new Exception("New user inserted Error $obj_db->error - $obj_db->errno");
        }
        $this->user_id = $obj_db->insert_id;
        
    }
    public function user_activate($act_code)
        {
        $reg = "/^[a-z0-9]{32}$/i";
        if(!preg_match($reg, $act_code))
        {
            throw new Exception("Invalid Activation Code");
        }
        $obj_db = $this->obj_db();
        $select_query = "select user_id, reset_code, signup_date, is_active from users "
                . " where user_id = $this->user_id";
        
//        echo $select_query;
//        die;
        $result = $obj_db->query($select_query);
        if($obj_db->errno)
        {
            throw new Exception("Select activate User Error - $obj_db->error - $obj_db->errno");
        }
        if(!$result->num_rows)
        {
            throw new Exception("User not found");
        }
        $data = $result->fetch_object();
        if($data->is_active)
        {
            throw new Exception("User already activate");
        }
        if($data->reset_code != $act_code)
        {
            throw new Exception("Invalid Activation Link");
        }
        $now = time();
        $expiry_time = strtotime($data->signup_date) + (60 * 60 * 24 * 3);
        if($now > $expiry_time)
        {
            $query_delete = "delete from users "
                    . " where user_id = $this->user_id ";
            $obj_db->query($query_delete);
            if($obj_db->errno)
            {
                throw new Exception("Delete User Error - $obj_db->error - $obj_db->errno");
            }
            if(!$obj_db->affected_rows)
            {
                throw new Exception("Falide to delete activate User");
            }
        }
        $query_update = "update users set "
                . " is_active = 1, "
                . " reset_code = NULL "
                . " where user_id = $this->user_id";
        $obj_db->query($query_update);
        if($obj_db->errno)
        {
            throw new Exception("Update activate User Error - $obj_db->error - $obj_db->errno");
        }
        if(!$obj_db->affected_rows)
        {
            throw new Exception("Failed to upadte activte User");
        }
      }
      private function get_login()
    {
        return $this->login_status;
    }
      public function login($remember = FALSE)
      {
          $obj_db = $this->obj_db();
          $query_select = "select user_id, email, is_active from users "
                  . " where email = '$this->email' "
                  . " and password = '$this->password' ";
          $_SESSION['user_id'] = $this->user_id;
         $result = $obj_db->query($query_select);
          if($obj_db->errno)
          {
              throw new Exception("Select Login User Error $obj_db->error - $obj_db->errno");
          }
          if(!$result->num_rows)
          {
              throw new Exception("Login Failed");
          }
          $data = $result->fetch_object();
          if(!$data->is_active)
          {
              throw new Exception("Your account pending activation");
          }
          $this->email = $data->email;
          $this->password = $data->password;
          $this->user_id = $data->user_id;
          $this->login_status = TRUE;
          $str_user = serialize($this);
          $_SESSION['obj_user'] = $str_user;
          
          if($remember)
          {
               $expiry_time = time() + (60 * 60 * 24 * 15);
               setcookie('UltronicBlast', $str_user,$expiry_time);
          }
          
      }
      public function logout()
        {
            if(isset($_SESSION['obj_user']))
            {
                unset($_SESSION['obj_user']);
            }
            if(isset($_COOKIE['obj_user']))
            {
                setcookie("UltronicBlast", "aaa", 1, "/");
            }
        }
        public function verify_retype_password($password, $retype_password)
    {
        if(empty($retype_password) || $password != $retype_password)
        {
            throw new Exception("Not match Password");
        }
    }
    

    public function profile()
    {
        $obj_db = $this->obj_db();
        $query_select = "select * from users "
                . " where user_id = '$this->user_id' ";
        $result = $obj_db->query($query_select);
        if($obj_db->errno)
        {
            throw new Exception("Select Profile Error - $obj_db->error - $obj_db->errno");
        }
        if(!$result->num_rows)
        {
            throw new Exception("Contact Admin");
        }
        $data = $result->fetch_object();
        if(!$data->is_active)
        {
            throw new Exception("Your account is Disabled - Contact Admin");
        }
        $this->first_name = $data->first_name;
        $this->last_name = $data->last_name;
        $this->user_name = $data->user_name;
        $this->mobile_number = $data->mobile_number;
        $this->address = $data->address;
        $this->profile_image = $data->profile_image;
    }
    public function update_account()
    {
        $query_update = "update users set "
                . " first_name = '$this->first_name', "
                . " last_name = '$this->last_name', "
                . " user_name = '$this->user_name', "
                . " mobile_number = '$this->mobile_number',"
                . " profile_image = '$this->profile_image' "
                . " where user_id = '$this->user_id' ";
        $obj_db = $this->obj_db();
        $result = $obj_db->query($query_update);
    }
    public function update_password()
    {
        
        $query_update = "UPDATE `users` SET `password` = '$this->password' WHERE `users`.`email` = '$this->email'";
//        echo($query_update);
//        die;
        $obj_db = $this->obj_db();
        $result = $obj_db->query($query_update);
    }
    
    public static function total_user()
    {
        $obj_db = self::obj_db();
        $query_select = "select count(*) 'count' from users "
                . " where is_active = 1 ";
        $result = $obj_db->query($query_select);
        $data = $result->fetch_object();
        $totalusers = $data->count;
        echo ($totalusers);
       
    }
    public static function users($start, $count)
    {
        $obj_db = self::obj_db();
        $query_select = "select * from users where is_active = 1";
//        echo ($query_select);
//        die;
        if ($start < 0) {
            $start = 0;
            }
            if ($count < 1) {
                $count = 1;
            }
            $query_select .= " limit $start, $count";
        $result= $obj_db->query($query_select);
        $data = array();
        while ($user = $result->fetch_object())
        {
            $temp = new User();
            $temp->user_id = $user->user_id; 
            $temp->user_name = $user->user_name;
            $temp->email = $user->email;
            $temp->mobile_number = $user->mobile_number;
            $temp->address = $user->address;
            $data[] = $temp;
        }
        return $data;
        
    }
    
    public function delete_user($key){
        $obj_db = self::obj_db();
        $query_delete = "delete from users where user_id = $key ";
        $obj_db->query($query_delete);
    }
    
    public function check_user($email){
        $obj_db = $this->obj_db();
        $query_select = "select email from users "
                . " where email = '$email' ";
        $result = $obj_db->query($query_select);
        //$num_rows = affected_rows($result);
        if($obj_db->affected_rows)
        {
            throw new Exception("Email already exist");
        }
        
    }

}

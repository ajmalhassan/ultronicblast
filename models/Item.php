<?php
require_once 'db_connection.php';
class Item extends DB_Connection {
    private $item_id;
    private $quantity;
    
    
    //Constructor
   public function __construct($item_id, $quantity = 1){
//        $this->item_id = $item_id;
//        $this->quantity = $quantity;
        $this->setItem_ID($item_id);
        $this->setQuantity($quantity);
    }
    
    public function __set($name, $value) {
        $restricted = array("item_id");
        
        if(in_array($name, $restricted)){
            throw new Exception("Cannot Access Read Only Property $name");
        }
        $method = "set$name";
        if (!method_exists($this, $method)) {
            throw new Exception("SET::Property $name does not exist");
        }
        $this->$method($value);
    }

    public function __get($name) {

        $method = "get$name";
        if (!method_exists($this, $method)) {
            throw new Exception("GET::Property $name does not exist");
        }
        return $this->$method();
    }

    private function setItem_ID($item_id){
        if(!is_numeric($item_id) || $item_id <= 0){
            throw new Exception("*Invalid/Missing Item ID");
        }
        $this->item_id = $item_id;
    }
    
    private function getItem_ID() {
        return $this->item_id;
    }
    
    private function setQuantity($quantity){
        if(!is_numeric($quantity) || $quantity <= 0){
            throw new Exception("*Invalid/Missing Quantity");
        }
        $this->quantity = $quantity;
    }
    
    private function getQuantity() {
        return $this->quantity;
    }
    
    private function getName() {
        $query = "select product_name from products "
                . " where productID = '$this->item_id'";
        $obj_db = self::obj_db();
        $result = $obj_db->query($query);
        $data = $result->fetch_object();
        return $data->product_name;
    }
    
    private function getUnit_Price() {
        $query = "select sale_price from products "
                . " where productID = '$this->item_id'";
        $obj_db = self::obj_db();
        $result = $obj_db->query($query);
        $data = $result->fetch_object();
        return $data->sale_price;
    }
    
    private function getTotal(){
        $total = $this->quantity * $this->unit_price;
        return $total;
    }
}

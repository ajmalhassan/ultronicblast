<?php
require_once 'db_connection.php';
class Brand extends DB_Connection{
    private $brandID;
    private $brand_name;
    
    //Constructer
    public function __construct() {
        
    }
    //Setter
    public function __set($name, $value) {
        $method_name = "set_$name";
        if(!method_exists($this, $method_name))
        {
            throw new Exception("Set Property $name does not exist");
        }
        $this->$method_name($value);
    }
    //Getter
    public function __get($name) {
        $method_name = "get_$name";
        if(!method_exists($this, $method_name))
        {
            throw new Exception("Get Property $name does not exist");
        }
        return $this->$method_name();
    }
    
    //Set Brand ID method 
    private function set_brandID($brandID)
    {
        if(is_numeric($brandID) || $brandID <= 0)
        {
            throw new Exception("Invalid Brand ID");
        }
        $this->brandID = $brandID;
        
    }
    //Get Brand ID method
    private function get_brandID()
    {
        return $this->brandID;
    }
    private function set_brand_name($brand_name)
    {
        $reg ="/^[a-z0-9 ]+$/i";
        if(!preg_match($reg, $brand_name))
        {
            throw new Exception("Invalid Brand Name");
        }
        $this->brand_name = $brand_name;  
    }
    private function get_brand_name()
    {
        return $this->brand_name;
    }
    
    //Add brand 
    
    public function add_brand()
    {
        $obj_db = $this->obj_db();
        $now = date("Y-m-d H:i:s");
        $query_insert = "insert into brands "
                . " (brand_id, brand_name, register_time) "
                . " values('$this->brandID', '$this->brand_name', '$now')";
        $result = $obj_db->query($query_insert);
        if($obj_db->errno)
        {
            throw new Exception("Brand Inserted Error $obj_db->error - $obj_db->errno");
        }
        $this->brandID = $obj_db->insert_id;
    }
    //Get Brands
    public static function get_brands($start, $count)
    {
        $obj_db = self::obj_db();
        $query_select = "select * from brands order by brand_id desc";
        if ($start < 0) {
            $start = 0;
            }
            if ($count < 1) {
                $count = 1;
            }
        $query_select .= " limit $start, $count";
        $result = $obj_db->query($query_select);
        if($obj_db->errno)
        {
            throw new Exception("Select Brand Error $obj_db->error - $obj_db->errno");
        }
        $brands = array();
        while ($data = $result->fetch_object())
        {
            $tmp = new Brand();
            $tmp->brandID = $data->brand_id;
            $tmp->brand_name = $data->brand_name;
            $brands[] = $tmp;
        }
        
        return $brands;
    }
    
    //Count total brands
    public static function total_brands()
    {
        $obj_db = self::obj_db();
        $query_select = "select count(*) 'count' from brands";
        $result = $obj_db->query($query_select);
        //$total_brand_rows = mysqli_fetch_array($result);
        //$total_brand = $total_brand_rows[0];
        $data = $result->fetch_object();
        $total_brands = $data->count;
        echo($total_brands);
    }
      public  function get_brand($keybra) {

        $obj_db = self::obj_db();

        $query = " select * from brands "
                . " where brand_id= $keybra ";

//        echo ($query);
//        die;
        $result = $obj_db->query($query);

        if ($obj_db->errno) {
            throw new Exception(" * Brand selection Error - $obj_db->error -$obj_db->errno");
        }

        if ($result->num_rows == 0) {
            throw new Exception(" * Brand(s) not found");
        }

        $brands = array();
        while ($data = $result->fetch_object()) {

            $temp = new Brand();
            $temp->brandID = $data->brand_id;
            $temp->brand_name = $data->brand_name;
            $brands[] = $temp;
        }

        return $brands;
    }
       public  function get_bra() {

        $obj_db = self::obj_db();

        $query = " select * from brands ";
        $result = $obj_db->query($query);

        if ($obj_db->errno) {
            throw new Exception(" * Brand selection Error - $obj_db->error -$obj_db->errno");
        }
        if ($result->num_rows == 0) {
            throw new Exception(" * Brand(s) not found");
        }
        $brands = array();
        while ($data = $result->fetch_object()) {

            $temp = new Brand();
            $temp->brandID = $data->brand_id;
            $temp->brand_name = $data->brand_name;
            $brands[] = $temp;
        }

        return $brands;
    }
    
    //Update product
    public function update_brand($keybra)
    {
        $obj_db = $this->obj_db();
        $query_update = "update brands set "
                . " brand_name = '$this->brand_name' "
                . " where "
                . " brand_id = $keybra";
//        echo($query_update);
//        die;
//        
        $obj_db->query($query_update);
        if($obj_db->affected_rows ==0)
        {
            throw new Exception("Update Brand Error $obj_db->error - $obj_db->errno");
        }
    }
    //Delete Brand
    public function delete_brand($key)
    {
        $obj_db = $this->obj_db();
        $query_delete = "delete from brands "
                . " where brand_id = '$key'";
        $obj_db->query($query_delete);
        if($obj_db->affected_rows == 0)
        {
            throw new Exception("Brand Not Deleted $obj_db->error - $obj_db->errno");
        }
    }
    public static function pagination($item_per_page)
    {
        $obj_db = self::obj_db();
        $query_select = "select count(*) 'count' from brands";
        
        $result = $obj_db->query($query_select);
        $data = $result->fetch_object();
        $total_brands = $data->count;
        $total_pages = ceil($total_brands/$item_per_page);
        $pNums = array();
        for($i = 1, $j = 0; $i <= $total_pages; $i++, $j += $item_per_page)
        {
            $pNums[$i] = $j;
        }
//        echo "<pre>";
//        print_r($pNums);
//        echo "</pre>";
//        die;
        return $pNums;
    }
}

<?php
require_once 'db_connection.php';
require_once 'category.php';
require_once 'brand.php';
class product extends DB_Connection
{
    private $product_ID;
    private $product_name;
    private $description;
    private $unit_price;
    private $sale_price;
    private $quantity;
    //private $view_count;
    private $product_image;
    private $product_image2;
    private $product_image3;
    private $product_image_name;
    private $product_image_name2;
    private $product_image_name3;
    private $product_discount;
    public $brand;
    public $category;
    
    public function __construct() {
        $this->category = new Category();
        $this->brand = new Brand();
    }
        //Setter
    public function __set($name, $value) {
        $method_name = "set_$name";
        if(!method_exists($this, $method_name))
        {
            throw new Exception("SET Property $name does not exist");
        }
        $this->$method_name($value);
    }
    //Getter
    public function __get($name) {
        $method_name = "get_$name";
        if(!method_exists($this, $method_name))
        {
            throw new Exception("GET Property $name does not exist");
        }
        return $this->$method_name();
    }
    
    //Private methods
    //Set method Product ID
    private function set_productID($productID)
    {
        if(!is_numeric($productID) || $productID <=0 )
        {
            throw new Exception("*Invalid missing product ID");
        }
        $this->product_ID = $productID;
    }
    //Get method product ID
    private function get_productID()
    {
        return $this->product_ID;
    }
    //Set method product_name
    private function set_product_name($product_name)
    {
        
        $reg ="/^[a-zA-Z0-9. ]+$/";
        if(!preg_match($reg, $product_name))
        {
            throw new Exception("*Invalid Product Name"); 
        }
        $this->product_name = ucfirst(strtolower($product_name));
    }
    //Get method product_name
    private function get_product_name()
    {
        return $this->product_name;
    }
    private function set_product_image_name($product_name)
    {
        
        //$reg = "/^[a-z ][a-z0-9 ]+$/i";
        $productn = $product_name;
        $reg = "/ /";
        $rep = "_";
        $name = preg_replace($reg, $rep, $productn);
        
        $this->product_image_name = $name;
    }
    //Get method product image name
    private function get_product_image_name()
    {
        return $this->product_image_name;
    }
     private function set_product_image_name2($product_name)
    {
        
        //$reg = "/^[a-z ][a-z0-9 ]+$/i";
        $productn = $product_name;
        $reg = "/ /";
        $rep = "_";
        $name = preg_replace($reg, $rep, $productn);
        
        $this->product_image_name2 = $name;
    }
    //Get method product image name
    private function get_product_image_name2()
    {
        return $this->product_image_name2;
    }
    private function set_product_image_name3($product_name)
    {
        
        //$reg = "/^[a-z ][a-z0-9 ]+$/i";
        $productn = $product_name;
        $reg = "/ /";
        $rep = "_";
        $name = preg_replace($reg, $rep, $productn);
        
        $this->product_image_name3 = $name;
    }
    //Get method product image name
    private function get_product_image_name3()
    {
        return $this->product_image_name3;
    }
    //Set method description
    private function set_description($description)
    {
//       // $description = trim($description);
//        $reg = "//";
//        if(!preg_match($reg, $description))
//        {
//            throw new Exception("*Missing/Too short Description");
//        }
        $this->description = $description;
    }
    //Get method description
    private function get_description()
    {
        return $this->description;
    }
    //Set method unit_price
    private function set_unit_price($unit_price)
    {
        $unit_price = trim($unit_price);
        if(!is_numeric($unit_price) || $unit_price <= 0)
        {
            throw new Exception("*Invalid/Missing Unit Price");
        }
        $this->unit_price = $unit_price;
    }
    //Get method unit_price
    private function get_unit_price()
    {
        return $this->unit_price;
    }
    //Set sale price
    private function set_sale_price($sale_price)
        {
            $sale_price = trim($sale_price);
        if(!is_numeric($sale_price) || $sale_price <= 0)
        {
            throw new Exception("*Invalid/Missing Sale Price");
        }
        $this->sale_price= $sale_price;
        }
        //Get method Sale Price
        private function get_sale_price()
        {
            return $this->sale_price;
        }
    //Set method quantity
    private function set_quantity($quantity)
    {
        $quantity = trim($quantity);
        if(!is_numeric($quantity) || $quantity <= 0)
        {
            throw new Exception("*Invalid/Missing Quantity");
        }
        $this->quantity = $quantity;
    }
    //Get method quantity
    private function get_quantity()
    {
        return $this->quantity;
    }
    //Set method product_image
    private function set_product_image($product_image)
    {
        if($product_image['error'] == 4)
        {
            throw new Exception("*File Missing");
        }
        $image =  getimagesize($product_image['tmp_name']);
        if(!$image)
        {
            throw new Exception("*Not a valid Image");
        }
        if($product_image['size'] >50000000)
        {
            throw new Exception("*Max file size allowed 5MB");
        }
        if($product_image['type'] != "image/png" && $product_image['type'] != "image/jpeg")
        {
            throw new Exception("*Only allow png image");
        }
        if($product_image['type'] != $image['mime'])
        {
            throw new Exception("*Corrupt image");
        }
        if(is_null($this->product_name))
        {
            throw new Exception("Failed to generate file name");
        }
        $path_info = pathinfo($product_image['name']);
        extract($path_info);
        $this->product_image = $this->product_image_name.".".$extension;
    }
    //Get product_image
    private function get_product_image()
    {
        return $this->product_image;
    }
    public function upload_product_image($source_path)
    {
        $str_path = "../products/$this->product_image_name/$this->product_image";
        if(!is_dir("../products"))
        {
            if(!mkdir("../products"))
            {
                throw new Exception("*Failed to create folder../products");
            }
        }
        if(!is_dir("../products/$this->product_image_name"))
        {
            if(!mkdir("../products/$this->product_image_name"))
            {
                throw new Exception("*Failed to create folder../products/$this->product_image_name");
            }
        }
        $result = @move_uploaded_file($source_path, $str_path);
        if(!$result)
        {
            throw new Exception("*Failed to upload Image");
        }
    }
     //Set method product_image
    private function set_product_image2($product_image2)
    {
        if($product_image2['error'] == 4)
        {
            throw new Exception("*File Missing");
        }
        $image =  getimagesize($product_image2['tmp_name']);
        if(!$image)
        {
            throw new Exception("*Not a valid Image");
        }
        if($product_image2['size'] >50000000)
        {
            throw new Exception("*Max file size allowed 5MB");
        }
        if($product_image2['type'] != "image/png" && $product_image2['type'] != "image/jpeg")
        {
            throw new Exception("*Only allow png image");
        }
        if($product_image2['type'] != $image['mime'])
        {
            throw new Exception("*Corrupt image");
        }
        if(is_null($this->product_name))
        {
            throw new Exception("Failed to generate file name");
        }
        $path_info = pathinfo($product_image2['name']);
        extract($path_info);
        $this->product_image2 = $this->product_image_name2 ."2.".$extension;
    }
    //Get product_image
    private function get_product_image2()
    {
        return $this->product_image2;
    }
    public function upload_product_image2($source_path)
    {
        $str_path = "../products/$this->product_image_name2/$this->product_image2";
        if(!is_dir("../products"))
        {
            if(!mkdir("../products"))
            {
                throw new Exception("*Failed to create folder../products");
            }
        }
        if(!is_dir("../products/$this->product_image_name2"))
        {
            if(!mkdir("../products/$this->product_image_name2"))
            {
                throw new Exception("*Failed to create folder../products/$this->product_image_name2");
            }
        }
        $result = @move_uploaded_file($source_path, $str_path);
        if(!$result)
        {
            throw new Exception("*Failed to upload Image");
        }
    }
     //Set method product_image
    private function set_product_image3($product_image3)
    {
        if($product_image3['error'] == 4)
        {
            throw new Exception("*File Missing");
        }
        $image =  getimagesize($product_image3['tmp_name']);
        if(!$image)
        {
            throw new Exception("*Not a valid Image");
        }
        if($product_image3['size'] >50000000)
        {
            throw new Exception("*Max file size allowed 5MB");
        }
        if($product_image3['type'] != "image/png" && $product_image3['type'] != "image/jpeg")
        {
            throw new Exception("*Only allow png image");
        }
        if($product_image3['type'] != $image['mime'])
        {
            throw new Exception("*Corrupt image");
        }
        if(is_null($this->product_name))
        {
            throw new Exception("Failed to generate file name");
        }
        $path_info = pathinfo($product_image3['name']);
        extract($path_info);
        $this->product_image3 = $this->product_image_name3 ."3.".$extension;
    }
    //Get product_image
    private function get_product_image3()
    {
        return $this->product_image3;
    }
    public function upload_product_image3($source_path)
    {
        $str_path = "../products/$this->product_image_name3/$this->product_image3";
        if(!is_dir("../products"))
        {
            if(!mkdir("../products"))
            {
                throw new Exception("*Failed to create folder../products");
            }
        }
        if(!is_dir("../products/$this->product_image_name3"))
        {
            if(!mkdir("../products/$this->product_image_name3"))
            {
                throw new Exception("*Failed to create folder../products/$this->product_image_name3");
            }
        }
        $result = @move_uploaded_file($source_path, $str_path);
        if(!$result)
        {
            throw new Exception("*Failed to upload Image");
        }
    }
    //Set method product_discount
        private function set_product_discount($product_discount)
        {
            $product_discount = trim($product_discount);
        if(!is_numeric($product_discount) || $product_discount <= 0)
        {
            throw new Exception("*Invalid/Missing discount");
        }
        $this->product_discount= $product_discount;
        }
        //Get method product_discount
        private function get_product_discount()
        {
            return $this->product_discount;
        }
        //Set method category
        private function set_category($category)
        {
            $reg = "/^[a-z ][a-z0-9 ]+$/i";
            //if(is_numeric($category_ID) || $category_ID <= 0)
            if(!preg_match($reg, $category))
            {
                throw new Exception("* Invalid Category");
            }
            $this->category = $category;
        }
        //Get method category
        
        private function get_category()
        {
            return $this->category;
        }
//        //Set method Brand
//        private function set_brand($brand)
//        {
//            $reg = "/^[a-z ][a-z0-9 ]+$/i";
//            //if(is_numeric($category_ID) || $category_ID <= 0)
//            if(!preg_match($reg, $brand))
//            {
//                throw new Exception("* Invalid Brand");
//            }
//            $this->brand = $brand;
//        }
//        //Get method brand
//        
//        private function get_brand()
//        {
//            return $this->brand;
//        }

        //Insert product to database
        
        public function add_product()
       {
            $obj_db = $this->obj_db();
            $insert_query = "INSERT INTO products"
                    . "(productID, product_name, unit_price, sale_price, quantity, "
                    . " product_discount, description, product_image, product_image2, product_image3, categoryID, brandID)"
                    . " VALUES "
                    . " ('$this->product_ID', '$this->product_name', '$this->unit_price', '$this->sale_price', '$this->quantity', "
                    . " '$this->product_discount', '$this->description', '$this->product_image', '$this->product_image2', '$this->product_image3', '$this->category', '$this->brand')";
            $result = $obj_db->query($insert_query);
//            echo ($insert_query);
//            die;
            if($obj_db->errno)
            {
                throw new Exception("*New product inserted Error - $obj_db->error - $obj_db->errno");
            }
            $this->product_ID = $obj_db->insert_id;
        }
        //get sort products
        public static function get_sorting_products($start, $count, $type ='all')
        {
            $obj_db = self::obj_db();
            $query = "select * from products";
            $types = array("all", "top", "new", "price");
            if(!in_array($type, $types))
            {
               $type = "all"; 
            }
            if($type == "new")
            {
                $query .=" order by productID desc";
            }
            if($start < 0)
            {
                $start = 0;
            }
            if($count < 1)
            {
                $count = 1;
            }
            $query .=" limit $start, $count";
            $result = $obj_db->query($query);
        $products = array();
        while($product = $result->fetch_object())
        {
            $temp = new $product();
            $temp->productID =$product->productID;
            $temp->product_name = $product->product_name;
            $temp->unit_price = $product->unit_price;
            $temp->sale_price = $product->sale_price;
            $temp->quantity = $product->quantity;
            $temp->product_discount = $product->product_discount;
            $temp->description = $product->description;
            $temp->product_image = $product->product_image;
            $products[] = $temp;
        }
        return $products;
        }
        
//Get products
    public static function get_products($start, $count)
    {
        $obj_db = self::obj_db();
        //$query = "select * from products";
        $query = "select p.productID, p.product_name, p.unit_price, p.sale_price, p.product_image, p.quantity, p.product_discount, p.description, "
                . "b.brand_name, c.category_name from products p "
                . "LEFT JOIN brands b ON p.brandID = b.brand_id "
                . "LEFT JOIN categories c ON p.categoryID = c.category_id";
        if($start < 0)
            {
                $start = 0;
            }
            if($count < 1)
            {
                $count = 1;
            }
            $query .=" limit $start, $count";
//        echo ($query);
//        die;
        $result = $obj_db->query($query);
        $products = array();
        while($product = $result->fetch_object())
        {
            $temp = new $product();
            $temp->productID =$product->productID;
            $temp->product_name = $product->product_name;
            $temp->unit_price = $product->unit_price;
            $temp->sale_price = $product->sale_price;
            $temp->quantity = $product->quantity;
            $temp->product_discount = $product->product_discount;
            $temp->description = $product->description;
            $temp->product_image = $product->product_image;
            $temp->brand = $product->brand_name;
            $temp->category = $product->category_name;
            $products[] = $temp;
        }
        return $products;
    }
    //get categories products 
    public static function get_cat_products($cat_id)
    {
        $obj_db =  self::obj_db();
        $query = "select * from products where categoryID=$cat_id";
        $result = $obj_db->query($query);
        $products = array();
        while($product = $result->fetch_object())
        {
            $temp = new $product();
            $temp->productID =$product->productID;
            $temp->product_name = $product->product_name;
            $temp->unit_price = $product->unit_price;
            $temp->sale_price = $product->sale_price;
            $temp->quantity = $product->quantity;
            $temp->product_discount = $product->product_discount;
            $temp->description = $product->description;
            $temp->product_image = $product->product_image;
            $products[] = $temp;
        }
        return $products;
        
    }
    //get brand products 
    public static function get_brand_products($brand_id)
    {
        $obj_db =  self::obj_db();
        $query = "select * from products where categoryID=$brand_id";
        $result = $obj_db->query($query);
        $products = array();
        while($product = $result->fetch_object())
        {
            $temp = new $product();
            $temp->productID =$product->productID;
            $temp->product_name = $product->product_name;
            $temp->unit_price = $product->unit_price;
            $temp->sale_price = $product->sale_price;
            $temp->quantity = $product->quantity;
            $temp->product_discount = $product->product_discount;
            $temp->description = $product->description;
            $temp->product_image = $product->product_image;
            $products[] = $temp;
        }
        return $products;
        
    }
    
    //Get search result products
    public static function get_search_result($user_keyword)
    {
        $obj_db = self::obj_db();
        $query_search = "select * from products where product_name like '%$user_keyword%'";
//        echo $query_search;
//        die;
        $result = $obj_db->query($query_search);
        $products = array();
        while($product = $result->fetch_object())
        {
            $temp = new $product();
            $temp->productID =$product->productID;
            $temp->product_name = $product->product_name;
            $temp->unit_price = $product->unit_price;
            $temp->sale_price = $product->sale_price;
            $temp->quantity = $product->quantity;
            $temp->product_discount = $product->product_discount;
            $temp->description = $product->description;
            $temp->product_image = $product->product_image;
            $products[] = $temp;
        }
        return $products;
        
    }
    //Products pagination
    
    public static function pagination($item_per_page)
    {
        $obj_db = self::obj_db();
        $query_select = "select count(*) 'count' from products";
        
        $result = $obj_db->query($query_select);
        $data = $result->fetch_object();
        $total_products = $data->count;
        $total_pages = ceil($total_products/$item_per_page);
        $pNums = array();
        for($i = 1, $j = 0; $i <= $total_pages; $i++, $j += $item_per_page)
        {
            $pNums[$i] = $j;
        }
//        echo "<pre>";
//        print_r($pNums);
//        echo "</pre>";
//        die;
        return $pNums;
    }
    public static function total_products()
    {
        $obj_db = self::obj_db();
        $query_select = "select count(*) 'count' from products";
        $result =  $obj_db->query($query_select);
        $data = $result->fetch_object();
//        $total_product_rows = mysqli_fetch_array($result);
        $total_products = $data->count;
        echo ($total_products);
    }
    //Get single product
    public  function get_single_product($keypro) 
            {

        $obj_db = self::obj_db();

        $query = " select * from products "
                . " where productID = $keypro ";

//        echo ($query);
//        die;
        $result = $obj_db->query($query);

        if ($obj_db->errno) {
            throw new Exception(" * Product selection Error - $obj_db->error -$obj_db->errno");
        }

        if ($result->num_rows == 0) {
            throw new Exception(" * Product(s) not found");
        }

        $products = array();
        while ($data = $result->fetch_object()) {

            $temp = new Product();
            $temp->product_ID = $data->productID;
            $temp->product_name = $data->product_name;
            $temp->description = $data->description;
            $temp->product_image = $data->product_image;
            $temp->unit_price = $data->unit_price;
            $temp->quantity = $data->quantity;
            $temp->product_discount = $data->product_discount;
            $products[] = $temp;
        }

        return $products;
    }
    
    //Update product
    public function update_product($keypro)
    {
        $obj_db = $this->obj_db();
        $query_update = "update products set "
                . " product_name = '$this->product_name', "
                . " unit_price   =  '$this->unit_price', "
                . " quantity = '$this->quantity ', "
                . " product_discount = '$this->product_discount', "
                . " description = '$this->description', "
                . "  product_image = '$this->product_image'"
                . " where "
                . " productID = $keypro";
//        echo($query_update);
//        die;     
        $obj_db->query($query_update);
        if($obj_db->affected_rows ==0)
        {
            throw new Exception("Update product Error $obj_db->error - $obj_db->errno");
        }
    }
    
    //Delete product
    
    public function delete_product($key)
    {
        $obj_db = $this->obj_db();
        $query_delete = "delete from products "
                . " where productId = '$key'";
        $obj_db->query($query_delete);
        if($obj_db->affected_rows == 0)
        {
            throw new Exception("Product Not Deleted $obj_db->error - $obj_db->errno");
        }
        
    }
      //Get Get single product
    public static function get_detail_product($keypro) 
            {

        $obj_db = self::obj_db();

        $query = " select * from products "
                . " where productID= $keypro ";

//        echo ($query);
//        die;
        $result = $obj_db->query($query);

        if ($obj_db->errno) {
            throw new Exception(" * Product selection Error - $obj_db->error -$obj_db->errno");
        }

        if ($result->num_rows == 0) {
            throw new Exception(" * Product(s) not found");
        }

        $products = array();
        while ($data = $result->fetch_object()) {

            $temp = new Product();
            $temp->product_name = $data->product_name;
            $temp->description = $data->description;
            $temp->product_image = $data->product_image;
            $temp->product_image2 = $data->product_image2;
            $temp->product_image3 = $data->product_image3;
            $temp->unit_price = $data->unit_price;
            $temp->sale_price = $data->sale_price;
            $temp->quantity = $data->quantity;
            $temp->product_discount = $data->product_discount;
            $products[] = $temp;
        }

        return $products;
    }
}

<?php
require_once 'db_connection.php';
require_once 'DBTrait.php';
require_once 'product.php';
require_once 'user.php';
class Order{
    private $order_id;
    use DBTrait;
    
    public static function place_order($obj_user, $obj_contact, $obj_cart)
    {
     $obj_db = self::obj_db();
     $now = date("Y-m-d H:i:s");
     $query_insert = " insert into orders "
             . " (order_id, user_id, total_quantity, total_price, order_date, address) "
             . " value(NULL, '$obj_user->user_id', '$obj_cart->count', '$obj_cart->total', '$now', '$obj_contact->address')";
    
//     echo($query_insert);
//     die;
     $obj_db->query($query_insert);
     if($obj_db->errno)
     {
         throw new Exception("Order Not Placed $obj_db->error - $obj_db->errno");
     }
     $order_id = $obj_db->insert_id;
     foreach ($obj_cart->items as $item)
     {
         $query_insert = " insert into orderitems "
                 . "(order_item_id, order_id, product_id, unit_price, quantity) "
                 . " value(NULL, '$order_id', '$item->item_id', '$item->Unit_Price', '$item->Quantity')";
//         echo ($query_insert);
//         die;
         $obj_db->query($query_insert);
         if($obj_db->errno)
         {
             throw new Exception("Order items not Inserted $obj_db->error - $obj_db->errno");
         }
     }
     $obj_cart->empty_cart();
    }
    
    public static function total_orders()
    {
        $obj_db = self::obj_db();
        $query_select = " select count(*) 'count' from orders "
                . " where order_status = 1 ";
        $result = $obj_db->query($query_select);
        $data = $result->fetch_object();
        $totalorders = $data->count;
        echo($totalorders);
    }
    public static function pending_orders()
    {
        $obj_db = self::obj_db();
        $query_select = " select count(*) 'count' from orders where order_status = 0";
        $result = $obj_db->query($query_select);
        $data = $result->fetch_object();
        $pendingorders = $data->count;
        echo($pendingorders);
    }
    
    public static function get_orders($start, $count)
    {
        $obj_db = self::obj_db();
        //$query = "select * from products";
        $query = "select u.user_name, u.email, o.order_id, o.total_quantity "
                . " from users u LEFT JOIN orders o ON u.user_id = o.user_id"
                . " where o.order_status = 0 "
                . " ORDER BY o.order_id DESC ";
        
//        echo ($query);
//        die;
        if ($start < 0) {
            $start = 0;
            }
            if ($count < 1) {
                $count = 1;
            }
            $query .= " limit $start, $count";
        $result = $obj_db->query($query);
        $products = array();
        while($product = $result->fetch_object())
        {
            $temp = new $product();
            $temp->customer_name = $product->user_name;
            $temp->customer_email = $product->email;
            $temp->total_quantity = $product->total_quantity;
            $temp->order_no = $product->order_id;
            $products[] = $temp;
        }
        return $products;
    }
    public static function get_complete_orders($start, $count)
    {
        $obj_db = self::obj_db();
//            $query = "SELECT orders.order_id, orders.total_price,orders.total_quantity, users.user_name,users.email "
//                ." FROM orders JOIN users "
//                . "ON orders.user_id = users.user_id "
//                    . " where order_status = 0";
//            $query = "select * from orders where order_status = 1"
//                    . " ORDER BY order_id DESC ";

        
        $query =  "SELECT orders.order_id, orders.user_id, orders.total_quantity, orders.total_price, orders.order_status, users.user_id, users.user_name, users.email FROM orders INNER JOIN users ON orders.user_id = users.user_id AND order_status = 1 order by  order_id DESC";
//        echo ($query);
//        die;
        if ($start < 0) {
            $start = 0;
            }
            if ($count < 1) {
                $count = 1;
            }
            $query .= " limit $start, $count";
        $result = $obj_db->query($query);
        $products = array();
        while($product = $result->fetch_object())
        {
            $temp = new $product();
            //$temp->customer_name = $product->user_name;
           // $temp->customer_email = $product->email;
            $temp->total_price = $product->total_price;
            $temp->total_quantity = $product->total_quantity;
            $temp->order_no = $product->order_id;
            $temp->user_name = $product->user_name;
            $temp->email = $product->email;
            $products[] = $temp;
        }
        return $products;
    }
    
    public static function get_order_detail($order_no)
    {
        $obj_db = self::obj_db();
        //$query = "select * from products";
        $query = "select u.user_name, u.email, o.order_id, o.total_quantity, o.order_date, o.address, o.total_price "
                . " from users u LEFT JOIN orders o ON u.user_id = o.user_id "
                . " where o.order_id = '$order_no' ";
        
//        echo ($query);
//        die;
        $result = $obj_db->query($query);
        $products = array();
        while($product = $result->fetch_object())
        {
            $temp = new $product();
            $temp->customer_name = $product->user_name;
            $temp->customer_email = $product->email;
            $temp->total_quantity = $product->total_quantity;
            
            $temp->order_no = $product->order_id;
            $temp->order_date = $product->order_date;
            $temp->shipping_address = $product->address;
            $temp->total_price = $product->total_price;
            $products[] = $temp;
        }
        return $products;
    }
    public static function pagination_order($item_per_page)
    {
        $obj_db = self::obj_db();
        $query_select = "select count(*) 'count' from orders";
        
        $result = $obj_db->query($query_select);
        $data = $result->fetch_object();
        $total_products = $data->count;
        $total_pages = ceil($total_products/$item_per_page);
        $pNums = array();
        for($i = 1, $j = 0; $i <= $total_pages; $i++, $j += $item_per_page)
        {
            $pNums[$i] = $j;
        }
//        echo "<pre>";
//        print_r($pNums);
//        echo "</pre>";
//        die;
        return $pNums;
    }
    
    public static function complete_order($order_id)
    {
        $obj_db = self::obj_db();
        $query_update ="update orders set "
                . " order_status = 1 "
                . " where order_id = $order_id ";
        $obj_db->query($query_update);
        
    }
    public static function single_product($order_id)
    {
        $obj_db = self::obj_db();
        $query_select = "SELECT orders.user_id, orders.order_id,orders.total_quantity, "
                . "orderitems.unit_price, products.product_name,products.product_image "
                . "FROM orders "
                . "left JOIN orderitems ON orderitems.order_id = orders.order_id " 
                . "left JOIN products ON products.productID = orderitems.product_id "
                . "where "
                . "orders.order_id = '$order_id'";
        //echo($query_select);       
        //die;
            $result = $obj_db->query($query_select);
            $single_product = array();
            while($single_product1 = $result->fetch_object())
            {
                $temp = new product();
                $temp->product_name = $single_product1->product_name;
                //$temp->product_image = $single_product1->product_image;
                $temp->quantity = $single_product1->total_quantity;
                $temp->unit_price = $single_product1->unit_price;
                
                $single_product[] = $temp;
            }
        return $single_product;
    }
 public static function user_orders($user_id)
    {
        $obj_db = self::obj_db();
        $query_select  = "SELECT orders.user_id, orders.order_id,orders.total_quantity, orders.total_price, orders.order_date, " 
               ." orderitems.unit_price, products.product_name,products.product_image " 
               ." FROM orders "
               ." left JOIN orderitems ON orderitems.order_id = orders.order_id "
               ." left JOIN products ON products.productID = orderitems.product_id "
               ." WHERE orders.user_id = '$user_id' ";
//        echo($query_select);
//        die;
        $result = $obj_db->query($query_select);
        $orders = array();
        while ($data = $result->fetch_object())
        {
            $temp = new Order();
            $temp->order_date = $data->order_date;
            $temp->total_price = $data->total_price;
            $temp->product_qty = $data->total_quantity;
            $orders[] = $temp;
        }
        return $orders;
    }      
    
    public function delete_order($id){
        $obj_db = $this->obj_db();
        $query_del = "delete from orders where order_id = $id";
        $obj_db->query($query_del);
    }
    
}


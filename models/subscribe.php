<?php
require_once 'DBTrait.php';
class Subscribe {
    private $email;
    public $browser;
    public  $date;

use DBTrait;
    
    public function __set($name, $value) {
        $method_name = "set_$name";
        if(!method_exists($this, $method_name)){
            throw new Exception("SET $name property does not exist");
        }
        $this->$method_name($value);
    }
    
    public function __get($name) {
        $method_name = "get_$name";
        if(!method_exists($this, $method_name)){
            throw new Exception("GET $name Property does not exist");
        }
        return $this->$method_name();
    }
    
    private function set_email($email)
    {
        $reg = "/^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zAZ]\.)+[a-zA-Z]{2,4})$/";
        if(!preg_match($reg, $email))
        {
            throw new Exception("Invalid Email");
        }
        $this->email = $email;
    }
    private function get_email()
    {
        return $this->email;
    }
    public function add_subscribe(){
        $obj_sub = $this->obj_db();
        $now  = date('y-m-d');
        $browserAgent = $_SERVER['HTTP_USER_AGENT'];
                
       $query = "INSERT INTO `subscribe` (`id`, `email`, `browser`, `date`) VALUES (NULL, '$this->email', '$browserAgent', '$now');";
       //echo($query);
       $obj_sub->query($query);
       //die();
    }

    
    public static function show_subscribe(){
        $obj_sub = self::obj_db();
        $query_select = "SELECT * FROM subscribe";
        $result = $obj_sub->query($query_select);
//        echo($query_select);
//        die();
        
        $data = array();
        while ($sub = $result->fetch_object())
        {
            $temp = new Subscribe();
            $temp->email = $sub->email; 
            $temp->browser = $sub->browser;
            $temp->date = $sub->date;
            $data[] = $temp;
        }
        return $data;
    }
    
}

<?php
require_once 'db_connection.php';
class Slider extends DB_Connection{
    private $slide_ID;
    private $slide_name;
    //private $slide_image_name;
   // private $slide_title;
    private $slide_link;
    private $slide_description;
    private $slide_image;
    
    //Setter
    
    public function __set($name, $value) {
        $method_name ="set_$name";
        if(!method_exists($this, $method_name))
        {
            throw new Exception("Set Property $name does not exist");
        }
        $this->$method_name($value);
    }
    //Getter
    public function __get($name) {
        $method_name = "get_$name";
        if(!method_exists($this, $method_name))
        {
            throw new Exception("GET Property $name does not exist");
        }
        return $this->$method_name();
    }
    private function set_slide_ID($slide_ID)
    {
        if(is_numeric($slide_ID) || $slide_ID <= 0)
        {
            throw new Exception("Invalid Slide ID");
        }
        $this->slide_ID = $slide_ID;
    }
    private function get_slide_ID()
    {
        return $this->slide_ID;
    }
    
    private function set_slide_name($slide_name)
    {
        $reg = "/^[a-z0-9 ]+$/i";
        if(!preg_match($reg, $slide_name))
        {
            throw new Exception("Invalid Slide Name");
        }
        $this->slide_name =$slide_name;
    }
    private function get_slide_name()
    {
        return $this->slide_name;
    }
    
    private function set_slide_link($slide_link)
    {
       // $reg = "/^(http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?$/";
       $reg = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
        if(!preg_match($reg, $slide_link))
        {
            throw new Exception("Invalid Slide Link");
        }
        $this->slide_link = $slide_link;
    }
    private function get_slide_link()
    {
        return $this->slide_link;
    }
    private function set_slide_description($slide_description)
    {
        
//        $reg = "/^[a-z ][a-z0-9 ]+$/i";
//        if(!preg_match($reg, $slide_description))
//        {
//            throw new Exception("Invalid Slide Description");
//        }
        $this->slide_description = $slide_description;
    }
    private function get_slide_description()
    {
        return $this->slide_description;
    }
    private function set_slide_image($slide_image)
    {
        if($slide_image['error'] == 4)
        {
            throw new Exception("Missing Image");
        }
        $image =  getimagesize($slide_image['tmp_name']);
        if(!$image)
        {
            throw new Exception("*Not a valid Image");
        }
        if($slide_image['size'] >50000000)
        {
            throw new Exception("*Max file size allowed 5MB");
        }
        if($slide_image['type'] != "image/jpeg" && $slide_image['type'] != "image/png")
        {
            throw new Exception("*Only allow jpeg image");
        }
        if($slide_image['type'] != $image['mime'])
        {
            throw new Exception("*Corrupt image");
        }
        if(is_null($this->slide_name))
        {
            throw new Exception("Failed to generate file name");
        }
        $path_info = pathinfo($slide_image['name']);
        extract($path_info);
        $this->slide_image = $this->slide_name .".".$extension;
        
    }
    private function get_slide_image()
    {
        return $this->slide_image;
    }
    public function upload_slide_image($source_path)
    {
        $str_path = "../slider_images/$this->slide_name/$this->slide_image";
        if(!is_dir("../slider_images"))
        {
            if(!mkdir("../slider_images"))
            {
                throw new Exception("*Failed to create folder../slider_images");
            }
        }
        if(!is_dir("../slider_images/$this->slide_name"))
        {
            if(!mkdir("../slider_images/$this->slide_name"))
            {
                throw new Exception("*Failed to create folder../slider_images/$this->slide_name");
            }
        }
        $result = @move_uploaded_file($source_path, $str_path);
        if(!$result)
        {
            throw new Exception("*Failed to upload Image");
        }
    }
    public function add_slider()
    {
        $obj_db = $this->obj_db();
        $query_insert = "insert into slider "
                . " (slide_id, slide_name, slide_link, slide_des, slide_image) "
                . " values (NULL, '$this->slide_name', '$this->slide_link', '$this->slide_description', '$this->slide_image')";
       
//        echo $query_insert;
//        die;
        $obj_db->query($query_insert);
        if($obj_db->errno)
        {
            throw new Exception("Slide Insert Error $obj_db->error - $obj_db->errno");
        }
        $this->slide_ID = $obj_db->insert_id;
    }
    public static function  get_slider()
    {
        $query_select = "select * from slider";
        $obj_db = self::obj_db();
        $result = $obj_db->query($query_select);
        $slider = array();
        while($data = $result->fetch_object())
        {
            $temp = new Slider();
            $temp->slide_ID = $data->slide_id;
            //$temp->slide_title = $data->slide_title;
            $temp->slide_link = $data->slide_link;
            $temp->slide_name = $data->slide_name;
            $temp->slide_description = $data->slide_des;
            $temp->slide_image = $data->slide_image;
            
            $slider[] = $temp;
            
        }
        return $slider;
        
    }
    public static function count_slides()
    {
//        $obj_db = self::obj_db();
//        $query_select = "select count (*) 'count' from slider";
//        $result = $obj_db->query($query_select);
//        $data = $result->fetch_object();
//        $total_slides = $data->count;
        
        $obj_db = self::obj_db();
        $query_select = "select count(*) 'count' from slider";
        $result =  $obj_db->query($query_select);
        $data = $result->fetch_object();
//        $total_product_rows = mysqli_fetch_array($result);
        $total_slides = $data->count;
        echo ($total_slides);
        
        if($total_slides > 5)
        {
            throw new Exception("Max 5 Slides Allow");
        }
    }
    //Get single slide
    public static function get_single_slide($keyslide)
    {
        $obj_db = self::obj_db();
        $query_select ="select * from slider "
                . " where slide_id = $keyslide ";
//        echo($query_select);
//        die;
        $result = $obj_db->query($query_select);
        if($obj_db->errno)
        {
            throw new Exception("Slide Not Select $obj_db->error - $obj_db->errno");
        }
        if($result->num_rows == 0)
        {
            throw new Exception("Slide(s) not found $obj_db->error - $obj_db->error");
        }
        $slides = array();
        while ($data = $result->fetch_object())
        {
            $temp =new self();
            $temp->slide_ID = $data->slide_id;
            $temp->slide_name = $data->slide_name;
            $temp->slide_image = $data->slide_image;
            $temp->slide_link = $data->slide_link;
            $temp->slide_description = $data->slide_des;
            
            $slides[] = $temp;
        }
        return $slides;
    }
    //Update Slider
    public function update_slider($keyslide)
    {
        $obj_db = $this->obj_db();
        $query_update ="update slider set "
                . " slide_link = '$this->slide_link', "
                . " slide_des = '$this->slide_description', "
                . " slide_image = '$this->slide_image' "
                . " where "
                . " slide_id = $keyslide ";
//        echo ($query_update);
//        die;
        $obj_db->query($query_update);
        if($obj_db->affected_rows == 0)
        {
            throw new Exception("Slide Upadate Error $obj_db->error - $obj_db->errno");
        }
    }


    //Delete slides
    public function delete_slide($key)
    {
        $obj_db = $this->obj_db();
        $query_delete = "delete from slider "
                . " where slide_id = '$key'";
//        echo $query_delete;
//        die;
        $obj_db->query($query_delete);
        if($obj_db->affected_rows == 0)
        {
            throw new Exception("Slide Not Deleted $obj_db->error - $obj_db->errno");
        }
    }
}

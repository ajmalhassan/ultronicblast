<?php
require_once 'db_connection.php';

function pagination()
{
    $pagination_buttons= 11;
    $page_number = (isset($_GET['page']) AND !empty($_GET['page']))? $_GET['page']:1;
    $per_page_records = 10;
    $rows             = 100;
    $last_page = ceil($rows/$per_page_records);
    //echo ($last_page);
    $pagination = "";
    $pagination .= "<nav aria-label='Page navigation'>";
    $pagination .= "<ul class='pagination'>";
    
    
    if($page_number <1 )
    {
        $page_number = 1;
    }
 elseif ($page_number > $last_page) {
        $page_number = $last_page;
    }
    echo("<h3>Showing Page: $page_number / $last_page</h3>");  
    echo $page_number;
    
}
?>
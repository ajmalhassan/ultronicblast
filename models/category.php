<?php
require_once 'db_connection.php';
class Category extends DB_Connection {
    private $categoryID;
    private $category_name;
    private $category_image_name;
    private $category_image;
    public function __construct() {
        
    }
    //Setter
    public function __set($name, $value) {
        $method_name = "set_$name";
        if(!method_exists($this, $method_name))
        {
            throw new Exception("SET Property of $name does not exist");
        }
        $this->$method_name($value);
    }
    //Gatter
    
    public function __get($name) {
        $method_name = "get_$name";
        if(!method_exists($this, $method_name))
        {
            throw new Exception("GET Property $name does not exist");
        }
        return $this->$method_name();
    }
    
    //Set categoryID
    private function set_categoryID($categoryID)
    {
        if(is_numeric($categoryID) || $categoryID <= 0)
        {
            throw new Exception("Category Id must be Numaric");
        }
        $this->categoryID = $categoryID;
    }
    //Get categortID
    private function get_categoryID()
    {
        return $this->categoryID;
    }
    //Set category Name
    private function set_category_name($category_name)
    {
        $reg = "/^[a-z ][a-z0-9 ]+$/i";
        if(!preg_match($reg, $category_name))
        {
            throw new Exception("Invalid Category Name");
        }
        $this->category_name = $category_name;
    }
    //Get category Name
    private function get_category_name()
    {
        return $this->category_name;
    }
    
    private function set_category_image_name($category_name)
    {
        
        //$reg = "/^[a-z ][a-z0-9 ]+$/i";
        $categoryn = $category_name;
        $reg = "/ /";
        $rep = "_";
        $name = preg_replace($reg, $rep, $categoryn);
        
        $this->category_image_name = $name;
    }
    //Get method category image name
    private function get_category_image_name()
    {
        return $this->category_image_name;
    }
    //Set method category_image
    private function set_category_image($category_image)
    {
        if($category_image['error'] == 4)
        {
            throw new Exception("*File Missing");
        }
        $image =  getimagesize($category_image['tmp_name']);
        if(!$image)
        {
            throw new Exception("*Not a valid Image");
        }
        if($category_image['size'] >50000000)
        {
            throw new Exception("*Max file size allowed 5MB");
        }
        if($category_image['type'] != "image/png")
        {
            throw new Exception("*Only allow png/jpg image");
        }
        if($category_image['type'] != $image['mime'])
        {
            throw new Exception("*Corrupt image");
        }
        if(is_null($this->category_name))
        {
            throw new Exception("Failed to generate file name");
        }
        $path_info = pathinfo($category_image['name']);
        extract($path_info);
        $this->category_image = $this->category_image_name .".".$extension;
    }
    //Get category_image
    private function get_category_image()
    {
        return $this->category_image;
    }
    public function upload_category_image($source_path)
    {
        $str_path = "../categories/$this->category_image_name/$this->category_image";
        if(!is_dir("../categories"))
        {
            if(!mkdir("../categories"))
            {
                throw new Exception("*Failed to create folder../categories");
            }
        }
        if(!is_dir("../categories/$this->category_image_name"))
        {
            if(!mkdir("../categories/$this->category_image_name"))
            {
                throw new Exception("*Failed to create folder../categories/$this->category_image_name");
            }
        }
        $result = @move_uploaded_file($source_path, $str_path);
        if(!$result)
        {
            throw new Exception("*Failed to upload Image");
        }
    }
    public function add_category()
    {
        $obj_db = $this->obj_db();
        $now = date("Y-m-d H:i:s");
        $query_insert = "insert into categories (category_id, category_name, category_image, register_time) "
                . " values (NULL, '$this->category_name', '$this->category_image', '$now')";
        $obj_db->query($query_insert);
        if($obj_db->errno)
        {
            throw new Exception("* Category inserted Error $obj_db->error - $obj_db->errno");
        }
        $this->categoryID = $obj_db->insert_id;
        
    }
    public static function get_categories($start, $count)
    {
        $obj_db = self::obj_db();
        $query_select = "select * from categories order by category_id desc";
        if ($start < 0) {
            $start = 0;
            }
            if ($count < 1) {
                $count = 1;
            }
            $query_select .= " limit $start, $count";
        $result = $obj_db->query($query_select);
        if($obj_db->errno)
        {
            throw new Exception("Select Categories Error $obj_db->error - $obj_db->errno");
        }
        $categories = array();
        while ($category = $result->fetch_object())
        {
            $tmp = new Category();
            $tmp->categoryID = $category->category_id;
            $tmp->category_name = $category->category_name;
            $tmp->category_image = $category->category_image;
            $categories[] = $tmp;
        }
        return $categories;
        
    }
    public static function get_cat()
    {
        $obj_db = self::obj_db();
        $query_select = "select * from categories order by category_id desc limit 3";
        
        $result = $obj_db->query($query_select);
        if($obj_db->errno)
        {
            throw new Exception("Select Categories Error $obj_db->error - $obj_db->errno");
        }
        $categories = array();
        while ($category = $result->fetch_object())
        {
            $tmp = new Category();
            $tmp->categoryID = $category->category_id;
            $tmp->category_name = $category->category_name;
            $tmp->category_image = $category->category_image;
            $categories[] = $tmp;
        }
        return $categories;
        
    }
    public static function get_category($keycat) {

        $obj_db = self::obj_db();

        $query = " select * from categories "
                . " where category_id='$keycat' ";

//        echo ($query);
//        die;
        $result = $obj_db->query($query);

        if ($obj_db->errno) {
            throw new Exception(" * Category selection Error - $obj_db->error -$obj_db->errno");
        }

        if ($result->num_rows == 0) {
            throw new Exception(" * Category(s) not found");
        }

        $categories = array();
        while ($category = $result->fetch_object())
        {
            $tmp = new Category();
            $tmp->categoryID = $category->category_id;
            $tmp->category_name = $category->category_name;
            $categories[] = $tmp;
        }
        return $categories;
    }
    
    //Update category
   public function update_category($keycat)
    {
        $obj_db = $this->obj_db();
        $query_update = "update categories set "
                . " category_name = '$this->category_name' "
                . " where "
                . " category_id = $keycat";
//        echo($query_update);
//        die;
//        
        $obj_db->query($query_update);
        if($obj_db->affected_rows ==0)
        {
            throw new Exception("Update Category Error $obj_db->error - $obj_db->errno");
        }
    }
    //Count total category 
    public static function total_categories()
    {
       $obj_db = self::obj_db();
       $query_select = "select count(*) 'count' from categories";
       $result = $obj_db->query($query_select);
       //$total_category_rows = mysqli_fetch_array($result);
       $data = $result->fetch_object();
       //$total_category = $total_category_rows[0];
        $total_category = $data->count;
       echo ($total_category);
       
    }
    //Delete category
    public function delete_category($key)
    {
        $obj_db = $this->obj_db();
        $query_delete = "delete from categories "
                . " where category_id = '$key'";
//        echo $query_delete;
//        die;
        $obj_db->query($query_delete);
        if($obj_db->affected_rows == 0)
        {
            throw new Exception("Category Not Deleted $obj_db->error - $obj_db->errno");
        }
    }
    public static function pagination($item_per_page)
    {
        $obj_db = self::obj_db();
        $query_select = "select count(*) 'count' from categories";
        
        $result = $obj_db->query($query_select);
        $data = $result->fetch_object();
        $total_categories = $data->count;
        $total_pages = ceil($total_categories/$item_per_page);
        $pNums = array();
        for($i = 1, $j = 0; $i <= $total_pages; $i++, $j += $item_per_page)
        {
            $pNums[$i] = $j;
        }
//        echo "<pre>";
//        print_r($pNums);
//        echo "</pre>";
//        die;
        return $pNums;
    }
}

<?php
require_once 'db_connection.php';
require_once 'DBTrait.php';
class Admin{
    use DBTrait;
    private $admin_ID;
    private $first_name;
    private $last_name;
    private $email;
    private $password;
    private $admin_name;
    private $profile_image;
    private $login_status;
    private $is_active;
    
    public function __construct() {
        
    }
    //Setter
    public function __set($name, $value) {
        $method_name = "set_$name";
        if(!method_exists($this, $method_name))
        {
            throw new Exception("SET Property $name does not exist");
        }
        $this->$method_name($value);
    }
    //Getter 
    
    public function __get($name) {
        $method_name = "get_$name";
        if(!method_exists($this, $method_name))
        {
            throw new Exception("Get Property $name does not exist");
        }
        return $this->$method_name();
    }
    //Private functions
    
    private function set_admin_ID($admin_ID)
    {
        if(!is_numeric($admin_ID) || $admin_ID <= 0)
        {
            throw new Exception("*Invalid AdminID");
        }
        $this->admin_ID = $admin_ID;
        
    }
    private function get_admin_ID()
    {
        return $this->admin_ID;
    }
    
    private function set_first_name($first_name)
    {
        $reg = "/^[a-z ]+$/i";
        if(!preg_match($reg, $first_name))
        {
            throw new Exception("Invalid First Name");
        }
        $this->first_name = $first_name;
    }
    
    private function get_first_name()
    {
        return $this->first_name;
    }
    
    private function set_last_name($last_name)
    {
        $reg = "/^[a-z ]+$/i";
        if(!preg_match($reg, $last_name))
        {
            throw new Exception("Invalid Last Name");
        }
        $this->last_name = $last_name;
    }
    
    private function get_last_name()
    {
        return $this->last_name;
    }
    
    private function set_admin_name($admin_name)
    {
        $reg = "/^[a-z ][a-z0-9 ]+$/i";
        if(!preg_match($reg, $admin_name))
        {
            throw new Exception("Invalid Admin Name");
        }
        $this->admin_name = $admin_name;
    }
    
    private function get_admin_name()
    {
        return $this->admin_name;
    }
    private function set_email($email)
    {
        $reg = "/^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zAZ]\.)+[a-zA-Z]{2,4})$/";
        if(!preg_match($reg, $email))
        {
            throw new Exception("Invalid Email");
        }
        $this->email = $email;
    }
    
    private function get_email()
    {
        return $this->email;
    }
    
    private function set_password($password)
    {
        $reg = "/^[a-z ][a-z0-9 ]+$/i";
        if(!preg_match($reg, $password))
        {
            throw new Exception("Invalid Password");
        }
        $this->password = sha1($password);
    }
    
    private function get_password()
    {
        return $this->password;
    }
    
    private function set_profile_image($profile_image)
    {
        if($profile_image['error'] == 4)
        {
            throw new Exception("*File Missing");
        }
        $image = getimagesize($profile_image['tmp_name']);
        if(!$image)
        {
            throw new Exception("* Not a valid Image");
        }
        if($profile_image['size'] > 5000000)
        {
            throw new Exception("*Max file size allow 500 K");
        }
        if($profile_image['type'] != "image/png")
        {
            throw new Exception("*Only png allowed");
        }
        if($profile_image['type'] != $image['mime'])
        {
            throw new Exception("* Curropt image");
        }
        $path_info = pathinfo($profile_image['name']);
        extract($path_info);
        $this->profile_image = $this->email.".".$extension;
    }
    
    private function get_profile_image()
    {
         $admin_image = $_SERVER['DOCUMENT_ROOT'] . "/ultronicblast/admin/admins/$this->email/$this->email.png";
        
        if(is_file($admin_image)){
            $img_uri = "http://" . $_SERVER['HTTP_HOST'] . "/ultronicblast/admin/admins/$this->email/$this->email.png";
        }
        else{
            $img_uri = "http://" . $_SERVER['HTTP_HOST'] . "/ultronicblast/admin/admins/no_img.gif";            
            // $img_uri = "http://" . $_SERVER['HTTP_HOST'] . "/ultronicblast/admin/admins/$this->admin_name/$this->admin_name.png";
        }
        return $img_uri;
    }

        public function upload_profile_image($source_path)
    {
        $str_path = "../admins/$this->email/$this->profile_image";
        if(!is_dir("../admins"))
        {
            if(!mkdir("../admins"))
            {
                throw new Exception("* Failed to create folder admins");
            }
        }
        if(!is_dir("../admins/$this->email"))
        {
            if(!mkdir("../admins/$this->email"))
            {
                throw new Exception("* Failed to create folder admins/$this->admin_name");
            }
        }
        $result = @move_uploaded_file($source_path, $str_path);
        if(!$result)
        {
            throw new Exception("* Failed to upload file");
        }
    }
    
    public function verify_retype_password($password, $retype_password)
    {
        if(empty($retype_password) || $password != $retype_password)
        {
            throw new Exception("Invalid Re-type Password");
        }
    }
    private function get_login()
    {
        return $this->login_status;
    }

        public function add_admin()
        {
            $obj_db = $this->obj_db();
//           
//                $reg = "/^[a-z0-9]{32}$/i";
//
//                if(!preg_match($reg, $act_code))
//                {
//                    throw new Exception("Invalid Activation Code");
//                }

                $now = date("Y-m-d H:i:s");
                $insert_query = "INSERT INTO admins"
                    . "(admin_id, first_name, last_name, admin_name, "
                    . " email, password, profile_image, reset_code, is_active, signup_date)"
                    . " VALUES "
                    . " (NULL, '$this->first_name', '$this->last_name', '$this->admin_name', '$this->email', "
                    . " '$this->password', '$this->profile_image', '$act_code', 0, '$now')";
        //        echo ($insert_query);
        //            die;
                $result = $obj_db->query($insert_query);
                if($obj_db->errno)
                {
                    throw new Exception("New Admin Inserted Error - $obj_db->error - $obj_db->errno");
                }
                $this->admin_ID = $obj_db->insert_id;
        }
        public function check_user($email){
        $obj_db = $this->obj_db();
        $query_select = "select email from admins "
                . " where email = '$email' ";
        $result = $obj_db->query($query_select);
        //$num_rows = affected_rows($result);
        if($obj_db->affected_rows)
        {
            throw new Exception("Email already exist");
        }
        
    }
        
//        public function user_activate($act_code)
//        {
//        $reg = "/^[a-z0-9]{32}$/i";
//        if(!preg_match($reg, $act_code))
//        {
//            throw new Exception("Invalid Activation Code");
//        }
//        $obj_db = $this->obj_db();
//        $select_query = "select admin_id, reset_code, signup_date, is_active from admins "
//                . " where admin_id = $this->admin_ID";
//        
////        echo $select_query;
////        die;
//        $result = $obj_db->query($select_query);
//        if($obj_db->errno)
//        {
//            throw new Exception("Select activete Admin Error - $obj_db->error - $obj_db->errno");
//        }
//        if(!$result->num_rows)
//        {
//            throw new Exception("Admin not found");
//        }
//        $data = $result->fetch_object();
//        if($data->is_active)
//        {
//            throw new Exception("Admin already activate");
//        }
//        if($data->reset_code != $act_code)
//        {
//            throw new Exception("Invalid Activation Link");
//        }
//        $now = time();
//        $expiry_time = strtotime($data->signup_date) + (60 * 60 * 24 * 3);
//        if($now > $expiry_time)
//        {
//            $query_delete = "delete from admins "
//                    . " where admin_id = $this->admin_ID ";
//            $obj_db->query($query_delete);
//            if($obj_db->errno)
//            {
//                throw new Exception("Delete Admin Error - $obj_db->error - $obj_db->errno");
//            }
//            if(!$obj_db->affected_rows)
//            {
//                throw new Exception("Falide to delete activete admin");
//            }
//        }
//        $query_update = "update admins set "
//                . " is_active = 1, "
//                . " reset_code = NULL "
//                . " where admin_id = $this->admin_ID";
//        $obj_db->query($query_update);
//        if($obj_db->errno)
//        {
//            throw new Exception("Update activate Admin Error - $obj_db->error - $obj_db->errno");
//        }
//        if(!$obj_db->affected_rows)
//        {
//            throw new Exception("Failed to upadte activte admin");
//        }
//        
//        }
        public function login($remember = FALSE)
        {
           $obj_db = $this->obj_db();
           
           $query_select = "select admin_id, email "
                   . " from admins "
                   . " where email = '$this->email' "
                   . " and password = '$this->password' ";
           $result = $obj_db->query($query_select);
           if($obj_db->errno)
           {
               throw new Exception("Select Login Admin Error - $obj_db->error - $obj_db->errno");
           }
           if(!$result->num_rows)
           {
               throw new Exception("Login Failed"); 
           }
           $data = $result->fetch_object();
           
           $this->email = $data->email;
           $this->password = $data->password;
           $this->admin_ID = $data->admin_id;
           $this->login_status = TRUE;
           $str_admin = serialize($this);
          // $_SESSION['obj_admin'] = serialize($obj_admin);
           $_SESSION['obj_admin'] = $str_admin;
           if($remember)
           {
               $expiry_time = time() + (60 * 60 * 24 * 15);
               setcookie("obj_admin", $str_admin, $expiry_time, "/");
           }
           
        }
        public function logout()
        {
            if(isset($_SESSION['obj_admin']))
            {
                unset($_SESSION['obj_admin']);
            }
            if(isset($_COOKIE['obj_admin']))
            {
                setcookie("obj_admin", "aaa", 1, "/");
            }
        }
        
        public function profile()
        {
            $obj_db = $this->obj_db();
            $query_select = "select * from admins "
                    . " where admin_id = '$this->admin_ID' ";
//            echo ($query_select);
//            die;
            $result = $obj_db->query($query_select);
            if($obj_db->errno)
            {
                throw new Exception("Select Admin error $obj_db->error - $obj_db->errno");
            }
            if(!$result->num_rows)
            {
                throw new Exception("* Admin(s) not Found");
            }
            $data = $result->fetch_object();
            
                $this->first_name = $data->first_name;
                $this->last_name = $data->last_name;
                $this->admin_name = $data->admin_name;
                $this->email  = $data->email;
                $this->profile_image = $data->profile_image;
        }
         public function edit_admin($id)
        {
            $obj_db = $this->obj_db();
            $query_select = "select * from admins "
                    . " where admin_id = $id ";
//            echo ($query_select);
//            die;
            $result = $obj_db->query($query_select);
            if($obj_db->errno)
            {
                throw new Exception("Select Admin error $obj_db->error - $obj_db->errno");
            }
            if(!$result->num_rows)
            {
                throw new Exception("* Admin(s) not Found");
            }
            $data = $result->fetch_object();
            if(!$data->is_active)
            {
                throw new Exception("Your account is Disabled - Contact Admin");
            }
                $this->first_name = $data->first_name;
                $this->last_name = $data->last_name;
                $this->admin_name = $data->admin_name;
                $this->email  = $data->email;
                $this->profile_image = $data->profile_image;
        }
        //Update change account
         public function update_account() {
        $query_update = "update admins set "
                . " first_name = '$this->first_name', "
                . " last_name = '$this->last_name', "
                . " admin_name = '$this->admin_name' "
                . " where admin_id = '$this->admin_ID' ";
        $obj_db = $this->obj_db();
//        echo ($query_update);
//        die;
        $result = $obj_db->query($query_update);
//        if($obj_db->affected_rows == 0)
//        {
//            throw new Exception("Not Update Admin $obj_db->error - $obj_db->errno");
//        }
    }
        
    public static function get_admin($start, $count){
        $obj_db = self::obj_db();
        $query_select ="select * from admins";
        if ($start < 0) {
            $start = 0;
            }
            if ($count < 1) {
                $count = 1;
            }
        $query_select .= " limit $start, $count";
        $result = $obj_db->query($query_select);
        $data = array();
        while($admin = $result->fetch_object()){
            $temp = new Admin();
            $temp->admin_id = $admin->admin_id;
            $temp->first_name = $admin->first_name;
            $temp->last_name = $admin->last_name;
            $temp->admin_name = $admin->admin_name;
            $temp->email = $admin->email;
            $temp->profile_image = $admin->profile_image; 
           $data[] = $temp;
        }
        return $data;
    }
    
    public function delete_admin($id){
        $obj_db = $this->obj_db();
        $query_del = "delete from admins where admin_ID = $id";
        $obj_db->query($query_del);
        
        
    }

    public static function pagination($item_per_page)
    {
        $obj_db = self::obj_db();
        $query_select = "select count(*) 'count' from admins";
        
        $result = $obj_db->query($query_select);
        $data = $result->fetch_object();
        $total_admin = $data->count;
        $total_pages = ceil($total_admin/$item_per_page);
        $pNums = array();
        for($i = 1, $j = 0; $i <= $total_pages; $i++, $j += $item_per_page)
        {
            $pNums[$i] = $j;
        }
//        echo "<pre>";
//        print_r($pNums);
//        echo "</pre>";
//        die;
        return $pNums;
    }
}

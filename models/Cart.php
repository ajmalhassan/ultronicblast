<?php
require 'Item.php';
class Cart {
    private $items;
    public function __construct() {
        $this->items = array();
    }
    public function __get($name) {
        $method = "get_$name";
        if (!method_exists($this, $method)) {
            throw new Exception("GET::Property $name does not exist");
        }
        return $this->$method();
    }
    private function get_Items() {
        return $this->items;
    }
     private function get_Count() {
        $total = 0;
        foreach ($this->items as $item) {
            $total += $item->quantity;
        }
        return $total;
    }
    private function get_Total() {
        $total = 0;
        foreach ($this->items as $item) {
            $total += $item->total;
        }
        return $total;
    }
    public function add_to_cart($item) {
        if (!$item instanceof Item) {
            throw new Exception("Not a valid Item Object");
        }
        if (array_key_exists($item->item_id, $this->items)) {
            $this->items[$item->item_id]->quantity += $item->quantity;
        } else {
            $this->items[$item->item_id] = $item;
        }
    }
    public function remove_item($item) {
        if (!$item instanceof Item) {
            throw new Exception("Not a valid Item Object");
        }
        if (array_key_exists($item->item_id, $this->items)) {
            unset($this->items[$item->item_id]);
        }
    }
     public function empty_cart() {
        $this->items = array();
        unset($_SESSION['obj_cart']);
    }
    public function update_cart($qtys) {
        
        foreach ($this->items as $item) {
//            if (is_numeric($qtys[$item->item_id])) {
//                
//                $item->quantity = $qtys[$item->item_id];
//            }

            try {
                if ($qtys[$item->item_id] === 0) {
                    $this->remove_item($item);
                } else {
                    $item->quantity = $qtys[$item->item_id];
                }
            } catch (Exception $ex) {
               
            }
        }

    }
}


<?php
require_once 'views/top.php';
require_once 'models/product.php';
require_once 'models/user.php';
require_once 'models/Order.php';
$obj_user->profile();
?>


<body>

<div class="super_container">
	<!-- Header -->
	<header class="header trans_300">
		<!-- Top Navigation -->
		<?php
                    require_once 'views/top_nav.php';
                   // <!-- Main Navigation -->
                    require_once 'views/main_nav.php';
                ?>
	</header>

	<div class="fs_menu_overlay"></div>
        <br><br><br><br><br><br><br><br>
<!--	<div class="hamburger_menu">-->
<?php
    require_once 'views/mobile_view.php';
?>
<div class="container">
    <div class="row">
        <?php
            require_once 'views/account_left_sidebar.php';
        ?>
<div class="col-md-9 "  id="contact_detail">
    <form action="controller/process_change_account.php" method="post" enctype="multipart/form-data">
    <div class="row">
        <div class="col-md-offset-1 col-md-12">
            <!--<h3 style="color: red;">Create account</h3>-->
            <h5 class="error-color">
                <?php
                    if(isset($_SESSION['msg']))
                    {
                        $msg = $_SESSION['msg'];
                        echo($msg);
                        unset($_SESSION['msg']);
                    }
                    if(isset($_SESSION['errors']))
                    {
                        $errors = $_SESSION['errors'];
                        unset($_SESSION['errors']);
                    }
                ?>
            </h5>
            <h4 style="border-bottom: 1px solid #ddd;">My Orders</h4>
           <div class="row">
            <div class="col-md-12 form-group">
                <?php
                echo("<table  class='table table-bordered table-responsive table-condensed'>
                       <tr>
                           
                           <th class='text-left'>Product Name</th>
                           <th class='text-left'>Quantity</th>
                           <th class='text-left'>Price</th>
                           <th class='text-left'>Image</th>
                           <th class='text-left'>Order Date</th>
                       </tr>");
                      $ab = Order::user_orders($obj_user->user_id);
                      foreach ($ab as $a)
                      {
                          $order_date = $a->order_date;
                          $total_price = $a->total_price;
                          $total_qty = $a->product_qty;
                      }
                          
                      
                   $order_product = product::user_order($obj_user->user_id);
                   foreach ($order_product as $product_order)
                   {
                       $product_path = pathinfo($product_order->product_image);
                       $product_image_name=$product_path['filename'];
                       echo("<tr>
                           
                           <td style=''>$product_order->product_name</td>
                           <td>$product_order->quantity</td>
                           <td>$total_price</td>
                           <th><img class='img-responsive' style='width:50px;' src='".BASE_URL."admin/products/$product_image_name/$product_order->product_image' alt=''/></th>
                            <th>$order_date</th>");
                            
                   }
                   echo("<tr></table>");
                   
                ?>
                
            </div>
        </div>
        </div>   
    </div>
  
</form>
                    
        </div>
  
    </div>
   
</div>
	

       <?php
                require_once 'views/news_letter.php';
                require_once 'views/footer.php';
        
	

	

	
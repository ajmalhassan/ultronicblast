
<div class="main_nav_container">
<div class="container">
        <div class="row">
                <div class="col-lg-12 text-right">
                        <div class="logo_container">
                            <!--<a href="<?php echo (BASE_URL);?>">Ultronic<span>Blast</span></a>-->
                            <a href="<?php echo (BASE_URL);?>">
                                <img style="width:200px;" src="images/logo.png"/>
                            </a>
                        </div>
                        <nav class="navbar">
                                <ul class="navbar_menu">
                                        <li>
                                           <div class="ui icon input loading topnav">
        <div class="search-container">
            <form action="result.php" method="get" enctype="multipart/form-data">
                <input type="text" name="user_query" value="" style="border: 1px solid #ddd; border-radius: 0px; width: 25em;" placeholder="Search...">
                <button style="visibility: hidden" type="submit" name="search"><i class="fa fa-search"></i></button>
           </form>
        </div>
    
</div>
                                        </li>
                                        <?php
                                           $categories = Category::get_cat();
                                           foreach ($categories as $cat)
                                           {
                                               ?>
                                               <li><a href="<?php echo(BASE_URL."categories.php?category_id=$cat->categoryID")?>"><?php echo ($cat->category_name)?></a></li>
                                        <?php   
                                        }
                                        ?>
                                        <!--<li><a href="<?php echo(BASE_URL);?>shop.php">shop</a></li>-->
                                </ul>
                                <ul class="navbar_user">
                                        
                                        <li class="checkout">
                                            <a href="<?php echo(BASE_URL);?>shopping_cart.php">
                                                <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                                <span id="checkout_items" class="checkout_items"><?php echo($obj_cart->count);?></span>
                                            </a>
                                        </li>
                                </ul>
                                <div class="hamburger_container">
                                        <i class="fa fa-bars" aria-hidden="true"></i>
                                </div>
                        </nav>
                </div>
        </div>
</div>
</div>
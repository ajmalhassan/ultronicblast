<?php
//$obj_user->profile();
?>
<div class="top_nav">
<div class="container">
    <div class="row">
        <div class="col-md-6">
                <div class="top_nav_left">free shipping on all Pakistan orders over Rs 1000</div>
        </div>
        <div class="col-md-6 text-right">
            <div class="top_nav_right">
                <ul class="top_nav_menu">

                        <!-- Currency / Language / My Account -->

<!--                    <li class="currency">
                        <a href="#">
                            usd
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="currency_selection">
                            <li><a href="#">cad</a></li>
                            <li><a href="#">aud</a></li>
                            <li><a href="#">eur</a></li>
                            <li><a href="#">gbp</a></li>
                        </ul>
                    </li>
                    <li class="language">
                        <a href="#">
                            English
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="language_selection">
                            <li><a href="#">French</a></li>
                            <li><a href="#">Italian</a></li>
                            <li><a href="#">German</a></li>
                            <li><a href="#">Spanish</a></li>
                        </ul>
                    </li>-->
                    
                      <?php
                      if($obj_user->login)
                      {
                        ?>
                       <li class="account">
                        <a href="#">
                            My Account
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="account_selection">
                            <li><a href="<?php BASE_URL?>my_account.php"><i class="fa fa-user" aria-hidden="true"></i>My Profile</a></li>
                            <li><a href="<?php BASE_URL?>my_orders.php"><i class="fa fa-cube" aria-hidden="true"></i>Orders</a></li>
                            
                            <li><a href="<?php BASE_URL?>controller/process_logout.php"><i class="fa fa-sign-out" aria-hidden="true"></i>Logout</a></li>
                        </ul>
                      </li>
                      <li class="account">
                          <?php $obj_user->profile();
                          ?>
                          <img src="<?php echo($obj_user->profile_image);?>" style="width: 30px; height: 30px; border-radius: 100px;"/>
                      </li>
                      <?php
                      }
                      else
                      {
                          ?>
                      <li class="account">
                          <a href="<?php BASE_URL?>login.php" class="fa fa-user-circle"> Login</a>
                      </li>
                      <?php
                      }
                      ?>
                </ul>
            </div>
        </div>
    </div>
</div>
</div>
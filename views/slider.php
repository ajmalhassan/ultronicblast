<?php
require_once 'models/slider.php';
$slides = Slider::get_slider();
?>
<div class="my-slider">
<?php
foreach ($slides as $s)
{
?>
 <?php
//            $product_path = pathinfo($s->slide_image);
//            $slide_image_name=$product_path['filename'];
            ?>
    <div class="main_slider" style="background-image:url(<?php echo ("admin/slider_images/$s->slide_name/$s->slide_image");?>)">
        <div class="container fill_height">
        <div class="row align-items-center fill_height">
            <div class="col">
                <div class="main_slider_content">
                    <h6><?php echo($s->slide_name);?></h6>
                    <h1><?php echo($s->slide_description);?></h1>
                    <div class="red_button shop_now_button"><a href="<?php echo($s->slide_link);?>" atl="<?php echo($s->slide_name);?>" target="_blank">shop now</a></div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
}
?>
</div>
<?php
session_start();
require_once 'models/user.php';
require_once 'models/Cart.php';
require_once 'models/category.php';
require_once 'models/Comment.php';
define("ITEM_PER_PAGE", 15);
define("BASE_FOLDER", "/ultronicblast/");
define("BASE_URL", "http://" . $_SERVER['HTTP_HOST'] . BASE_FOLDER);
if(isset($_SESSION['user_id']))
{
    $user_id = $_SESSION['user-id'];
}
    if(isset($_COOKIE['UltronicBlast']) && !isset($_SESSION['obj_user'])){
    $_SESSION['obj_user'] = $_COOKIE['UltronicBlast'];
}
if(isset($_SESSION['obj_user'])){
    $obj_user = unserialize($_SESSION['obj_user']);
}
else{
    $obj_user = new User();
}
if (isset($_SESSION['obj_cart'])) {
    $obj_cart = unserialize($_SESSION['obj_cart']);
} else {
    $obj_cart = new Cart();
}
$current = $_SERVER['PHP_SELF'];
//echo($current);

$public_pages = array(
    BASE_FOLDER . "login.php",
    BASE_FOLDER . "register.php",
);

$restricted_pages = array(
    BASE_FOLDER . "my_account.php",
    BASE_FOLDER . "check_out.php",
);

if($obj_user->login && in_array($current, $public_pages)){
    $_SESSION['msg'] = "You must logout to view this page";
    
    header("Location:" . BASE_URL . "msg.php");
}

if(!$obj_user->login && in_array($current, $restricted_pages)){
    //$_SESSION['msg'] = "You must login to view this page";
    header("Location:" . BASE_URL . "login.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Ultronic Blast</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Colo Shop Template">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--<link rel="stylesheet" href="styles/form_style.css" type="text/css"/>-->
<link href="styles/bootstrap4/bootstrap.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="styles/bootstrap4/bootstrap.min.css">
<link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/animate.css">
<link rel="stylesheet" href="plugins/themify-icons/themify-icons.css">
<link rel="stylesheet" type="text/css" href="plugins/jquery-ui-1.12.1.custom/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="styles/contact_styles.css">
<link rel="stylesheet" type="text/css" href="styles/contact_responsive.css">
<link rel="stylesheet" type="text/css" href="styles/categories_styles.css">
<link rel="stylesheet" type="text/css" href="styles/categories_responsive.css">
<link rel="stylesheet" type="text/css" href="styles/main_styles.css">

<link rel="stylesheet" type="text/css" href="styles/single_styles.css">
<link rel="stylesheet" type="text/css" href="styles/responsive.css">
<link href="plugins/slider/slick/slick.css" rel="stylesheet" type="text/css"/>
<link href="plugins/slider/slick/slick-theme.css" rel="stylesheet" type="text/css"/>
<link rel="icon" type="image/icon" href="images/Favicon1.ico" />

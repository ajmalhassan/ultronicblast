<div class="new_arrivals">
    <div class="container">
        <div class="row">
            <div class="col text-center">
                <div class="section_title new_arrivals_title">
                    <h2>Popular Products</h2>
                        
                </div>
            </div>
            
        </div>
        
<div class="row">
    <div class="col">
        <div class="product-grid" data-isotope='{ "itemSelector": ".product-item", "layoutMode": "fitRows" }'>

                <!-- Product 1 -->

            <?php
//            $type = isset($_GET['type']) ? $_GET['type']:"all";
//            $start = isset($_GET['start']) ? $_GET['start'] : 0;
//            $count = isset($_GET['count']) ? $_GET['count'] : 10;
        $products = product::popular_products();
        
        foreach ($products as $p)
        {
            $product_path = pathinfo($p->product_image);
            $product_image_name=$product_path['filename'];
            echo ("
       <div class='product-item men'>
    <div class='product discount product_filter' style='border-right: 2px solid red;'>
        <div class='product_image'>
        <a href='".BASE_URL."detail.php?product_id=$p->productID' target='_blank'>
            <img src='".BASE_URL."admin/products/$product_image_name/$p->product_image' alt='$p->product_name'>
                </a>
        </div>
        <div class='favorite favorite_left'></div>
        <div class='product_bubble product_bubble_right product_bubble_red d-flex flex-column align-items-center'><span>-%$p->product_discount</span></div>
        <div class='product_info'>
            <h6 class='product_name'><a href='".BASE_URL."detail.php?product_id=$p->productID' target='_blank'>$p->product_name</a></h6>
            <div class='product_price'>Rs.$p->sale_price<span>Rs.$p->unit_price</span></div>
        </div>
    </div>
    
    <div class='red_button add_to_cart_button  ' style='margin-left:0px;width:100%;'>
        <form action='".BASE_URL."controller/process_cart.php' method='post'>
            <input type='hidden' name='action' value='add_to_cart'/>
            <input type='hidden' name='product_id' value='$p->productID'/>
            <input class='cart_button' type='submit' value='add to cart'/>    
        </form>
    </div>
</div> 
 ");
        }
            ?>

        </div>
        <a href="<?php echo (BASE_URL);?>shop.php" class="btn btn-group-sm btn-danger pull-right" >View all</a>
    </div>
</div>
    </div>
</div>
<div class="col-md-2 custom-menu ">
            <h5 class="border-bottom" style="border-bottom:1px solid #ddd">My Account</h5>
           <ul class="list-unstyled text-uppercase account-list">
               <li class="fa fa-angle-double-right"><a id="account_setting" href="my_account.php"> Account Setting</a></li>
               <li class="fa fa-angle-double-right"><a id="personal_info" href="change_account.php"> Personal Information</a></li>
               <li class="fa fa-angle-double-right"><a id="my_orders" href="my_orders.php"> My Orders</a></li>
           </ul>
        </div>
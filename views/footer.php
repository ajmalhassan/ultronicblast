<footer class="footer" style="background-color: #ddd;">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
                                    <div class="footer_nav_container d-flex flex-sm-row flex-column align-items-center justify-content-lg-start justify-content-center text-center">
						<ul class="footer_nav">
                                                    <li><a href="http://localhost:8080/ultronicblastblog/index.php" target="_blank">Blog</a></li>
                                                        <li><a href="<?php echo(BASE_URL);?>ub-faq/faqs.php" target="_blank">FAQs</a></li>
							<li><a href="<?php echo(BASE_URL);?>contact.php">Contact us</a></li>
						</ul>
					</div>
				</div>
                            
				<div class="col-lg-6">
					<div class="footer_social d-flex flex-row align-items-center justify-content-lg-end justify-content-center">
						<ul>
							<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
							<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
							<li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
							<li><a href="#"><i class="fa fa-skype" aria-hidden="true"></i></a></li>
							<li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
                                    <div class="footer_nav_container" style="height: 40px;">
						<div class="cr">©2018 All Rights Reserverd. This website is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="<?php echo(BASE_URL);?>">UltronicBlast</a></div>
					</div>
				</div>
			</div>
		</div>
	</footer>

</div>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/Isotope/isotope.pkgd.min.js"></script>
<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="js/custom.js"></script>
<!--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDGLxRQbaSfyz5Koxy34ugSvs32m4CSjhk"></script>-->
<script src="plugins/jquery-ui-1.12.1.custom/jquery-ui.js"></script>
<!--<script src="js/contact_custom.js"></script>-->
<script src="js/single_custom.js"></script>
<script src="js/categories_custom.js"></script>
<script src="plugins/slider/slick/slick.js" type="text/javascript"></script>
<script type="text/javascript">
    var $j = jQuery.noConflict();
    $j(document).ready(function(){
      $j('.my-slider').slick({
        dots:false,
        arrow:true,
        autoplay:true,
        autoplaySpeed:2000
      });
    });
  </script>
  <script type="text/javascript">
    var $i = jQuery.noConflict();
    $i(document).ready(function(){
      $i('.my-brands').slick({
        slidesToShow: 6,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
      arrows:true
      });
    });
  </script>
<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
  <script type="text/javascript">
    $(document).ready(function(){
        $(window).load(function() {
		// Animate loader off screen
		$(".pageloader").fadeOut("fast");;
	});
    });
        
  </script>-->
  <script type='text/javascript' src='//platform-api.sharethis.com/js/sharethis.js#property=5ae46eedde20620011e03410&product=sticky-share-buttons' async='async'></script>

    
</body>
<!--<div class="pageloader"></div>-->
</html>

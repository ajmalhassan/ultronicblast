<?php
require_once 'models/category.php';
require_once 'models/product.php';
?>
<div class="banner">
    <div class="container">
        <div class="row">
            <?php
            $categories = Category::get_cat();
            foreach ($categories as $category)
            {
                $category_path = pathinfo($category->category_image);
                $category_image_name=$category_path['filename'];
                echo ("<div class='col-md-4'>
                <div class='banner_item align-items-center' style='background-image:url(admin/categories/$category_image_name/$category->category_image)'>
                    <div class='banner_category'>
                        <a href='".BASE_URL."categories.php?category_id=$category->categoryID' target='_blank'>$category->category_name</a>
                    </div>
                </div>
            </div>");
            }
            ?>
        </div>
    </div>
</div>
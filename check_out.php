<?php
require_once 'views/top.php';
require_once 'models/Cart.php';
require_once 'models/product.php';
require_once 'models/user.php';
?>
<body>

<div class="super_container">
	<!-- Header -->
	<header class="header trans_300">
		<!-- Top Navigation -->
		<?php
                    require_once 'views/top_nav.php';
                   // <!-- Main Navigation -->
                    require_once 'views/main_nav.php';
                ?>
	</header>

	<div class="fs_menu_overlay"></div>
        <br><br><br><br><br><br><br><br>
<!--	<div class="hamburger_menu">-->
<?php
                require_once 'views/mobile_view.php';
        ?>
<div class="container">
    <div class="row">
        <?php
            //require_once 'views/account_left_sidebar.php';
        ?>
<div class="col-md-9 "  id="contact_detail">
    <form action="controller/process_order.php" method="post" enctype="multipart/form-data">
    <div class="row">
        <div class="col-md-offset-1 col-md-8">
            <!--<h3 style="color: red;">Create account</h3>-->
            <h5 class="error-color">
                <?php
                
                    if(isset($_SESSION['msg']))
                    {
                        $msg = $_SESSION['msg'];
                        echo($msg);
                        unset($_SESSION['msg']);
                    }
                    if(isset($_SESSION['errors']))
                    {
                        $errors = $_SESSION['errors'];
                        unset($_SESSION['errors']);
                    }
                    if(isset($_SESSION['obj_user']))
                    {
                        $obj_user = unserialize($_SESSION['obj_user']);
                    }
                    else
                    {
                        $obj_user = new User();
                    }
                    $obj_user->profile();
                ?>
            </h5>
            <h4 style="border-bottom: 1px solid #ddd;">Place Order</h4>
           <div class="row">
            <div class="col-md-12 form-group">
              <label>Customer Name</label>
              <input type="text" name="user_name" value="<?php echo($obj_user->user_name);?>" class="form-control control-form focusedInput" placeholder="Customer Name">
              <span>
                  <?php
                  if(isset($errors['user_name']))
                  {
                      echo ($errors['user_name']);
                  }
                  ?>
              </span>
            </div>
              
            <div class="col-md-12 form-group">
                <label>Email  </label>&nbsp;&nbsp;
                <input type="email" name="email" value="<?php echo($obj_user->email);?>" class="form-control control-form focusedInput" placeholder="Customer Email">
              <span>
                  <?php
                  if(isset($errors['email']))
                  {
                      echo ($errors['email']);
                  }
                  ?>
              </span>
            </div>
                <div class="col-md-12 form-group">
                <label>Mobile Number</label>
                <input type="text" name="mobile_number" value="<?php echo($obj_user->mobile_number);?>" class="form-control control-form focusedInput" placeholder="+92">
                <span>
                  <?php
                  if(isset($errors['mobile_number']))
                  {
                      echo ($errors['mobile_number']);
                  }
                  ?>
              </span>
            </div>
                <div class="col-md-12 form-group">
                <label>Shipping Address</label>
                <textarea name="shipping_address" cols="72"><?php echo($obj_user->address);?></textarea>
                <span>
                  <?php
                  if(isset($errors['shipping_address']))
                  {
                      echo ($errors['shipping_address']);
                  }
                  ?>
              </span>
            </div>
            <div class="col-md-6 form-group">
                <input type="submit" class="btn btn-sm btn-danger" value="Place Order">
                
            </div>
        </div>
        </div>   
    </div>
  
</form>
                    
        </div>
  
    </div>
   
</div>
	

       <?php
                require_once 'views/news_letter.php';
                require_once 'views/footer.php';
        
	

	

	
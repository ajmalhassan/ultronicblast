<?php
require_once 'views/top.php';
require_once 'models/product.php';
require_once 'models/user.php';
$obj_user->profile();
?>


<body>

<div class="super_container">
	<!-- Header -->
	<header class="header trans_300">
		<!-- Top Navigation -->
		<?php
                    require_once 'views/top_nav.php';
                   // <!-- Main Navigation -->
                    require_once 'views/main_nav.php';
                ?>
	</header>

	<div class="fs_menu_overlay"></div>
        <br><br><br><br><br><br><br><br>
<!--	<div class="hamburger_menu">-->
<?php
    require_once 'views/mobile_view.php';
?>
<div class="container">
    <div class="row">
        <?php
            require_once 'views/account_left_sidebar.php';
        ?>
        <div class="col-md-9 col-md-offset-1"  id="contact_detail">
            <h5 style="border-bottom: 1px solid #ddd;">Hello <?php echo($obj_user->user_name);?></h5>
            <div class="row">
                <div class="col-md-6">
                    <div class="card bg-light mb-3">
                        <div class="card-header">Contact details</div>
                        <div class="card-body">
                          <h5 class="card-title"><?php echo ($obj_user->user_name);?>
                              <img style="border-radius: 50%; width: 80px; height: 80px; margin-top: -13px; margin-right: -15px;" class="pull-right" src="<?php echo($obj_user->profile_image);?>"  />
                          </h5>
                         
                            <p class="card-text"><span class="error-color">Email: </span><?php echo ($obj_user->email);?></p>
                            <p class="card-text"><span class="error-color">Tel#: </span><?php echo ($obj_user->mobile_number);?></p>
                            <p class="card-text"><span class="error-color">Address: </span><?php echo ($obj_user->address);?></p>
                          <a href="change_account.php" class="card-link"><i class="fa fa-edit"></i>Edit</a>
                          <a href="change_password.php" class="card-link"><i class="fa fa-edit"></i>Change Password</a>
                        </div>
                    </div>
                </div>
            </div>
                    
        </div>
  
    </div>
   
</div>
	

       <?php
                require_once 'views/news_letter.php';
                require_once 'views/footer.php';
        
	

	

	
<?php
require_once "views/top.php";
require_once '../models/admin.php';

?>
</head>
<body>
<div id="wrapper">
<?php
   $obj_admin->profile();        
?>
<!----->
        <nav class="navbar-default navbar-static-top" role="navigation">
             <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
               <h1> <a class="navbar-brand" href="index.php">Ultronic Blast</a></h1>
               <div class="clearfix"></div>
            </div>
			 <div class=" border-bottom">
        	<?php
                require_once 'views/header.php';
                ?>
                             <div class="clearfix"></div>
                <?php
                //require_once 'views/navbar.php';
                require_once './views/left_sidebar_nav.php';
                ?>
			
        </nav>

        <div id="page-wrapper" class="gray-bg dashbard-1">
       <div class="content-main">
            <div class="validation-system">
               <div class="validation-form">
           <h1><i class="fa fa-user-secret"></i> Profile <small>Edit Profile</small></h1><hr style="margin-top: 10px; margin-bottom: 10px;">
                        <ol class="breadcrumb">
                            <li class=""><i class="fa fa-dashboard"></i> Dashboard</li>
                            <li class="active"><i class="fa fa-user-secret"></i> Edit Profile</li>
                        </ol>
           
<div class=" profile">
       <div class="row">
           
           <div class="col-md-8">
               <span style="color: green;">
                        <?php
                                if(isset($_SESSION['msg']))
                                {
                                    $msg = $_SESSION['msg'];
                                    echo ($msg);
                                    unset($_SESSION['msg']);
                                }
                                ?>
               </span>
               <span style="color:red;">
                   <?php
                                if(isset($_SESSION['errors']))
                                {
                                    $errors = $_SESSION['errors'];
                                    unset($_SESSION['errors']);
                                }
                            ?>
               </span>
              <form action="controller/process_edit_profile.php" method="post" enctype="multipart/form-data">
                  <div class="form-group">
                    <label>First Name</label>
                    <span class="pull-right text-danger">
                        <?php
                            if(isset($errors['first_name']))
                            {
                                echo ($errors['first_name']);
                            }
                        ?> 
                    </span>
                    <input type="text" name="first_name" value="<?php echo ($obj_admin->first_name);?>" class="form-control" placeholder="First Name"/>
                 </div>
                  <div class="form-group">
                    <label>Last Name</label>
                    <span class="pull-right text-danger">
                        <?php
                            if(isset($errors['last_name']))
                            {
                                echo ($errors['last_name']);
                            }
                        ?> 
                    </span>
                    <input type="text" name="last_name" value="<?php echo($obj_admin->last_name);?>" class="form-control" placeholder="Last Name"/>
                 </div>
                    <div class="form-group">
                        <label>Admin Name</label>
                        <span class="pull-right text-danger">
                            <?php
                                if(isset($errors['admin_name']))
                                {
                                    echo ($errors['admin_name']);
                                }
                               ?>
                        </span>
                        <input type="text" name="admin_name" value="<?php echo ($obj_admin->admin_name);?>" class="form-control" placeholder="User Name"/>
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <span class="pull-right text-danger">
                        
                        </span>
                        <input type="email" name="email" value="<?php echo($obj_admin->email);?>" class="form-control" placeholder="Email" disabled=""/>
                    </div>
                  <input type="submit" value="Update" class="btn btn-primary"/>
                </form>
          </div>
          <div class="col-md-4">
              <img class="img-responsive img-circle img-thumbnail" src="<?php echo($obj_admin->profile_image);?>"/>
              <br><br>
              <form action="controller/process_update_image.php" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <input type="file" name="profile_image" class="form-control" value=""/>
                        <span>
                            <?php
                                if(isset($errors['profile_image']))
                                    {
                                    echo ($errors['profile_image']);
                                    }
                            ?>
                        </span>
                    </div>
                  <p class="text-danger">
                      <?php
                        if(isset($_SESSION['msg_img']))
                        {
                            $msg_img = $_SESSION['msg_img'];
                            echo ($msg_img);
                            unset($_SESSION['msg_img']);
                        }
                        if(isset($_SESSION['errors']))
                        {
                            $errors = $_SESSION['errors'];
                            unset($_SESSION['errors']);
                        }
                    ?>
                  </p>
                  
                  <input class="btn btn-danger form-control" type="submit" value="Update Image"/>
                  
                    
                </form>
          </div>
            
        </div> 
    </div>
               </div>
            </div>
  		
	
<?php
require_once 'views/footer.php';


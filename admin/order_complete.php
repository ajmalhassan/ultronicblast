<?php
//session_start();
require_once "views/top.php";
require_once '../models/product.php';
require_once '../models/Order.php';
require_once 'views/profile.php';
?>
<link href="css/print.css" rel="stylesheet" media="print" type="text/css"/>
<style>
    .table>tbody>tr>th
    {
       padding: 10px !important;
       
    }
   
</style>
</head>
<body>
<div id="wrapper">

<!----->
        <nav class="navbar-default navbar-static-top" role="navigation">
             <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
               <h1> <a class="navbar-brand" href="index.php">Ultronic Blast</a></h1>         
            </div>
			 <div class=" border-bottom">
        	<?php
                //require_once 'views/searchbar.php';
                require_once 'views/header.php';
                ?>
                <div class="clearfix"></div>
                <?php
                require_once 'views/left_sidebar_nav.php';
                ?>
                <div class="clearfix"></div>
		
        </nav>
<div class="clearfix"></div>
        <div id="page-wrapper" class="gray-bg dashbard-1">
       <div class="content-main">
           <div class="validation-system">
                <div class="validation-form">
                    <h1><i class="fa fa-opencart"></i> Orders <small>View all Orders</small></h1><hr style="margin-top: 10px; margin-bottom: 10px;">
                        <ol class="breadcrumb">
                            <li class=""><i class="fa fa-dashboard"></i> Dashboard</li>
                            <li class="active"><i class="fa fa-opencart"></i> All Order</li>
                        </ol>
                   <h2 class="h2">Completed Orders
                        <span class="error-color">
                            <?php 
                            if(isset($_SESSION['msg']))
                            {
                                $msg = $_SESSION['msg'];
                                echo ($msg);
                                unset($_SESSION['msg']);
                            }
                                
                            ?>
                        </span>
                    </h2> 
 <div class="table-responsive" ><!-- table-responsive Starts -->

<table class="table table-bordered table-condensed table-all" ><!-- table table-bordered table-hover table-striped Starts -->

<thead><!-- thead Starts -->

<tr style="background-color: #337ab7; color: #fff;">
<th>Order No</th>
<th>User Name</th>
<th>Email</th>

<th>Product Qty</th>
<th>Total Price</th>
<th>Status</th>
</tr>
</thead><!-- thead Ends -->

<tbody><!-- tbody Starts -->

<?php

    $start = isset($_GET['start']) ? $_GET['start'] : 0;
    $count = isset($_GET['count']) ? $_GET['count'] : ITEM_PER_PAGE;
    $orders = Order::get_complete_orders($start, $count);
 
 foreach ($orders as $order)
 {
   echo("<tr>"
    ."<td>$order->order_no</td>"
    ."<td>$order->user_name</td>"
    ."<td>$order->email</td>"
    ."<td>$order->total_quantity</td>"
    ."<td>$order->total_price</td>"
    ."<td>Completed</td>"
    ."</tr>");  
 }

?>

</tbody><!-- tbody Ends -->


</table><!-- table table-bordered table-hover table-striped Ends -->
<nav aria-label="brand-nav">
                <ul class="pagination">
                  <?php
                    $pNums = Order::pagination_order(ITEM_PER_PAGE);
                     foreach ($pNums as $pNo=>$start)
                     {
                        echo("<li class='page-link'><a href='" . BASE_URL . "order_complete.php?start=$start'>$pNo <span class='sr-only'>(current)</span></a></li>");
                     }
                     
                 ?>
                </ul>
            </nav>
                  
                       </div>
               </div>
               
           </div>
  		
	
<?php
require_once 'views/footer.php';
?>
<script>
  function printData()
{
   var divToPrint=document.getElementById("printTable");
   newWin= window.open("");
   newWin.document.write(divToPrint.outerHTML);
   newWin.print();
   newWin.close();
}
$('button').on('click',function(){
printData();
});
</script>


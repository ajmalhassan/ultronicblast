<?php
//session_start();
require_once '../models/product.php';
require_once '../models/category.php';
require_once "views/top.php";

?>
<script type="text/javascript">
    $(document).ready(function(){
        $(".string-validate").limitkeypress({ rexp: /^[A-Za-z0-9 ]*$/});
        $(".number-validate").limitkeypress({ rexp: /^[0-9]*$/});
    });
</script>
</head>
<body>
<div id="wrapper">

<!----->
        <nav class="navbar-default navbar-static-top" role="navigation">
             <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
               <h1> <a class="navbar-brand" href="index.php">Ultronic Blast</a></h1>         
            </div>
			 <div class=" border-bottom">
        	<?php
                //require_once 'views/searchbar.php';
                require_once 'views/header.php';
                ?>
			<div class="clearfix"></div>
	  
		    <?php
                    require_once 'views/left_sidebar_nav.php';
                    ?>
        </nav>
        <div id="page-wrapper" class="gray-bg dashbard-1">
       <div class="content-main">
 
  	<div class="validation-system">
 		
 		<div class="validation-form">
        <h2>Update Category 
            <span class="error-color">
                <?php
             if(isset($_SESSION['msg']))
            {
                $msg = $_SESSION['msg'];
                echo ($msg);
                unset($_SESSION['msg']);
            }
        ?>
            </span>
        
        </h2>
        <?php
            if(isset($_SESSION['errors']))
            {
                $errors = $_SESSION['errors'];
                unset($_SESSION['errors']);
            }
            if(isset($_SESSION['obj_category']))
            {
                $obj_category= unserialize($_SESSION['obj_category']);
            }
            else
            {
                $obj_category = new Category();
                
            }
        ?>
        <?php
        try {
             $keycat = $_GET['categoryID'];
//            echo ($keycat);
//            die;
            $category = Category::get_category($keycat);
    foreach ($category as $c)
        {
            echo ("
             <form method='post' action='controller/update_category_process.php?categoryID=$c->categoryID' enctype='multipart/form-data'>
            <div class='vali-form'>
                <div class='col-md-6 form-group1'>
                  <label class='control-label'>Category Name</label>
                  <input type='text' placeholder='Category Name' class='string-validate' name='category_name' value='$c->category_name' required=''>
                  <p class='error-color'>
                      
                      
                  </p>
                </div>
                <div class='clearfix'> </div>
            </div>
            
            <div class='col-md-12 form-group'>
              <button type='submit' class='btn btn-primary'>Update Category</button>
            </div>
          <div class='clearfix'> </div>
        </form>
        ");
        }
        } catch (Exception $ex) {
            echo $ex->getMessage();
        }

        
        ?>
        
    
 	<!---->
 </div>

</div>	
	
<?php
require_once 'views/footer.php';


<?php
//session_start();
require_once "views/top.php";
require_once '../models/product.php';
?>
</head>
<body>
<div id="wrapper">

<!----->
        <nav class="navbar-default navbar-static-top" role="navigation">
             <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
               <h1> <a class="navbar-brand" href="index.php">Ultronic Blast</a></h1>         
            </div>
			 <div class=" border-bottom">
        	<?php
                //require_once 'views/searchbar.php';
                require_once 'views/header.php';
                require_once './views/left_sidebar_nav.php';
                ?>
			<div class="clearfix"></div>
        </nav>
        <div id="page-wrapper" class="gray-bg dashbard-1">
       <div class="content-main">
           <div class="blank">
               <div class="blank-page">
                   <?php
                   if(isset($_SESSION['msg']))
                   {
                       $msg = $_SESSION['msg'];
                       echo ($msg);
                       unset($_SESSION['msg']);
                   }
                   if(isset($_SESSION['msg_err']))
                   {
                       $msg_err = $_SESSION['msg_err'];
                       echo ($msg_err);
                       unset($_SESSION['msg_err']);
                   }
                   ?>
                   <a href="addproduct.php" class="btn btn-primary">Go Back</a>
               </div>
           </div>
  		
	
<?php
require_once 'views/footer.php';
?>


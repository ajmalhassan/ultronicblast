<?php
require_once "views/top.php";
require_once '../models/slider.php';
require_once 'views/profile.php';
?>
</head>
<body>
<div id="wrapper">

<!----->
        <nav class="navbar-default navbar-static-top" role="navigation">
             <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
               <h1> <a class="navbar-brand" href="index.php">Ultronic Blast</a></h1>         
            </div>
			 <div class=" border-bottom">
        	<?php
                require_once 'views/header.php';
                ?>
                   <div class="clearfix"></div>
                <?php
                require_once 'views/left_sidebar_nav.php';
                ?>
			<div class="clearfix"></div>
        </nav>
        <div id="page-wrapper" class="gray-bg dashbard-1">
       <div class="content-main">
           <div class="validation-system">
               <div class="validation-form">
                    <h1><i class="fa fa-sliders"></i> Slider <small>Add New Slide</small></h1><hr style="margin-top: 10px; margin-bottom: 10px;">
                        <ol class="breadcrumb">
                            <li><i class="fa fa-dashboard"></i> Dashboard</li>
                            <li class="active"><i class="fa fa-sliders"></i> New Slide</li>
                        </ol>
           <h2>Ultronic Blast Slider
               <span class="error-color"> 
                   <?php
                    if(isset($_SESSION['msg']))
                    {
                        $msg = $_SESSION['msg'];
                        echo ($msg);
                        unset($_SESSION['msg']);
                    }
                    if(isset($_SESSION['errors']))
                    {
                        $errors = $_SESSION['errors'];
                        unset($_SESSION['errors']);
                    }
                    if(isset($_SESSION['obj_slider']))
                    {
                        $obj_slider = unserialize($_SESSION['obj_slider']);
                        unset($_SESSION['obj_slider']);
                    }
                    else
                    {
                        $obj_slider = new Slider();
                    }
                   ?>
               </span>
           </h2>
 <form action="controller/slider_process.php" method="post" enctype="multipart/form-data">
    <div class="vali-form">
        <div class="col-md-6 form-group1">
          <label class="control-label">Slide Name</label>
          <input type="text" placeholder="Slide Name" name="slide_name" value="<?php echo ($obj_slider->slide_name);?>" required="">
          <p class="error-color">
              <?php
                if(isset($errors['slide_name']))
                {
                    echo ($errors['slide_name']);
                }
              ?>
          </p>
        </div>

        <div class="clearfix"> </div>
    </div>
           <div class="col-md-12 form-group1 form-last">
          <label class="control-label">Button Link</label>
          <input type="text" placeholder="http://example.com" class="" name="slide_link" value="<?php echo ($obj_slider->slide_link);?>" required="">
          <p class="error-color">
              <?php
                if(isset($errors['slide_link']))
                {
                    echo ($errors['slide_link']);
                }
              ?>
          </p>
        </div>
       
     <div class="clearfix"> </div>
    <div class="col-md-12 form-group1 ">
      <label class="control-label">Slide Description</label>
      <textarea  placeholder="Slide Description" name="slide_description" class="" red="" cols="120"><?php echo($obj_slider->slide_description);?></textarea>

      <p class="error-color">
          <?php
            if(isset($errors['slide_description']))
                {
                    echo ($errors['slide_description']);
                }
          ?>
      </p>
    </div>
     <div class="clearfix"> </div>

     <div class="col-md-6 form-group">
         <input type="file" id="slide_image" value="" name="slide_image"/>
         <p class="error-color">
             <?php
                   if(isset($errors['slide_image']))
                   {
                       echo ($errors['slide_image']);
                   }
              ?>
         </p>
    </div>
    <div class="col-md-12 form-group">
      <button type="submit" class="btn btn-primary">Add Slide</button>
      <input type="reset" class="btn btn-default"/>
    </div>
  <div class="clearfix"> </div>
 </form>
       
               </div>
           </div>
  		
	
<?php
require_once 'views/footer.php';


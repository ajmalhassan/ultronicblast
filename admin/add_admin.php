<?php
require_once '../models/admin.php';
require_once '../models/db_connection.php';
require_once 'views/top.php';
require_once './views/profile.php';
?>
</head>
<body>
<div id="wrapper">
<!----->
        <nav class="navbar-default navbar-static-top" role="navigation">
             <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
               <h1> <a class="navbar-brand" href="index.php">Ultronic Blast</a></h1>
               <div class="clearfix"></div>
            </div>
			 <div class=" border-bottom">
        	<?php
                require_once 'views/header.php';
                ?>
                             <div class="clearfix"></div>
                <?php
                //require_once 'views/navbar.php';
                require_once './views/left_sidebar_nav.php';
                ?>
			
        </nav>

        <div id="page-wrapper" class="gray-bg dashbard-1">
       <div class="content-main">
            <div class="validation-system">
               <div class="validation-form">
           <h1><i class="fa fa-user-plus"></i> User <small>Add User</small></h1><hr style="margin-top: 10px; margin-bottom: 10px;">
                        <ol class="breadcrumb">
                            <li class=""><i class="fa fa-dashboard"></i> Dashboard</li>
                            <li class="active"><i class="fa fa-user-plus"> Add New User</i> </li>
                        </ol>
           
<div class=" profile">
    <div class="">
        <div class="row">
  <div class="col-md-8 col-md-offset-2">
       <span class="">
        <?php
            if(isset($_SESSION['msg']))
            {
            $msg = $_SESSION['msg'];
            echo ($msg);
            unset($_SESSION['msg']);
            }
            if(isset($_SESSION['errors']))
            {
            $errors = $_SESSION['errors'];
            unset($_SESSION['errors']);
            }
            if(isset($_SESSION['check_email']))
            {
            $check_email = $_SESSION['check_email'];
            unset($_SESSION['check_email']);
            }
            if(isset($_SESSION['obj_admin']))
            {
            $obj_admin = unserialize($_SESSION['obj_admin']);
            //unset($_SESSION['obj_admin']);
            }
            else 
            {
            $obj_admin = new Admin();
            }
        ?>
</span>
      <form action="controller/signup_process.php" method="post" enctype="multipart/form-data">
          <div class="form-group">
            <label>First Name*</label>
            <span class="text-danger pull-right">
                <?php 
                if(isset($errors['first_name']))
                    {
                        echo $errors['first_name'];
                    }
                    ?>
            </span>
            <input type="text" name="first_name" value="<?php echo $obj_admin->first_name; ?>" class="form-control" placeholder="First Name"/>
         </div>
          <div class="form-group">
            <label>Last Name*</label>
            <span class="text-danger pull-right">
                <?php 
                if(isset($errors['last_name']))
                {
                    echo $errors['last_name'];
                }
             ?>
            </span>
            <input type="text" name="last_name" value="<?php echo $obj_admin->last_name; ?>" class="form-control" placeholder="Last Name"/>
         </div>
            <div class="form-group">
                <label>Admin Name*</label>
                <span class="text-danger pull-right">
                <?php if(isset($errors['admin_name']))
                    {
                        echo $errors['admin_name'];
                    }
                 ?>
                </span>
                <input type="text" name="admin_name" value="<?php echo $obj_admin->admin_name; ?>" class="form-control" placeholder="User Name"/>
            </div>
            <div class="form-group">
                <label>Email*</label>
                <span class="text-danger pull-right">
                <?php if(isset($errors['email']))
                    {
                        echo $errors['email'];
                    }elseif (isset($errors['check_email']))
                      {
                        echo ($errors['check_email']);
                      }
                 ?>
                </span>
                <input type="email" name="email" value="<?php echo $obj_admin->email; ?>" class="form-control" placeholder="Email"/>
            </div>
            <div class="form-group">
                <label>Password*</label>
                <span class="text-danger pull-right">
                <?php if(isset($errors['password']))
                    {
                        echo $errors['password'];
                    }
                ?>
                </span>
                <input type="password" name="password" class="form-control" placeholder="Password"/>
            </div>
            <div class="form-group">
                <label>Re-type Password*</label>
                <span class="text-danger pull-right">
                <?php if(isset($errors['retype_password']))
                    {
                        echo $errors['retype_password'];
                    }
                ?>
                </span>
                <input type="password" name="password2" class="form-control" placeholder="Password"/>
            </div>
            <div class="form-group">
                <label>Profile Image*</label>
                <span class="text-danger pull-right">
                <?php
                    if(isset($errors['profile_image']))
                    {
                        echo $errors['profile_image'];
                    }
                    ?>
                </span>
                <input type="file" value="<?php echo $obj_admin->profile_image; ?>" name="profile_image" class="form-control"/>
            </div>
          <input type="submit" value="Add Admin" class="btn btn-primary"/>
        </form>
  </div>
 
                  </div>
    </div>
</div>
               </div>
            </div>
  		
	
<?php
require_once 'views/footer.php';


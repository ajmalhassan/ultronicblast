<?php
//session_start();
require_once '../models/admin.php';
require_once '../models/db_connection.php';
require_once 'views/top.php';
?>
</head>
<body>
    <div class="login" style="padding: 2em 0 0;">
    <h1><a href="index.php">Ultronic Blast </a></h1>
    <div class="login-bottom">
        
        <h2>Create New Account
        <span class="error-color">
         <?php
        if(isset($_SESSION['msg']))
        {
            $msg = $_SESSION['msg'];
            echo ($msg);
            unset($_SESSION['msg']);
        }
        if(isset($_SESSION['errors']))
        {
            $errors = $_SESSION['errors'];
            unset($_SESSION['errors']);
        }
        if(isset($_SESSION['obj_admin']))
        {
            $obj_admin = unserialize($_SESSION['obj_admin']);
            //unset($_SESSION['obj_admin']);
        }
        else 
        {
            $obj_admin = new Admin();
        }
        ?>
            </span>
        
        </h2>
        <form action="controller/signup_process.php" method="post" enctype="multipart/form-data">
        <div class="col-md-6">
            <div class="login-mail" <?php if(isset($errors['first_name']))
            {
                echo $errors['first_name'];
            }
             ?>
                 >
                <input type="text" name="first_name" value="<?php echo $obj_admin->first_name; ?>" placeholder="First Name" >
                <i class="fa fa-envelope"></i>
            </div>
            
            
        </div>
            
            <div class="col-md-6">
                <div class="login-mail" <?php if(isset($errors['last_name']))
            {
                echo $errors['last_name'];
            }
             ?>
                 >
                <input type="text" name="last_name" value="<?php echo $obj_admin->last_name; ?>" placeholder="Last Name" >
                <i class="fa fa-envelope"></i>
                </div>
            </div>
            <div class="col-md-6">
            <div class="login-mail" 
                <?php if(isset($errors['admin_name']))
                {
                    echo $errors['admin_name'];
                }
                 ?>
                 >
                <input type="text" name="admin_name" value="<?php echo $obj_admin->admin_name; ?>" placeholder="Admin Name" >
                <i class="fa fa-envelope"></i>
            </div>
            </div>
            <div class="col-md-6">
            <div class="login-mail" <?php if(isset($errors['email']))
            {
                echo $errors['email'];
            }
             ?>
                 >
                <input type="text" name="email" value="<?php echo $obj_admin->email; ?>" placeholder="Email" >
                <i class="fa fa-envelope"></i>
            </div>
            </div>
            <div class="col-md-6">
            <div class="login-mail" <?php if(isset($errors['password']))
            {
                echo $errors['password'];
            }
             ?>
                 >
                <input type="password" name="password" value="" placeholder="Password">
                <i class="fa fa-lock"></i>
            </div>
            </div>
            <div class="col-md-6">
            <div class="login-mail" <?php if(isset($errors['retype_password']))
            {
                echo $errors['retype_password'];
            }
             ?>
                 >
                <input type="password" name="password2" value="" placeholder="Repeated password" >
                <i class="fa fa-lock"></i>
            </div>
            </div>
            <div class="col-md-6">
                <div class="login-mail" style="border: none;padding: 0;">
                <input type="file" name="profile_image" value="<?php echo $obj_admin->profile_image; ?>" />
                <span class="error-color">
                    <?php
                    if(isset($errors['profile_image']))
                    {
                        echo $errors['profile_image'];
                    }
                    ?>
                </span>
                </div>
                 
            </div>
            <div class="clearfix"></div>
            <div class="col-md-6">
                <a class="news-letter" href="#">
                  <label class="checkbox1"><input type="checkbox" name="term_checkbox" ><i> </i>I agree with the terms</label>
               </a>
            </div>
            <div class="clearfix"></div>
             <div class="col-md-6 login-do">
                <label class="hvr-shutter-in-horizontal login-sub">
                <input type="submit" value="Submit">
                </label>
             </div>
            <div class="col-md-6 login-do">
                <label class="hvr-shutter-in-horizontal login-sub">
                <a href="signin.php" class="hvr-shutter-in-horizontal">Login</a>
                </label>
             </div>
            <div class="clearfix"></div>
            </form>
        </div>
        
        <div class="clearfix"> </div>
    </div>
</div>
            <?php
        require_once 'views/footer.php';
<?php
require_once 'views/top.php';
require_once '../models/subscribe.php';
?>
</head>
<body>
<div id="wrapper">

<!----->
        <nav class="navbar-default navbar-static-top" role="navigation">
             <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
               <h1> <a class="navbar-brand" href="index.php">Ultronic Blast</a></h1>         
            </div>
			 <div class=" border-bottom">
        	<?php
                require_once 'views/header.php';
                ?>
                             <div class="clearfix"></div>
                <?php
                require_once 'views/left_sidebar_nav.php';
                ?>
			<div class="clearfix"></div>
        </nav>
        <div id="page-wrapper" class="gray-bg dashbard-1">
       <div class="content-main">
           <div class="blank">
         <div class="blank-page">
             <h1><i class="fa fa-support"></i> Subscribed</h1><hr style="margin-top: 10px; margin-bottom: 10px;">
                  <ol class="breadcrumb">
                      <li class="active"><i class="fa fa-dashboard"></i> Dashboard</li>
                      <li class="active"><i class="fa fa-support"></i> Subscribed</li>
                  </ol>

<div class="table-responsive" ><!-- table-responsive Starts -->

<table class="table table-bordered table-hover table-striped" ><!-- table table-bordered table-hover table-striped Starts -->

<thead><!-- thead Starts -->

<tr>
<th>Email</th>
<th>Browser</th>
<th>Date</th>
</tr>
</thead><!-- thead Ends -->

<tbody><!-- tbody Starts -->

<?php
    
    $subscribe = Subscribe::show_subscribe();
 
 foreach ($subscribe as $sub)
 {
   ?>
    <tr>
    <td><?php echo("$sub->email")?></td>
    <td><?php echo("$sub->browser")?></td>
    <td><?php echo("$sub->date")?></td>
    
    </tr>
 <?php
 
 }

?>

</tbody><!-- tbody Ends -->


</table><!-- table table-bordered table-hover table-striped Ends -->
<nav aria-label="brand-nav">
                <ul class="pagination">
                  <?php
//                    $pNums = Order::pagination_order(ITEM_PER_PAGE);
//                     foreach ($pNums as $pNo=>$start)
//                     {
//                        echo("<li class='page-link'><a href='" . BASE_URL . "index.php?start=$start'>$pNo <span class='sr-only'>(current)</span></a></li>");
//                     }
//                     
//                 ?>
                </ul>
            </nav>
              
</div><!-- table-responsive Ends -->
                       </div>
                    </div>
           
  		
	
<?php
require_once 'views/footer.php';



<?php
require_once "views/top.php";
require_once '../models/slider.php';
require_once 'views/profile.php';
?>
</head>
<body>
<div id="wrapper">

<!----->
        <nav class="navbar-default navbar-static-top" role="navigation">
             <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
               <h1> <a class="navbar-brand" href="index.php">Ultronic Blast</a></h1>         
            </div>
			 <div class=" border-bottom">
        	<?php
                require_once 'views/header.php';
                ?>
                   <div class="clearfix"></div>
                <?php
                require_once './views/left_sidebar_nav.php';
                ?>
			<div class="clearfix"></div>
        </nav>
        <div id="page-wrapper" class="gray-bg dashbard-1">
       <div class="content-main">
           <div class="validation-system">
               <div class="validation-form">
           <h2>Ultronic Blast Slider
               <span class="error-color"> 
                   <?php
                    if(isset($_SESSION['msg']))
                    {
                        $msg = $_SESSION['msg'];
                        echo ($msg);
                        unset($_SESSION['msg']);
                    }
                    if(isset($_SESSION['errors']))
                    {
                        $errors = $_SESSION['errors'];
                        unset($_SESSION['errors']);
                    }
                    if(isset($_SESSION['obj_slider']))
                    {
                        $obj_product = unserialize($_SESSION['obj_slider']);
                        unset($_SESSION['obj_slider']);

                    }
                    else
                    {
                        $obj_slider = new Slider();
                    }
                   ?>
               </span>
           </h2>
              <?php
              if(isset($_GET['slideID'])){
              $keyslide = $_GET['slideID'];
                $slides = Slider::get_single_slide($keyslide);
                foreach ($slides as $s)
                {
                   ?>
                   <div class="row">
                       <div class="col-md-6">
    <form action="controller/slider_update_process.php?slideID=<?php echo($s->slide_ID);?>" method="post" enctype="multipart/form-data">
           
                <div class="col-md-12 form-group1">
                  <label class="control-label">Slide Name</label>
                    <input type="text" class="" placeholder="Slide Name" name="slide_name" value="<?php echo($s->slide_name);?>" required="">
                    <p class="error-color">

                  </p>
                </div>
        <div class="clearfix"> </div>
           <div class="col-md-12 form-group1 form-last">
          <label class="control-label">Button Link</label>
          <input type="text" placeholder="http://example.com" class="" name="slide_link" value="<?php echo($s->slide_link);?>" required="">
          <p class="error-color">
              
          </p>
        </div>
       
     <div class="clearfix"> </div>
    <div class="col-md-12 form-group1 form-last">
       <label class="control-label">Description</label>
      <textarea  placeholder="Slide Description" name="slide_description" class="string-validate " required="" cols="120"><?php echo($s->slide_description);?></textarea>
      <p class="error-color">
          
      </p>
    </div>
     <div class="clearfix"> </div>
     <br>
     <div class="col-md-12 form-group">
         <input type="file" id="slide_image" value="" name="slide_image" required/>
         <p class="error-color">
             
         </p>
    </div>
    <div class="col-md-12 form-group">
      <button type="submit" class="btn btn-primary">Update Slide</button>
    </div>
  <div class="clearfix"> </div>
 </form>
                       </div>
                       <div class="col-md-6">
                           <img src="<?php echo(BASE_URL."slider_images/$s->slide_name/$s->slide_image")?>" width="100%" />
                       </div>
                   </div>
             
 <?php
                }
              }
              ?>
       
               </div>
           </div>
  		
	
<?php
require_once 'views/footer.php';


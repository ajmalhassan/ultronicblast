<?php

require_once "views/top.php";
require_once '../models/product.php';
require_once '../models/user.php';
require_once 'views/profile.php';
?>
<style>
    .table>tbody>tr>th
    {
       padding: 10px !important;
    }
</style>
</head>
<body>
<div id="wrapper">
<?php

?>
<!----->
        <nav class="navbar-default navbar-static-top" role="navigation">
             <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
               <h1> <a class="navbar-brand" href="index.php">Ultronic Blast</a></h1>         
            </div>
			 <div class=" border-bottom">
        	<?php
                require_once 'views/header.php';
                ?>
                             <div class="clearfix"></div>
                <?php
                require_once 'views/left_sidebar_nav.php';
                ?>
			<div class="clearfix"></div>
        </nav>
        <div id="page-wrapper" class="gray-bg dashbard-1">
       <div class="content-main">
           <div class="validation-system">
                <div class="validation-form">
                    <h1><i class="fa fa-users"></i> Customers <small>View all Customer</small></h1><hr style="margin-top: 10px; margin-bottom: 10px;">
                  <ol class="breadcrumb">
                      <li class=""><i class="fa fa-dashboard"></i> Dashboard</li>
                      <li class="active"><i class="fa fa-users"></i> All Customers</li>
                  </ol>
                    <h2 class="h2">All Customers
                        <span class="error-color">
                            <?php 
                            if(isset($_SESSION['msg']))
                            {
                                $msg = $_SESSION['msg'];
                                echo ($msg);
                                unset($_SESSION['msg']);
                            }
                                
                            ?>
                        </span>
                    </h2> 
                    
    <div class="table-responsive">
         <table class="table table-bordered table-condensed table-all">
                    <tr class="bg-danger" style="background-color: #337ab7; color: #fff;">
                   <th>sr #</th>
                   <th>User Name</th>
                   <th>Email</th>
                   <th>Mobile No</th>
                   <th>Address</th>
                   <th>Delete</th>
               </tr>
               <?php
             try {
                 $start = isset($_GET['start']) ? $_GET['start'] : 0;
                 $count = isset($_GET['count']) ? $_GET['count'] : ITEM_PER_PAGE;
                 $users = User::users($start, $count);
                
               foreach ($users as $u){
               echo ("<tr>"
                       . "<td>$u->user_id</td>"
                       . "<td>$u->user_name</td>"
                       . "<td>$u->email</td>"
                       . "<td>$u->mobile_number</td>"
                       . "<td>$u->address</td>"
                       
                       . "<td><a class='delete-action' href='".BASE_URL."controller/remove_user.php?action=remove_user&user_id=$u->user_id'><i class='fa fa-times'></i></a></td>"     
                       . "</tr>");
               }
               ?>
                </table>
            </div>
              <nav aria-label="brand-nav">
                <ul class="pagination">
                  <?php
                    $pNums = product::pagination(ITEM_PER_PAGE);
                     foreach ($pNums as $pNo=>$start)
                     {
                        echo("<li class='page-link'><a href='" . BASE_URL . "display_users.php?start=$start'>$pNo <span class='sr-only'>(current)</span></a></li>");
                     }
                     
                 ?>
                </ul>
            </nav>
               <?php
               
                } catch (Exception $ex) {
                    echo $ex->getMessage();
                }

               ?>
          
            
	        </div>
	    </div>
           
	
<?php
require_once 'views/footer.php';



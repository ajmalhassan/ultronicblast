<?php
require_once "views/top.php";
?>
</head>
<body>
<div id="wrapper">
<?php
$obj_admin->profile();
?>
<!----->
        <nav class="navbar-default navbar-static-top" role="navigation">
             <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
               <h1> <a class="navbar-brand" href="index.php">Ultronic Blast</a></h1>         
            </div>
			 <div class=" border-bottom">
        	<?php
                require_once 'views/header.php';
                ?>
                    <div class="clearfix"></div>             
                <?php
                //require_once 'views/navbar.php';
                require_once './views/left_sidebar_nav.php';
                ?>
			<div class="clearfix"></div>
        </nav>
        <div id="page-wrapper" class="gray-bg dashbard-1">
       <div class="content-main">
          <div class=" profile">

    <div class="profile-bottom">
            <h3><i class="fa fa-user"></i>Edit Profile Image
                <small>
                    <?php
                        if(isset($_SESSION['msg']))
                        {
                            $msg = $_SESSION['msg'];
                            echo ($msg);
                            unset($_SESSION['msg']);
                        }
                        if(isset($_SESSION['errors']))
                        {
                            $errors = $_SESSION['errors'];
                            unset($_SESSION['errors']);
                        }
                    ?>
                </small>
            </h3>
            <div class="profile-bottom-top">
            <div class="col-md-4 profile-bottom-img">
                    
                <img src="<?php echo($obj_admin->profile_image);?>">
            </div>
            <div class="col-md-8 profile-text">
                <form action="controller/process_update_image.php" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <input type="file" name="profile_image" value=""/>
                        <span>
                            <?php
                                if(isset($errors['profile_image']))
                                    {
                                    echo ($errors['profile_image']);
                                    }
                            ?>
                        </span>
                    </div>
                    <input class="btn btn-danger" type="submit" value="Save"/>
                </form>    
            </div>
            <div class="clearfix"></div>
            </div>
            <div class="profile-bottom-bottom">
            <div class="col-md-4 profile-fo">
<!--                    <h4>23,5k</h4>
                    <p>Followers</p>-->
<a href="<?php echo (BASE_URL);?>change_account.php" class="pro"><i class="fa fa-plus-circle"></i>Edit</a>
            </div>
            <div class="col-md-4 profile-fo">
<!--                    <h4>348</h4>
                    <p>Following</p>-->
            <a href="<?php echo (BASE_URL);?>change_image.php" class="pro1"><i class="fa fa-user"></i>Change Image</a>
            </div>
            <div class="col-md-4 profile-fo">
<!--                    <h4>23,5k</h4>
                    <p>Snippets</p>-->
            <a href="<?php echo (BASE_URL);?>change_password.php"><i class="fa fa-cog"></i>Change Password</a>
            </div>
            <div class="clearfix"></div>
            </div>
            <div class="profile-btn">

    <!--<button type="button" href="#" class="btn bg-red">Save changes</button>-->
<div class="clearfix"></div>
            </div>


    </div>
</div>
           
       </div>	
	
<?php
require_once 'views/footer.php';



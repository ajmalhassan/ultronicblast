<?php
//session_start();
require_once '../models/product.php';
require_once '../models/category.php';
require_once 'views/top.php';
require_once 'views/profile.php';

?>
<script type="text/javascript">
    $(document).ready(function(){
        $(".string-validate").limitkeypress({ rexp: /^[A-Za-z0-9 ]*$/});
        $(".number-validate").limitkeypress({ rexp: /^[0-9]*$/});
    });
</script>
</head>
<body>
<div id="wrapper">

<!----->
        <nav class="navbar-default navbar-static-top" role="navigation">
             <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
               <h1> <a class="navbar-brand" href="index.php">Ultronic Blast</a></h1>         
            </div>
			 <div class=" border-bottom">
        	<?php
                //require_once 'views/searchbar.php';
                require_once 'views/header.php';
                ?>
			<div class="clearfix"></div>
	  
		    <?php
                    require_once 'views/left_sidebar_nav.php';
                    ?>
        </nav>
        <div id="page-wrapper" class="gray-bg dashbard-1">
       <div class="content-main">
 
  	<div class="validation-system">
 		
 		<div class="validation-form">
                     <h1><i class="fa fa-folder-open"></i> Categories <small></small></h1><hr style="margin-top: 10px; margin-bottom: 10px;">
                        <ol class="breadcrumb">
                            <li><i class="fa fa-dashboard"></i> Dashboard</li>
                            <li class="active"><i class="fa fa-folder-open-o"></i> Categories </li>
                        </ol>
        
        <?php
            if(isset($_SESSION['errors']))
            {
                $errors = $_SESSION['errors'];
                unset($_SESSION['errors']);
            }
            if(isset($_SESSION['obj_category']))
            {
                $obj_category = unserialize($_SESSION['obj_category']);
                unset($_SESSION['obj_category']);
            }
            else
            {
                $obj_category = new Category();
                
            }
        ?>
    <div class="row">
        <div class="col-md-6">
            <h2 class="h2">Add new Category 
                <small class="error-color">
                <?php
             if(isset($_SESSION['msg']))
            {
                $msg = $_SESSION['msg'];
                echo ($msg);
                unset($_SESSION['msg']);
            }
            ?>
            </small>
            </h2>
        <form action="controller/category_process.php" method="post" enctype="multipart/form-data">
            <div class="">
                <div class="col-md-12 form-group1">
                  <label class="control-label">Category Name</label>
                  <input type="text" placeholder="Category Name" class="string-validate form-control" name="category_name" value="<?php echo ($obj_category->category_name);?>" required="">
                  <p class="error-color">
                      <?php
                      if(isset($errors['category_name']))
                      {
                          echo ($errors['category_name']);
                      }
                      ?>
                  </p>
                </div>
                <div class="clearfix"> </div>
            </div><br>
            <div class="col-md-12 form-group">
                <input type="file" id="category_image" value=""  name="category_image"/>
                 <p class="error-color">
                     <?php
                      if(isset($errors['category_image']))
                      {
                          echo ($errors['category_image']);
                      }
                      ?>
                 </p>
            </div>
            <div class="col-md-12 form-group">
              <button type="submit" class="btn btn-primary">Add Category</button>
              
            </div>
          <div class="clearfix"> </div>
        </form>
            <?php
            if(isset($_GET['categoryID'])){
                 $keycat = $_GET['categoryID'];
                ?>
            <hr>
            <div class="update-category">
                <h2 class="h2">Update Category</h2>
        <?php
            if(isset($_SESSION['errors']))
            {
                $errors = $_SESSION['errors'];
                unset($_SESSION['errors']);
            }
            if(isset($_SESSION['obj_category']))
            {
                $obj_category= unserialize($_SESSION['obj_category']);
            }
            else
            {
                $obj_category = new Category();
            }
        ?>
        <?php
        try {
//            echo ($keycat);
//            die;
            $category = Category::get_category($keycat);
    foreach ($category as $c)
        {
        ?>
             <form action="controller/update_category_process.php?categoryID=<?php echo($c->categoryID);?>" method="post" enctype="multipart/form-data">
            <div class="vali-form">
                <div class="col-md-12 form-group1">
                  <label class="control-label">Category Name</label>
                  <input type="text" placeholder="Category Name" class="string-validate" name="category_name" value="<?php echo($c->category_name);?>" required="">
                  <p class="error-color"></p>
                </div>
                <div class="clearfix"> </div>
            </div>
            
            <div class="col-md-12 form-group">
                <button type="submit" class="btn btn-primary">Update</button>
            </div>
          <div class="clearfix"> </div>
        </form>
        <?php
        }
        } catch (Exception $ex) {
            echo $ex->getMessage();
        }
        ?>
            </div>
            <?php
            }
            ?>
            
        </div>
        <div class="col-md-6">
            <h2 class="h3">All Categories 
            <small>
                    <?php
                   if (isset($_SESSION['update_msg'])) {
                        $update_msg = $_SESSION['update_msg'];
                        echo ($update_msg);
                       unset($_SESSION['update_msg']);
                    }
                    ?>
            </small>
            </h2>
            <table class="table table-responsive table-bordered">
                <thead>
                    <tr class="bg-danger" style="background-color: #337ab7; color: #fff;">
                       <th>sr #</th>
                       <th>Brand Name</th>
                       <th>Image</th>
                       <th>Edit</th>
                       <th>Delete</th>
                    </tr>
                </thead>
               <?php
               try {
                 $start = isset($_GET['start']) ? $_GET['start'] : 0;
                 $count = isset($_GET['count']) ? $_GET['count'] : ITEM_PER_PAGE;
               $category = Category::get_categories($start, $count);
               foreach ($category as $c){
                   $category_path =  pathinfo($c->category_image);
                   $category_image_name =$category_path['filename'] ;
                   ?>
                <tbody>
                <tr>
                 <td><?php echo($c->categoryID);?></td>
                 <td><?php echo($c->category_name); ?></td>
                 <td><img src="<?php echo(BASE_URL."categories/$category_image_name/$c->category_image")?>" alt="<?php echo($c->category_name);?>" width="30px"/></td>
                 <td><a href="<?php echo(BASE_URL."categories.php?categoryID=$c->categoryID")?>"><span><i class="fa fa-edit"></i></span></a></td>
                 <td><a class="delete-action" href="<?php echo(BASE_URL."controller/remove.php?action=remove_category&categoryID=$c->categoryID")?>"><span><i class="fa fa-times"></i></span></a></td>
                </tr>
                </tbody>
               <?php
               }
               ?>
           </table>
            <nav aria-label="brand-nav">
                <ul class="pagination">
                  <?php
                    $pNums = Category::pagination(ITEM_PER_PAGE);
                     foreach ($pNums as $pNo=>$start)
                     {
                        echo("<li class='page-link'><a href='" . BASE_URL . "categories.php?start=$start'>$pNo <span class='sr-only'>(current)</span></a></li>");
                     }
                     
                 ?>
                </ul>
            </nav>
            <?php
               
                } catch (Exception $ex) {
                    echo $ex->getMessage();
                }

               ?>
        </div>
    </div>
    
 	<!---->
 </div>

</div>	
	
<?php
require_once 'views/footer.php';

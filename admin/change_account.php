<?php
require_once "views/top.php";
require_once '../models/admin.php';
?>
</head>
<body>
<div id="wrapper">
<?php
//$adminkey = $_GET['admin_id'];
//   $obj_admin->get_profile($adminkey); 
//   
?>
<!----->
        <nav class="navbar-default navbar-static-top" role="navigation">
             <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
               <h1> <a class="navbar-brand" href="index.php">Ultronic Blast</a></h1>         
            </div>
			 <div class=" border-bottom">
        	<?php
                require_once 'views/header.php';
                ?>
                 <div class="clearfix"></div>
                 <?php
                //require_once 'views/navbar.php';
                require_once './views/left_sidebar_nav.php';
                ?>
			<div class="clearfix"></div>
        </nav>
        <div id="page-wrapper" class="gray-bg dashbard-1">
       <div class="content-main">
           
<div class=" profile">

    <div class="profile-bottom">
            <h3><i class="fa fa-user"></i>Change Account
                <small>
                    <?php
                                if(isset($_SESSION['msg']))
                                {
                                    $msg = $_SESSION['msg'];
                                    echo ($msg);
                                    unset($_SESSION['msg']);
                                }
                                if(isset($_SESSION['errors']))
                                {
                                    $errors = $_SESSION['errors'];
                                    unset($_SESSION['errors']);
                                }
                            ?>
                </small>
            </h3>
            <div class="profile-bottom-top">
<!--            <div class="col-md-4 profile-bottom-img">
                    
                <img src="<?php echo($obj_admin->profile_image);?>">
            </div>-->
            <div class="col-md-8 profile-text">
                
                    <form action="controller/process_account.php" method="post" >
                    <table>
                    <tr>
                    <td>Admin Name</td>
                    <td> :</td>
                    <td><input type="text" name="admin_name" value="<?php echo($obj_admin->admin_name);?>"
                               <?php
                                if(isset($errors['admin_name']))
                                {
                                    echo ($errors['admin_name']);
                                }
                               ?>
                               /> </td>
                    <td>
                        
                    </td>
                    </tr>
                    <tr><td>First Name</td>  
                    <td>:</td>  
                    <td><input type="text" name="first_name" value="<?php echo($obj_admin->first_name);?>"
                     <?php
                     if(isset($errors['first_name']))
                     {
                         echo ($errors['first_name']);
                     }
                     ?>          
                     /></td></tr>
                    <tr>
                    <td>Last Name</td>
                    <td> :</td>
                    <td><input type="text" name="last_name" value="<?php echo($obj_admin->last_name);?>"
                      <?php
                     if(isset($errors['last_name']))
                     {
                         echo ($errors['last_name']);
                     }
                     ?> 
                      /></td>
                    </tr>
                    <tr>
                    <td>Email</td>
                    <td> :</td>
                    <td> <?php echo($obj_admin->email);?></td>
                    </tr>
                    <tr>
                        <td><input class="btn btn-danger" type="submit" value="Update"/></td>
                    <td></td>
                    <td></td>
                    </tr>
                    </table>
                    </form>
            </div>
            <div class="clearfix"></div>
            </div>
            <div class="profile-bottom-bottom">
            <div class="col-md-4 profile-fo">
<!--                    <h4>23,5k</h4>
                    <p>Followers</p>-->
            <a href="../admin/change_account.php" class="pro"><i class="fa fa-plus-circle"></i>Edit</a>
            </div>
            <div class="col-md-4 profile-fo">
<!--                    <h4>348</h4>
                    <p>Following</p>-->
                    <a href="../admin/change_image.php" class="pro1"><i class="fa fa-user"></i>Change Image</a>
            </div>
            <div class="col-md-4 profile-fo">
<!--                    <h4>23,5k</h4>
                    <p>Snippets</p>-->
                    <a href="../admin/change_password.php"><i class="fa fa-cog"></i>Change Password</a>
            </div>
            <div class="clearfix"></div>
            </div>
            <div class="profile-btn">

    <!--<button type="button" href="#" class="btn bg-red">Save changes</button>-->
<div class="clearfix"></div>
            </div>


    </div>
</div>
  		
	
<?php
require_once 'views/footer.php';;
?>


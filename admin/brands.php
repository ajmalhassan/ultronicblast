<?php
//session_start();
require_once '../models/product.php';
require_once '../models/brand.php';
require_once 'views/top.php';
require_once 'views/profile.php';

?>
<script type="text/javascript">
    $(document).ready(function(){
        $(".string-validate").limitkeypress({ rexp: /^[A-Za-z0-9 ]*$/});
        $(".number-validate").limitkeypress({ rexp: /^[0-9]*$/});
    });
</script>
</head>
<body>
<div id="wrapper">

<!----->
        <nav class="navbar-default navbar-static-top" role="navigation">
             <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
               <h1> <a class="navbar-brand" href="index.php">Ultronic Blast</a></h1>         
            </div>
			 <div class=" border-bottom">
        	<?php
                //require_once 'views/searchbar.php';
                require_once 'views/header.php';
                ?>
			<div class="clearfix"></div>
	  
		    <?php
                    require_once 'views/left_sidebar_nav.php';
                    ?>
        </nav>
        <div id="page-wrapper" class="gray-bg dashbard-1">
       <div class="content-main">
 
  	<div class="validation-system">
 		
 		<div class="validation-form">
             <h1><i class="fa fa-briefcase"></i> Brands</h1><hr style="margin-top: 10px; margin-bottom: 10px;">
                <ol class="breadcrumb">
                    <li><i class="fa fa-dashboard"></i> Dashboard</li>
                    <li class="active"><i class="fa fa-file-text"></i> Brands</li>
                </ol>
        
        <?php
            if(isset($_SESSION['errors']))
            {
                $errors = $_SESSION['errors'];
                unset($_SESSION['errors']);
            }
            if(isset($_SESSION['obj_brand']))
            {
                $obj_brand = unserialize($_SESSION['obj_brand']);
            }
            else
            {
                $obj_brand= new Brand();
                
            }
        ?>
        
        <div class="row">
            <div class="col-md-6">
                <h2>Add new Brand</h2>
              <form action="controller/brand_process.php" method="post" enctype="multipart/form-data">
                    <div class="vali-form">
                        <div class="col-md-12 form-group1">
                          <label class="control-label">Brand Name</label>
                          <span class="error-color pull-right">
                                 <?php
                                 if(isset($_SESSION['msg']))
                                {
                                    $msg = $_SESSION['msg'];
                                    echo ($msg);
                                    unset($_SESSION['msg']);
                                }
                                ?>
                            </span>
                          <input type="text" placeholder="Brand Name" class="string-validate" name="brand_name" value="" required="">
                          <p class="error-color">
                              <?php
                              if(isset($errors['brand_name']))
                              {
                                  echo ($errors['brand_name']);
                              }
                              ?>
                          </p>
                        </div>
                        <div class="clearfix"> </div>
                    </div>

                    <div class="col-md-12 form-group">
                      <button type="submit" class="btn btn-primary">Add Brand</button>
                      <button type="reset" class="btn btn-default">Reset</button>
                    </div>
          <div class="clearfix"> </div>
        </form>
        <div class="update-brand">
            <?php
                if(isset($_GET['brandID'])){
                    ?>
            <hr>
                    <h2>Update Brand</h2>
        <?php
            if(isset($_SESSION['errors']))
            {
                $errors = $_SESSION['errors'];
                unset($_SESSION['errors']);
            }
            if(isset($_SESSION['obj_brand']))
            {
                $obj_brand = unserialize($_SESSION['obj_brand']);
               // unset($_SESSION['obj_product']);
               
            }
            else
            {
                $obj_brand = new Brand();
                
            }
//            if(isset($_SESSION['obj_category']))
//            {
//                $obj_category = unserialize($_SESSION['obj_category']);
//            }
//            else {
//                $obj_category = new Category();
//            }
        ?>
<?php
        try {
       $keybra = $_GET['brandID'];
//    echo ($keybra);
//    die;
    $brands = $obj_brand->get_brand($keybra);
    foreach ($brands as $a)
        {
        ?>
        <form action="controller/update_brand_process.php?brandID=<?php echo($a->brandID);?>" method="post" enctype="multipart/form-data">
            <div class="vali-form">
                <div class="col-md-12 form-group1">
                  <label class="control-label">Brand Name</label>
                  <input type="text" placeholder="Brand Name" class="string-validate" name="brand_name" value="<?php echo($a->brand_name);?>" required="">
                  <p class="error-color">
                  </p>
                </div>
                <div class="clearfix"> </div>
            </div>
            
            <div class="col-md-12 form-group">
              <button type="submit" class="btn btn-primary">Update Brand</button>
            </div>
          <div class="clearfix"> </div>
        </form>
          
        <?php
        
        }
        } catch (Exception $ex) {
            echo($ex->getMessage());
        }

                }
            ?>
        </div>
            </div>
            <div class="col-md-6">
                <h2 class="">All Brands 
                         
                    <small class="error-color">
                <?php
             if(isset($_SESSION['update_msg']))
            {
                $update_msg = $_SESSION['update_msg'];
                echo ($update_msg);
                unset($_SESSION['update_msg']);
            }
        ?>
                    </small>
                    </h2>
                <table class="table table-responsive table-bordered table-sm" style=" width: 484px;">
                   <tr class="bg-danger" style="background-color: #337ab7; color: #fff;">
                   <th>sr #</th>
                   <th>Name</th>
                   <th>Edit</th>
                   <th>Delete</th>
               </tr>
               <?php
               try {
                   $start = isset($_GET['start']) ? $_GET['start'] : 0;
                   $count = isset($_GET['count']) ? $_GET['count'] : ITEM_PER_PAGE;
                   $brands = Brand::get_brands($start, $count);
               foreach ($brands as $b){
               ?>
               <tr>
                   <td><?php echo($b->brandID);?></td>
                   <td><?php echo(ucfirst($b->brand_name));?></td>
                   <td><a href="<?php echo(BASE_URL."brands.php?brandID=$b->brandID")?>"><span><i class="fa fa-edit"></i></span></a></td>
                   <td><a class="delete-action" href="<?php echo(BASE_URL."controller/remove.php?action=remove_brand&brandID=$b->brandID")?>"><span><i class="fa fa-times"></i></span></a></td>
               </tr>
               <?php
               }
               ?>
           </table>
       <nav aria-label="brand-nav">
                <ul class="pagination">
                  <?php
                    $pNums = Brand::pagination(ITEM_PER_PAGE);
                     foreach ($pNums as $pNo=>$start)
                     {
                        echo("<li class='page-link'><a href='" . BASE_URL . "brands.php?start=$start'>$pNo <span class='sr-only'>(current)</span></a></li>");
                     }
                     
                 ?>
                </ul>
            </nav>
<?php
               } catch (Exception $exc) {
                   echo $exc->getTraceAsString();
               }
               ?>

               
            </div>
        </div>
          
    
 	<!---->
 </div>

</div>	
	
<?php
require_once 'views/footer.php';
<?php
//session_start();
require_once "views/top.php";
require_once '../models/product.php';
require_once '../models/Order.php';
require_once 'views/profile.php';
?>
<link href="css/print.css" rel="stylesheet" media="print" type="text/css"/>
</head>
<body>
<div id="wrapper">

<!----->
        <nav class="navbar-default navbar-static-top" role="navigation">
             <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
               <h1> <a class="navbar-brand" href="index.php">Ultronic Blast</a></h1>         
            </div>
		<div class=" border-bottom">
        	<?php
                //require_once 'views/searchbar.php';
                require_once 'views/header.php';
                ?>
                <div class="clearfix"></div>
                <?php
                require_once 'views/left_sidebar_nav.php';
                ?>
                <div class="clearfix"></div>
		
        </nav>
<div class="clearfix"></div>
        <div id="page-wrapper" class="gray-bg dashbard-1">
       <div class="content-main">
           <div class="blank">
               <div class="blank-page">
                   <?php
//                   if(isset($_SESSION['msg']))
//                   {
//                       $msg = $_SESSION['msg'];
//                       echo ($msg);
//                       unset($_SESSION['msg']);
//                   }
                   if(isset($_SESSION['msg_err']))
                   {
                       $msg_err = $_SESSION['msg_err'];
                       echo ($msg_err);
                       unset($_SESSION['msg_err']);
                   }
                   ?>
                   <?php
                   $order_no = $_GET['order_id'];
                   $order_details = Order::get_order_detail($order_no);
                   ?>
                   <div id="printTable">
                   <?php
                   foreach ($order_details as $detail)
                   {
                       ?>
                       <h2 style='font-family:Adobe Heiti Std; font-size:20px;'>Shipping Information</h2><br>
                       <div class="row">
                           <div class="col-xs-12 col-md-8">
                               <table class="table table-bordered">
                           <thead>
                           <th>Customer Name</th>
                           <th>Email</th>
                           <th>Order ID</th>
                           <th>Order Date</th>
                           <th>Shipping Address</th>
                           </thead>
                           <tbody>
                               <tr>
                                   <td><?php echo($detail->customer_name);?></td>
                                   <td><?php echo($detail->customer_email);?></td>
                                   <td><?php echo($detail->order_no);?></td>
                                   <td><?php echo($detail->order_date);?></td>
                                   <td><?php echo($detail->shipping_address);?></td>
                               </tr>
                           </tbody>
                       </table><br>
                           </div>
                       </div>
                            
                  <?php
                            }
                            ?>
                       <h2>Products Detail</h2><br>
                       <div class="row">
                           <div class="col-xs-12 col-md-8">
                                <table  class="table table-bordered">
                       <thead>
                       <tr>
                           <th>Product Name</th>
                           <th>Quantity</th>
                           <th>Price</th>
                       </tr>
                       </thead>
                       <?php
                   $order_product = Order::single_product($order_no);
                   foreach ($order_product as $product_order)
                   {
                       $product_path = pathinfo($product_order->product_image);
                       $product_image_name=$product_path['filename'];
                       ?>
                       <tbody>
                      <tr>
                           <td style=''><?php echo($product_order->product_name);?></td>
                           <td><?php echo($product_order->quantity);?></td>
                           <td><?php echo($product_order->unit_price);?></td>
                       </tr>
                       </tbody>
                   <?php       
                   }
                   ?>
                   </table>
                               
                               <br>
                       <h4 class='pull-right'>SubTotal = <?php echo($detail->total_price);?></h4>
                           </div>
                       </div>
                       
                       <button class='btn btn-sm btn-danger no-print'>Print</button>
                       <a class="btn btn-sm btn-primary no-print" href="controller/complete_order.php?action=complete_order&order_id=<?php echo($order_no)?>">Complete Order</a>
                       </div>
               </div>
               
           </div>
  		
	
<?php
require_once 'views/footer.php';
?>
<script>
  function printData()
{
   var divToPrint=document.getElementById("printTable");
   newWin= window.open("");
   newWin.document.write(divToPrint.outerHTML);
   newWin.print();
   newWin.close();
}
$('button').on('click',function(){
printData();
});
</script>


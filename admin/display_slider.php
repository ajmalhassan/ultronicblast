<?php

require_once "views/top.php";
require_once '../models/slider.php';
require_once 'views/profile.php';
?>
<style>
    .table>tbody>tr>th
    {
       padding: 10px !important;
    }
</style>
</head>
<body>
<div id="wrapper">
<?php

?>
<!----->
        <nav class="navbar-default navbar-static-top" role="navigation">
             <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
               <h1> <a class="navbar-brand" href="index.php">Ultronic Blast</a></h1>         
            </div>
			 <div class=" border-bottom">
        	<?php
                require_once 'views/header.php';
                ?>
                             <div class="clearfix"></div>
                <?php
                require_once 'views/left_sidebar_nav.php';
                ?>
			<div class="clearfix"></div>
        </nav>
        <div id="page-wrapper" class="gray-bg dashbard-1">
       <div class="content-main">
           <div class="validation-system">
                <div class="validation-form">
                    <h1><i class="fa fa-sliders"></i> Slider <small>View all Slides</small></h1><hr style="margin-top: 10px; margin-bottom: 10px;">
                  <ol class="breadcrumb">
                      <li class="active"><i class="fa fa-dashboard"></i> Dashboard</li>
                      <li class="active"><i class="fa fa-sliders"></i> All slides</li>
                  </ol>
                    <h2 class="h2 pull-left">Ultronic Blast Slider 
                        <span class="error-color">
                           <?php
                           if(isset($_SESSION['msg']))
                           {
                               $msg = $_SESSION['msg'];
                               echo ($msg);
                               unset($_SESSION['msg']);
                           }
                           ?>
                        </span>
                        <a href="ulblast_slider.php" class="btn btn-primary">Add New</a>
                    </h2> 
                    
                    
                <table class="table table-responsive table-bordered table-sm table-all">
                    <thead>
                        <tr class="bg-danger" style="background-color: #337ab7; color: #fff;">
                            <th>sr #</th>
                            <th>Slide name</th>
                            <th>Link</th>
                            <th>Description</th>
                            <th>Image</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
              <?php
                $slides = Slider::get_slider();
                foreach ($slides as $s)
                {
                    ?>
                    <tbody>
                         <tr>
                            <td><?php echo($s->slide_id)?></td>
                            <td><?php echo($s->slide_name)?></td>
                            <td><?php echo($s->slide_link)?></td>
                            <td><?php echo($s->slide_description)?></td>
                            <td><img style="width:34px; height:18px;" src="<?php echo(BASE_URL."slider_images/$s->slide_name/$s->slide_image")?>" alt=""/></td>
                            <td><a href="<?php echo(BASE_URL."update_slider.php?slideID=$s->slide_ID")?>"><span><i class="fa fa-edit"></i></span></a></td>
                            <td><a class="delete-action" href="<?php echo(BASE_URL."controller/remove.php?action=remove_slide&slideID=$s->slide_ID")?>"><span><i class="fa fa-times"></i></span></a></td>
                         </tr>
                    </tbody>
                   <?php
                }
              ?>
           </table>
<!--            <nav aria-label="brand-nav">
                <ul class="pagination">
                  <li class="page-item disabled">
                    <a class="page-link" href="#" tabindex="-1">Previous</a>
                  </li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item active">
                    <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                  </li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item">
                    <a class="page-link" href="#">Next</a>
                  </li>
                </ul>
            </nav>-->
	        </div>
	    </div>
           
	
<?php
require_once 'views/footer.php';


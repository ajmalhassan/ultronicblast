<?php
require_once 'views/top.php';
require_once './views/profile.php';
require_once '../models/user.php';
require_once '../models/product.php';
require_once '../models/category.php';
require_once '../models/brand.php';
require_once '../models/Order.php';
?>
</head>
<body>
<div id="wrapper">

<!----->
        <nav class="navbar-default navbar-static-top" role="navigation">
             <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
               <h1> <a class="navbar-brand" href="index.php">Ultronic Blast</a></h1>         
            </div>
			 <div class=" border-bottom">
        	<?php
                require_once 'views/header.php';
                ?>
                             <div class="clearfix"></div>
                <?php
                require_once 'views/left_sidebar_nav.php';
                ?>
			<div class="clearfix"></div>
        </nav>
        <div id="page-wrapper" class="gray-bg dashbard-1">
       <div class="content-main">
           <div class="blank">
         <div class="blank-page">
             <h1><i class="fa fa-opencart"></i> New Order <small>Customer orders</small></h1><hr style="margin-top: 10px; margin-bottom: 10px;">
                  <ol class="breadcrumb">
                      <li class=""><i class="fa fa-dashboard"></i> Dashboard</li>
                      <li class="active"><i class="fa fa-opencart"></i> New Order</li>
                  </ol>
 
<div class="table-responsive" ><!-- table-responsive Starts -->

<table class="table table-bordered table-hover table-striped" ><!-- table table-bordered table-hover table-striped Starts -->

<thead><!-- thead Starts -->

<tr>
<th>Order No</th>
<th>Customer Name</th>
<th>Customer Email</th>
<th>Product Qty</th>
<th>View</th>
<th>Delete</th>
</tr>
</thead><!-- thead Ends -->

<tbody><!-- tbody Starts -->

<?php
echo ("New Orders<br>");
    $start = isset($_GET['start']) ? $_GET['start'] : 0;
    $count = isset($_GET['count']) ? $_GET['count'] : ITEM_PER_PAGE;
    $orders = Order::get_orders($start, $count);
 
 foreach ($orders as $order)
 {
   ?>
    <tr>
    <td><?php echo($order->order_no)?></td>
    <td><?php echo($order->customer_name)?></td>
    <td><?php echo($order->customer_email)?></td>
    <td><?php echo($order->total_quantity)?></td>
    <td><a href="order_detail.php?order_id=<?php echo($order->order_no);?>"><i class='fa fa-eye'></i></a></td>
    <td><a class='delete-action' href="<?php echo (BASE_URL."controller/remove_order.php?action=remove_order&order_id=$order->order_no")?>"><i class="fa fa-times"></i></a></td>
    </tr>
 <?php
 
 }

?>

</tbody><!-- tbody Ends -->


</table><!-- table table-bordered table-hover table-striped Ends -->
<nav aria-label="brand-nav">
                <ul class="pagination">
                  <?php
                    $pNums = Order::pagination_order(ITEM_PER_PAGE);
                     foreach ($pNums as $pNo=>$start)
                     {
                        echo("<li class='page-link'><a href='" . BASE_URL . "new_order.php?start=$start'>$pNo <span class='sr-only'>(current)</span></a></li>");
                     }
                     
                 ?>
                </ul>
            </nav>
              
</div><!-- table-responsive Ends -->
                       </div>
                    </div>
           
  		
	
<?php
require_once 'views/footer.php';



<?php
//session_start();
require_once '../models/admin.php';
require_once 'views/top.php';
?>
</head>
<body>
<div class="login">
<h1><a href="index.php">Ultronic Blast </a></h1>
    <div class="login-bottom">
    <h2>Login
        <span class="error-color">
            <?php
                if(isset($_SESSION['msg']))
                {
                    $msg = $_SESSION['msg'];
                    echo ($msg);
                    unset($_SESSION['msg']);
                }
                if(isset($_SESSION['errors']))
                {
                    $errors = $_SESSION['errors'];
                }
                if(isset($_SESSION['obj_admin']))
                {
                    $obj_admin = unserialize($_SESSION['obj_admin']);
                }
                 else {
                        $obj_admin = new Admin();
                 }
            ?>
        </span>
    </h2>
    <form action="controller/login_process.php" method="post">
       
    <div class="col-md-12 col-lg-6 col-lg-offset-3">
        <div class="login-mail" <?php if(isset($errors['email']))
            {
                echo $errors['email'];
            }
             ?>
             >
            <input type="text" name="email" value="<?php echo $obj_admin->email; ?>" placeholder="Email" required="">
            <i class="fa fa-envelope"></i>
        </div>
        <div class="login-mail">
            <input type="password" name="password"  placeholder="Password" required="">
            <i class="fa fa-lock"></i>
        </div>
           <a class="news-letter " href="#">
               <label class="checkbox1"><input type="checkbox" name="remember" ><i> </i>Remember</label>
          </a>
        <div class="col-md-6 login-do">
        <label class="hvr-shutter-in-horizontal login-sub">
            <input type="submit" value="login">
            </label>
<!--            <p>Do not have an account?</p>
        <a href="signup.php" class="hvr-shutter-in-horizontal">create account</a>-->
    </div>
    </div>
<!--    <div class="col-md-6 login-do">
        <label class="hvr-shutter-in-horizontal login-sub">
            <input type="submit" value="login">
            </label>
            <p>Do not have an account?</p>
        <a href="signup.php" class="hvr-shutter-in-horizontal">create account</a>
    </div>-->

    <div class="clearfix"> </div>
    </form>
    </div>
</div>
    <?php
require_once 'views/footer.php';
   
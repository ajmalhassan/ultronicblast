<?php
session_start();
require_once '../models/admin.php';
require_once '../models/slider.php';
define("ITEM_PER_PAGE", 5);
define("BASE_FOLDER", "/ultronicblast/admin/");
define("BASE_URL", "http://" . $_SERVER['HTTP_HOST'].BASE_FOLDER);
if(isset($_COOKIE['obj_admin']) && !isset($_SESSION['obj_admin'])){
    $_SESSION['obj_admin'] = $_COOKIE['obj_admin'];
}
if(isset($_SESSION['obj_admin'])){
    $obj_admin = unserialize($_SESSION['obj_admin']);
}
else{
    $obj_admin = new Admin();
}
$current = $_SERVER['PHP_SELF'];
$public_pages = array(
            BASE_FOLDER . "signin.php",
            BASE_FOLDER . "signup.php",
            BASE_FOLDER . "add_product.php",
            BASE_FOLDER . "active.php",
            BASE_FOLDER . "add_brand.php",
            BASE_FOLDER . "add_category.php",
            BASE_FOLDER . "change_account.php",
            BASE_FOLDER . "change_image.php",
            BASE_FOLDER . "change_password.php",
            BASE_FOLDER . "display_brands.php",
            BASE_FOLDER . "display_categories.php",
            BASE_FOLDER . "display_products.php",
            BASE_FOLDER . "message.php",
            BASE_FOLDER . "index.php",
            BASE_FOLDER . "profile.php",
            BASE_FOLDER . "ulblast_slider.php",
);
$restricted_index_page= array(BASE_FOLDER . "index.php"); 
$restricted_addproduct_page = array(
    BASE_FOLDER . "add_product.php", 
    BASE_FOLDER . "ulblast_slider.php",
    BASE_FOLDER . "active.php", 
    BASE_FOLDER . "add_brand.php",
    BASE_FOLDER . "add_category.php",
    BASE_FOLDER . "change_account.php",
    BASE_FOLDER . "change_image.php",
    BASE_FOLDER . "change_password.php",
    BASE_FOLDER . "display_brands.php",
    BASE_FOLDER . "display_categories.php",
    BASE_FOLDER . "display_products.php",
    BASE_FOLDER . "index.php",
    BASE_FOLDER . "message.php",
    ); 
//if(!$obj_admin->login && in_array($current, $public_pages))
//{
//   $_SESSION['msg'] = "You must logout to view this page222";
//    header("Location:msg.php");
//}
if(!$obj_admin->login && in_array($current, $restricted_index_page))
{
//    $_SESSION['msgindexpage'] = "You must login first";
//    header("Location:msg.php");
    header("Location:signin.php");
}
if(!$obj_admin->login && in_array($current, $restricted_addproduct_page))
{
//    $_SESSION['msgaddproductpage'] = "You must login first";
//    header("Location:msg.php");
        header("Location:signin.php");

}

?>

<!DOCTYPE HTML>
<html>
<head>
<title>Ultronic Blast</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Minimal Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<link href="css/font-awesome.css" rel="stylesheet"> 
<!--Image upload preview-->
<link href="css/jpreview.css" rel="stylesheet" type="text/css"/>
<link rel="icon" type="image/icon" href="images/favicon.ico"/>
<script src="extra/tinymce/tinymce.min.js" type="text/javascript"></script>
<script>tinymce.init({ selector:'textarea' });</script>
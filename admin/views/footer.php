<div class="copy">
    <p> &copy; <?php echo date("Y"); ?> Ultronic Blast. All Rights Reserved | Design by <a href="<?php echo(BASE_URL);?>">Ultronic Blast</a> </p>
 </div>
		</div>
		<div class="clearfix"> </div>
       </div>
     </div>
<!---->
<script src="js/jquery.min.js"> </script>
<!-- Mainly scripts -->
<script src="js/jquery.metisMenu.js"></script>
<!--<script src="js/jquery.slimscroll.min.js"></script>
 Custom and plugin javascript -->
<link href="css/custom.css" rel="stylesheet">
<script src="js/custom.js"></script>
<script src="js/screenfull.js"></script>
		<script>
		$(function () {
			$('#supported').text('Supported/allowed: ' + !!screenfull.enabled);

			if (!screenfull.enabled) {
				return false;
			}
			$('#toggle').click(function () {
				screenfull.toggle($('#container')[0]);
			});
		});
		</script>

<!----->

<!--pie-chart--->
<!--<script src="js/pie-chart.js" type="text/javascript"></script>
 <script type="text/javascript">

        $(document).ready(function () {
            $('#demo-pie-1').pieChart({
                barColor: '#3bb2d0',
                trackColor: '#eee',
                lineCap: 'round',
                lineWidth: 8,
                onStep: function (from, to, percent) {
                    $(this.element).find('.pie-value').text(Math.round(percent) + '%');
                }
            });

            $('#demo-pie-2').pieChart({
                barColor: '#fbb03b',
                trackColor: '#eee',
                lineCap: 'butt',
                lineWidth: 8,
                onStep: function (from, to, percent) {
                    $(this.element).find('.pie-value').text(Math.round(percent) + '%');
                }
            });

            $('#demo-pie-3').pieChart({
                barColor: '#ed6498',
                trackColor: '#eee',
                lineCap: 'square',
                lineWidth: 8,
                onStep: function (from, to, percent) {
                    $(this.element).find('.pie-value').text(Math.round(percent) + '%');
                }
            });

           
        });

    </script>-->
<!--skycons-icons-->
<script src="js/skycons.js"></script>
<script src="js/jquery.limitkeypress.js" type="text/javascript"></script>
<!--//skycons-icons-->
<!--<script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>-->

	
	<script src="js/scripts.js"></script>
	<!--//scrolling js-->
	<script src="js/bootstrap.min.js"> </script>
        <script type="text/javascript">
    $(document).ready(function(){
        $(".string-validate").limitkeypress({ rexp: /^[A-Za-z0-9. ]*$/});
        $(".number-validate").limitkeypress({ rexp: /^[0-9]*$/});
        $(".slider-string-validate").limitkeypress({ rexp: /^[a-zA-Z0-9]*$/});
    });
</script>
<script language="JavaScript" type="text/javascript">
$(document).ready(function(){
    $("a.delete-action").click(function(e){
        if(!confirm('Are you sure?')){
            e.preventDefault();
            return false;
        }
        return true;
    });
});
</script>
 <script>
$(document).on("change keyup blur", "#discount", function() {
var main = $('#unit-price').val();
var disc = $('#discount').val();
var dec = (disc/100).toFixed(2); //its convert 10 into 0.10
var mult = main*dec; // gives the value for subtract from main value
var discont = main-mult;
$('#sale-price').val(discont);
});
</script>
<!--<script>
$(document).ready(function(){
    
  $(".error-color").fadeOut(2000);
 
});
</script>-->

</body>
</html>
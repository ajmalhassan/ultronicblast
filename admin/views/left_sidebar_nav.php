<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">

        <li>
            <a href="index.php" class=" hvr-bounce-to-right"><i class="fa fa-dashboard nav_icon "></i><span class="nav-label">Dashboards</span> </a>
        </li>
        <li>
            <a href="../index.php" class=" hvr-bounce-to-right" target="_blank"><i class="fa fa-dashboard nav_icon "></i><span class="nav-label">Visit Site</span> </a>
        </li>

        <li>
            <a href="#" class=" hvr-bounce-to-right"><i class="fa glyphicon-menu-hamburger nav_icon"></i> <span class="nav-label">Product</span><span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
                <li><a href="add_product.php" class=" hvr-bounce-to-right"> <i class="fa fa-product-hunt nav_icon"></i>Add New Product</a></li>
                <li><a href="display_products.php" class=" hvr-bounce-to-right"><i class="fa fa-briefcase nav_icon"></i>All Products</a></li>
             </ul>
        </li>
        <li>
            <a href="../admin/categories.php" class=" hvr-bounce-to-right"><i class="fa fa-indent nav_icon"></i> <span class="nav-label">Categories</span></a>
        </li> 
        <li>
            <a href="../admin/brands.php" class=" hvr-bounce-to-right"><i class="fa fa-indent nav_icon"></i> <span class="nav-label">Brand</span></a>
        </li>
        <li>
            <a href="#" class=" hvr-bounce-to-right"><i class="fa fa-indent nav_icon"></i> <span class="nav-label">ULBlast Slider</span><span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
                <li><a href="../admin/ulblast_slider.php" class=" hvr-bounce-to-right"> <i class="fa fa-area-chart nav_icon"></i>Add Slide</a></li>
                <li><a href="../admin/display_slider.php" class=" hvr-bounce-to-right"><i class="fa fa-map-marker nav_icon"></i>View Slider</a></li>
             </ul>
        </li> 
         <li>
            <a href="../admin/new_order.php" class=" hvr-bounce-to-right"><i class="fa fa-cog nav_icon"></i> <span class="nav-label">New Order</span></a>
            <a href="../admin/order_complete.php" class=" hvr-bounce-to-right"><i class="fa fa-cog nav_icon"></i> <span class="nav-label">Complete Order</span></a>
        </li>
        <li>
            <a href="../admin/display_users.php" class=" hvr-bounce-to-right"><i class="fa fa-cog nav_icon"></i> <span class="nav-label">Customers</span></a>
            <a href="../admin/display_admins.php" class=" hvr-bounce-to-right"><i class="fa fa-cog nav_icon"></i> <span class="nav-label">Admins</span></a>
            <a href="../admin/subscribed.php" class=" hvr-bounce-to-right"><i class="fa fa-cog nav_icon"></i> <span class="nav-label">Subscribed</span></a>
        </li>
    </ul>
</div>
</div>

<div class="full-left">
    
<section class="full-top">
   
    <button id="toggle"><i class="fa fa-arrows-alt"></i></button>
        
    <span class="error-color">
        <?php
        if(isset($_SESSION['order_msg']))
        {
            $order_msg = $_SESSION['order_msg'];
            echo($order_msg);
            unset($_SESSION['order_msg']);
        }
        ?>
    </span>
</section>

<div class="clearfix"> </div>
</div>
<div class="drop-men" >
    
    <ul class=" nav_1 my_nav">
    
    <li class="dropdown at-drop">
       <a href="add_admin.php"><i class="fa fa-user-plus"></i> Add User</a>
    </li>
    <li class="dropdown">
          <a href="#" class="dropdown-toggle dropdown-at" data-toggle="dropdown"><span class=" name-caret"><?php echo ($obj_admin->admin_name);?><i class="caret"></i></span></a>
          <ul class="dropdown-menu " role="menu">
              <li><a href="../admin/edit_profile.php"><i class="fa fa-user"></i>Edit Profile</a></li>
            
            <li>
                <?php
                if($obj_admin->login)
                {
                ?>
                <a href="<?php echo (BASE_URL);?>controller/logout_process.php"><i class="fa fa-sign-out"></i> Logout</a>
                <?php
                }
                else
                {
                ?>
                <a href="<?php echo (BASE_URL);?>controller/login_process.php"><i class="fa fa-sign-in"></i> Login</a>
                <?php
                }
                ?>
            </li>
          </ul>
    </li>
    <li class="dropdown at-drop">
        <img class="admin-image img-circle img-thumbnail" style="height: 40px; width: 40px; margin-top: 5px; margin-right: 5px;" src="<?php echo $obj_admin->profile_image?>"/>
        <!--<img class="admin-image img-circle img-thumbnail" style="height: 40px; width: 40px; margin-top: 5px; margin-right: 5px;" src="admins/sss@gmail.com/sss@gmail.com.jpg"/>-->
    </li>

    </ul>
 </div>
<?php
//session_start();
require_once '../models/admin.php';
require_once '../models/product.php';
require_once '../models/category.php';
require_once '../models/brand.php';
require_once 'views/top.php';
require_once 'views/profile.php';

?>

</head>
<body>
<div id="wrapper">

<!----->
        <nav class="navbar-default navbar-static-top" role="navigation">
             <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
               <h1> <a class="navbar-brand" href="index.php">Ultronic Blast</a></h1>         
            </div>
			 <div class=" border-bottom">
        	<?php
                //require_once 'views/searchbar.php';
                require_once 'views/header.php';
                ?>
			<div class="clearfix"></div>
	  
		    <?php
                    require_once 'views/left_sidebar_nav.php';
                    ?>
        </nav>
        <div id="page-wrapper" class="gray-bg dashbard-1">
       <div class="content-main">
 
  	<div class="validation-system">
 		
 		<div class="validation-form">
        <h2>Update product 
            <span class="error-color">
                <?php
             if(isset($_SESSION['msg']))
            {
                $msg = $_SESSION['msg'];
                echo ($msg);
                unset($_SESSION['msg']);
            }
        ?>
            </span>
        
        </h2>
        <?php
            if(isset($_SESSION['errors']))
            {
                $errors = $_SESSION['errors'];
                unset($_SESSION['errors']);
            }
            
            if(isset($_SESSION['obj_product']))
            {
                $obj_product = unserialize($_SESSION['obj_product']);
               
            }
            else
            {
                $obj_product = new product();
                
            }
        ?>
<?php
        try {
            
       $keypro = $_GET['productID'];
       if(isset($keypro))
       {
           $keypro = $_GET['productID'];
       }
//    echo ($keypro);
//    die;
    $products = $obj_product->get_single_product($keypro);    
    foreach ($products as $a)
        {
        ?> 
        <form action="controller/update_product_process.php?productID=<?php echo $a->productID?>" method="post" enctype="multipart/form-data">
            <div class="vali-form">
                <div class="col-md-6 form-group1">
                  <label class="control-label">Product Name</label>
                  <input type="text" placeholder="product Name" class="string-validate" name="product_name" value="<?php echo ($a->product_name);?>" required="">
                  <p class="error-color">
                      
                  </p>
                </div>
                <div class="col-md-6 form-group1 form-last">
                  <label class="control-label">Unit Price</label>
                  <input type="text" placeholder="Unit Price" class="number-validate" name="unit_price" value="<?php echo($a->unit_price);?>" required="">
                  <p class="error-colo">
                     
                  </p>
                </div>
                <div class="clearfix"> </div>
            </div>
            
            <div class="col-md-6 form-group1 group-mail">
              <label class="control-label">Quantity</label>
              <input type="text" placeholder="Quantity" name="quantity" value="<?php echo($a->quantity);?>" class="number-validate" required="">
              <p class="error-color">
                  
              </p>
            </div>
            <div class="col-md-6 form-group1 group-mail">
              <label class="control-label">Product Discount (%)</label>
              <input type="text" placeholder="Product Discount" name="product_discount" value="<?php echo($a->product_discount);?>" class="number-validate" required="">
              <p class="error-color">
                  
              </p>
            </div>
            
             <div class="clearfix"> </div>
            <div class="col-md-12 form-group1 ">
              <label class="control-label">Product Description</label>
              <textarea  placeholder="Product Description" name="product_description" class="string-validate" required="" cols="120"><?php echo($a->description);?></textarea>
               <!--<textarea placeholder="Product Description" name="product_description"></textarea>-->
              <p class="error-color">
                
              </p>
            </div>
             
             <div class="clearfix"> </div>
             <div class="col-md-6 form-group">
                 <input type="file" id="product_image" value="" name="product_image" required="">
                 <p class="error-color">
                     
                 </p>
            </div>
            <div class="col-md-12 form-group">
              <button type="submit" class="btn btn-primary">Update Product</button>
              
            </div>
          <div class="clearfix"> </div>
        </form>
      <?php
        }
        } catch (Exception $ex) {
            echo($ex->getMessage());
        }

        
    
?>
        
    
 	<!---->
 </div>

</div>	
	
<?php
require_once 'views/footer.php';



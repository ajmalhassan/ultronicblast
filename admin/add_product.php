<?php
//session_start();
require_once '../models/product.php';
require_once '../models/category.php';
require_once '../models/brand.php';
require_once "views/top.php";
require_once 'views/profile.php';

?>

</head>
<body>
<div id="wrapper">

<!----->
        <nav class="navbar-default navbar-static-top" role="navigation">
             <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
               <h1> <a class="navbar-brand" href="index.php">Ultronic Blast</a></h1>         
            </div>
			 <div class=" border-bottom">
        	<?php
                //require_once 'views/searchbar.php';
                require_once 'views/header.php';
                ?>
			<div class="clearfix"></div>
	  
		    <?php
                    require_once 'views/left_sidebar_nav.php';
                    ?>
        </nav>
        <div id="page-wrapper" class="gray-bg dashbard-1">
       <div class="content-main">
 
  	<div class="validation-system">
 	<div class="validation-form">
            <h1><i class="fa fa-product-hunt"></i> Add Product <small>Add New Product</small></h1><hr style="margin-top: 10px; margin-bottom: 10px;">
                  <ol class="breadcrumb">
                      <li><i class="fa fa-dashboard"></i> Dashboard</li>
                      <li class="active"><i class="fa fa-file-text"></i> New Product</li>
                  </ol>
            <p> 
            <span class="error-color">
                <?php
             if(isset($_SESSION['msg']))
            {
                $msg = $_SESSION['msg'];
                echo ($msg);
                unset($_SESSION['msg']);
            }
        ?>
            </span>
        
        </p>
        <?php
            if(isset($_SESSION['errors']))
            {
                $errors = $_SESSION['errors'];
                unset($_SESSION['errors']);
            }
            if(isset($_SESSION['obj_product']))
            {
                $obj_product = unserialize($_SESSION['obj_product']);
                unset($_SESSION['obj_product']);
               
            }
            else
            {
                $obj_product = new product();
                
            }
//            if(isset($_SESSION['obj_category']))
//            {
//                $obj_category = unserialize($_SESSION['obj_category']);
//            }
//            else {
//                $obj_category = new Category();
//            }
        ?>
        <form action="controller/product_process.php" method="post" enctype="multipart/form-data">
            <div class="vali-form">
                <div class="col-md-6 form-group1">
                  <label class="control-label">Product Name</label>
                  <input type="text" placeholder="product Name" class="string-validate" name="product_name" value="<?php echo $obj_product->product_name; ?>" required="">
                  <p class="error-color">
                      <?php
                      if(isset($errors['product_name']))
                      {
                          echo ($errors['product_name']);
                      }
                      ?>
                  </p>
                </div>
                <div class="col-md-6 form-group1 form-last">
                  <label class="control-label">Quantity</label>
                  <input type="text" placeholder="Quantity"  class="number-validate" name="quantity" value="<?php echo $obj_product->quantity; ?>" required="">
                  <p class="error-color">
                      <?php
                      if(isset($errors['quantity']))
                      {
                          echo ($errors['quantity']);
                      }
                      ?>
                  </p>
                </div>
                <div class="clearfix"> </div>
            </div>
            
            <div class="col-md-6 form-group1 group-mail">
              <label class="control-label">Unit Price</label>
              <input type="text" placeholder="Unit Price" id="unit-price" name="unit_price" value="<?php echo $obj_product->unit_price?>" class="number-validate" required="">
              <p class="error-color">
                  <?php
                      if(isset($errors['unit_price']))
                      {
                          echo ($errors['unit_price']);
                      }
                      ?>
              </p>
            </div>
            <div class="col-md-3 form-group1 group-mail">
              <label class="control-label">Product Discount %</label>
              <input type="text" placeholder="Product Discount" id="discount" name="product_discount" value="<?php echo $obj_product->product_discount; ?>" class="number-validate" required="">
              <p class="error-color">
                  <?php
                      if(isset($errors['product_discount']))
                      {
                          echo ($errors['product_discount']);
                      }
                      ?>
              </p>
            </div>
            <div class="col-md-3 form-group1 group-mail">
              <label class="control-label">Sale Price</label>
              <input type="text" placeholder="Sale Price" id="sale-price" name="sale_price" value="<?php echo $obj_product->sale_price; ?>" class="number-validate" readonly=""  required="">
              <p class="error-color">
                  <?php
                      if(isset($errors['sale_price']))
                      {
                          echo ($errors['sale_price']);
                      }
                      ?>
              </p>
            </div>
            
             <div class="clearfix"> </div>
            <div class="col-md-12 form-group1 ">
              <label class="control-label">Long Description</label>
              <!--<textarea  placeholder="Long Product Description" name="product_description" class="string-validate" required="" cols="120"><?php echo $obj_product->description; ?></textarea>-->
              <textarea placeholder="Long Product Description" name="product_description" cols="140"><?php echo $obj_product->description; ?></textarea>
              <p class="error-color">
                  <?php
                      if(isset($errors['description']))
                      {
                          echo ($errors['description']);
                      }
                      ?>
              </p>
            </div>
             <div class="clearfix"> </div>
             <div class="col-md-12 form-group1 ">
              <label class="control-label">Short Description</label>
              <!--<textarea  placeholder="Short Product Description" name="product_description" class="string-validate" required="" cols="120"><?php echo $obj_product->description; ?></textarea>-->
              <textarea placeholder="Short Product Description" name="short_description" cols="140"><?php echo $obj_product->short_description; ?></textarea>
              <p class="error-color">
                  <?php
                      if(isset($errors['short_description']))
                      {
                          echo ($errors['short_description']);
                      }
                      ?>
              </p>
            </div>
             <div class="clearfix"> </div>
             <div class="col-md-6 form-group2 group-mail">
              <label class="control-label">Category</label>
            <select name="category_id">
                <?php
                  try {
                      echo "<option value='0'>Select Category</option>";
                      $category = Category::get_cat();
                      
                      foreach ($category as $c)
                      {
                          echo ("<option value='$c->categoryID'>$c->category_name</option>");
                      }
                  } catch (Exception $ex) {
                      echo ("<option>".$ex->getMessage(). "</option>");
                  }
                ?>
            </select>
              <p class="error-color">
                  <?php
                  if(isset($errors['category_id']))
                  {
                      echo ($errors['category_id']);
                  }
                  ?>
              </p>
            </div>
             <div class="col-md-6 form-group2 group-mail">
              <label class="control-label">Brand</label>
            <select name="brand_id">
            	<?php
                  try {
                       echo ("<option value=''>Select Brand</option>");
                       $brands = Brand::get_bra();
                       foreach ($brands as $b)
                       {
                           echo("<option value='$b->brandID'>$b->brand_name</option>");
                       }
                  } catch (Exception $ex) {
                      echo ("<option>".$ex->getMessage()."</option>");
                      }
                ?>
            </select>
              <p class="error-color"></p>
            </div>
             <div class="clearfix"> </div>
             <div class="col-md-6 form-group">
                 <input type="file" id="product_image" value="" name="product_image"/>
                 <p class="error-color">
                     <?php
                      if(isset($errors['product_image']))
                      {
                          echo ($errors['product_image']);
                      }
                      ?>
                 </p>
            </div>
             <div class="clearfix"> </div>
             <div class="col-md-6 form-group">
                 <input type="file" id="product_image2" value="" name="product_image2"/>
                 <p class="error-color">
                     <?php
                      if(isset($errors['product_image2']))
                      {
                          echo ($errors['product_image2']);
                      }
                      ?>
                 </p>
            </div>
             <div class="clearfix"> </div>
             <div class="col-md-6 form-group">
                 <input type="file" id="product_image3" value="" name="product_image3"/>
                 <p class="error-color">
                     <?php
                      if(isset($errors['product_image3']))
                      {
                          echo ($errors['product_image3']);
                      }
                      ?>
                 </p>
            </div>
            <div class="col-md-12 form-group">
              <button type="submit" class="btn btn-primary">Add Product</button>
              <!--<input type="reset" class="btn btn-default"/>-->
            </div>
          <div class="clearfix"> </div>
        </form>
                   
   
 	<!---->
 </div>

</div>	
	
<?php
require_once 'views/footer.php';



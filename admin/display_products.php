<?php
require_once "views/top.php";
require_once '../models/product.php';
require_once 'views/profile.php';
?>
<style>
    .table>tbody>tr>th
    {
       padding: 10px !important;
    }
</style>
</head>
<body>
<div id="wrapper">
<?php

?>
<!----->
        <nav class="navbar-default navbar-static-top" role="navigation">
             <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
               <h1> <a class="navbar-brand" href="index.php">Ultronic Blast</a></h1>         
            </div>
			 <div class=" border-bottom">
        	<?php
                require_once 'views/header.php';
                ?>
                             <div class="clearfix"></div>
                <?php
                require_once 'views/left_sidebar_nav.php';
                ?>
			<div class="clearfix"></div>
        </nav>
        <div id="page-wrapper" class="gray-bg dashbard-1">
       <div class="content-main">
           <div class="validation-system">
                <div class="validation-form">
                     <h1><i class="fa fa-product-hunt"></i> View Products <small>Display all products</small></h1><hr style="margin-top: 10px; margin-bottom: 10px;">
                        <ol class="breadcrumb">
                            <li><i class="fa fa-dashboard"></i> Dashboard</li>
                            <li class="active"><i class="fa fa-product-hunt"></i> View All Products</li>
                        </ol>
                    <h2 class="h2">All Products 
                        <span class="error-color">
                            <?php 
                            if(isset($_SESSION['msg']))
                            {
                                $msg = $_SESSION['msg'];
                                echo ($msg);
                                unset($_SESSION['msg']);
                            }
                                
                            ?>
                        </span>
                    </h2> 
                    
    <div class="">
         <table class="table table-bordered table-condensed table-all">
             <thead>   
             <tr class="bg-danger" style="background-color: #337ab7; color: #fff;">
                   <th>sr #</th>
                   <th>Product name</th>
                   <th>Unit Price</th>
                   <th>Sale Price</th>
                   <th>Quantity</th>
                   <th>Discount</th>
                   <th>Category</th>
                   <th>Brand</th>
                   <th>image</th>
                   <th>Edit</th>
                   <th>Delete</th>
               </tr>
             </thead>
             <tbody>
               <?php
             try {
                 $start = isset($_GET['start']) ? $_GET['start'] : 0;
                 $count = isset($_GET['count']) ? $_GET['count'] : ITEM_PER_PAGE;
                 $products = product::get_products($start, $count);
                
               foreach ($products as $p){
                   $category_path =  pathinfo($p->product_image);
                   $product_image_name =$category_path['filename'] ;
                   ?>
                       <tr>
                        <td><?php echo($p->productID);?></td>
                        <td><?php echo($p->product_name);?></td>
                        <td><?php echo("$".$p->unit_price);?></td>
                        <td><?php echo($p->sale_price);?></td>
                        <td><?php echo($p->quantity);?></td>
                        <td><?php echo($p->product_discount ."%");?></td>
                        <td><?php echo($p->brand);?></td>
                        <td><?php echo($p->category);?></td>
                        <td><img src="<?php echo(BASE_URL."products/$product_image_name/$p->product_image")?>" width="30px"/></td>
                        <td><a href="<?php echo(BASE_URL."update_product.php?productID=$p->productID")?>" ><i class="fa fa-pencil"></i></a></td>
                        <td><a class="delete-action" href="<?php echo(BASE_URL."controller/remove.php?action=remove_product&productID=$p->productID")?>" ><i class='fa fa-times'></i></a></td>   
                       </tr>
               <?php
               }
               ?>
             </tbody>
                </table>
            </div>
              <nav aria-label="brand-nav">
                <ul class="pagination">
                  <?php
                    $pNums = product::pagination(ITEM_PER_PAGE);
                     foreach ($pNums as $pNo=>$start)
                     {
                        echo("<li class='page-link'><a href='" . BASE_URL . "display_products.php?start=$start'>$pNo <span class='sr-only'>(current)</span></a></li>");
                     }
                     
                 ?>
                </ul>
                </nav>
               <?php
               
                } catch (Exception $ex) {
                    echo $ex->getMessage();
                }

               ?>
	        </div>
	    </div>
           
	
<?php
require_once 'views/footer.php';



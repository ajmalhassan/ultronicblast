<?php
session_start();
require_once '../../models/admin.php';
$obj_admin = new Admin();
$errors = array();
//echo("<pre>");
//print_r($_FILES);
//echo("</pre>");
//die;
 try {
        $obj_admin->first_name = $_POST['first_name'];
    } catch (Exception $ex) {
        $errors['first_name'] = $ex->getMessage();
    }
    try {
        $obj_admin->last_name = $_POST['last_name'];
    } catch (Exception $ex) {
        $errors['last_name'] = $ex->getMessage();
    }
    try {
        $obj_admin->admin_name = $_POST['admin_name'];
    } catch (Exception $ex) {
        $errors['admin_name'] = $ex->getMessage();
    }
    try {
        $obj_admin->email = $_POST['email'];
    } catch (Exception $ex) {
        $errors['email'] = $ex->getMessage();
    }
    try {
        $obj_admin->password = $_POST['password'];
    } catch (Exception $ex) {
        $errors['password'] = $ex->getMessage();
    }
    try {
        $obj_admin->profile_image = $_FILES['profile_image'];
    } catch (Exception $ex) {
        $errors['profile_image'] = $ex->getMessage();
    }
    try {
    $obj_admin->verify_retype_password($_POST['password2'], $_POST['password']);
} catch (Exception $ex) {
   $errors['retype_password'] = $ex->getMessage();
}
try {
    $obj_admin->check_user($_POST['email']);
} catch (Exception $ex) {
    echo $errors['check_email'] = $ex->getMessage();
}

//$errors[0] ="";
if(count($errors) == 0)
{
    try {
        //$act_code = md5(crypt(rand(), 'ai'));
//        $obj_admin->add_admin($act_code);
        $obj_admin->add_admin();
        $msg = "<span style='color:green;'>New Admin has been added</span>";
        $_SESSION['msg'] = $msg;
        //$msg_email = "Click to <a href='http://localhost:8080/ultronicblast/admin/activate.php?act_code=$act_code&admin_ID=$obj_admin->admin_ID'>ACTIVATE</a>";
        $obj_admin->upload_profile_image($_FILES['profile_image']['tmp_name']);
        //$_SESSION['msg_email'] = $msg_email;
        header("Location:../add_admin.php");
        
    } catch (Exception $ex) {
       $_SESSION['msg_err'] = $ex->getMessage();
    }
}
else
{
   $msg = "<span style='color:red;'>New Admin has not been added</span>";
   $_SESSION['msg'] = $msg;
   $_SESSION['errors'] = $errors;
   $_SESSION['obj_admin'] = serialize($obj_admin);
   header("Location:../add_admin.php");
}
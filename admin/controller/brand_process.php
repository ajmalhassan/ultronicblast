<?php
session_start();
require_once '../../models/brand.php';
$obj_brand = new Brand();
$errors = array();
try {
    $obj_brand->brand_name = $_POST['brand_name'];
} catch (Exception $ex) {
    $errors['brand_name'] = $ex->getMessage();
}
if(count($errors) == 0)
{
    try {
        $obj_brand->add_brand();
        $msg = "New Brand Inserted";
        $_SESSION['msg'] = $msg;
        header("Location:../brands.php");
        $_SESSION['msg'] = $ex->getMessage();
    } catch (Exception $ex) {
        $_SESSION['msg'] = $ex->getMessage();
        header("Location:../add_brand.php");
    }
}
else
{
    $msg = "*Check Your Error";
    $_SESSION['msg'] = $msg;
    $_SESSION['errors'] = $errors;
    $_SESSION['obj_brand'] = serialize($obj_brand);
    header("Location:../brands.php");
}




<?php
session_start();
require_once '../../models/user.php';

if(isset($_SESSION['obj_user']))
{
    $obj_user = unserialize($_SESSION['obj_user']);
}
 else {
     $obj_user = new User();
}
if (isset($_GET['action'])) {
   
    switch ($_GET['action']) {
        
        case "remove_user":
            $key = ($_GET['user_id']);
            $obj_user->delete_user($key);
            break;
    }
}
$_SESSION['obj_user'] = serialize($obj_user);
header("Location:" . $_SERVER['HTTP_REFERER']);
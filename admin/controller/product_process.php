<?php
session_start();
require_once '../../models/product.php';
$obj_product = new product();
$errors = array();
try {
    $obj_product->product_name = $_POST['product_name'];
} catch (Exception $ex) {
    $errors['product_name'] = $ex->getMessage();
} 
try {
    $obj_product->product_image_name = $_POST['product_name'];
} catch (Exception $ex) {
    $errors['product_image_name'] = $ex->getMessage();
}
try {
    $obj_product->product_image_name2 = $_POST['product_name'];
} catch (Exception $ex) {
    $errors['product_image_name2'] = $ex->getMessage();
}
try {
    $obj_product->product_image_name3 = $_POST['product_name'];
} catch (Exception $ex) {
    $errors['product_image_name3'] = $ex->getMessage();
}
try {
    $obj_product->description = $_POST['product_description'];
} catch (Exception $ex) {
    $errors['description'] = $ex->getMessage();
}
try {
    $obj_product->short_description = $_POST['short_description'];
} catch (Exception $ex) {
    $errors['short_description'] = $ex->getMessage();
}
try {
    $obj_product->unit_price = $_POST['unit_price'];
} catch (Exception $ex) {
    $errors['unit_price'] = $ex->getMessage();
}
try {
    $obj_product->quantity = $_POST['quantity'];
} catch (Exception $ex) {
    $errors['quantity'] = $ex->getMessage();
}
try {
    $obj_product->product_discount = $_POST['product_discount'];
} catch (Exception $ex) {
    $errors['product_discount'] = $ex->getMessage();
}
try {
    $obj_product->sale_price = $_POST['sale_price'];
} catch (Exception $ex) {
    $errors['sale_price'] = $ex->getMessage();
}
try {
    $obj_product->category = $_POST['category_id'];
} catch (Exception $ex) {
   $errors['category_id'] = $ex->getMessage();
}
try {
    $obj_product->brand = $_POST['brand_id'];
} catch (Exception $ex) {
   $errors['brand_id'] = $ex->getMessage();
}

try {
    $obj_product->product_image = $_FILES['product_image'];
} catch (Exception $ex) {
  $errors['product_image'] = $ex->getMessage();  
}
try {
    $obj_product->product_image2 = $_FILES['product_image2'];
} catch (Exception $ex) {
  $errors['product_image2'] = $ex->getMessage();  
}
try {
    $obj_product->product_image3 = $_FILES['product_image3'];
} catch (Exception $ex) {
  $errors['product_image3'] = $ex->getMessage();  
}
if(count($errors) == 0)
{
    try {
        $obj_product->add_product();
        $msg = "Product has been Published";
        $_SESSION['msg'] = $msg;
        $obj_product->upload_product_image($_FILES['product_image']['tmp_name']);
        $obj_product->upload_product_image2($_FILES['product_image2']['tmp_name']);
        $obj_product->upload_product_image3($_FILES['product_image3']['tmp_name']);
} catch (Exception $ex) {
        $errors['msg_err'] =$ex->getMessage();
}
    header("Location:../add_product.php");
}
else
{
    $msg = "Product has not been published";
    $_SESSION['msg'] =$msg;
    $_SESSION['errors'] = $errors;
    $_SESSION['obj_product'] = serialize($obj_product);
    header("Location:../add_product.php");
}

<?php
session_start();
require_once '../../models/admin.php';
$obj_admin = new Admin();
$errors = array();
try {
    $obj_admin->email = $_POST['email'];
} catch (Exception $ex) {
    $errors['email'] = $ex->getMessage();
}
try {
    $obj_admin->password = $_POST['password'];
} catch (Exception $ex) {
    $errors['password'] = $ex->getMessage();
}

if(count($errors) == 0)
{
    try {
        $remember = isset($_POST['remember']) ? TRUE : FALSE;
        $obj_admin->login($remember);
        header("Location:../index.php");
    } catch (Exception $ex) {
        $_SESSION['msg'] = $ex->getMessage();
        header("Location:../signin.php");
    }
}
else
{
    $msg = "*Check Your Error";
    $_SESSION['msg'] = $msg;
    $_SESSION['errors'] = $errors;
    $_SESSION['obj_admin'] = serialize($obj_admin);
    header("Location:../signin.php");
}




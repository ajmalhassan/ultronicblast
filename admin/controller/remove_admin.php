<?php
session_start();
require_once '../../models/admin.php';

if(isset($_SESSION['obj_admin']))
{
    $obj_admin = unserialize($_SESSION['obj_admin']);
}
 else {
     $obj_admin = new Admin();
}
if (isset($_GET['action'])) {
   
    switch ($_GET['action']) {
        
        case "remove_admin":
            $key = ($_GET['admin_id']);
            $obj_admin->delete_admin($key);
            break;
    }
}
$_SESSION['obj_admin'] = serialize($obj_admin);
header("Location:" . $_SERVER['HTTP_REFERER']);
<?php
session_start();
require_once '../../models/brand.php';
$obj_brand = new Brand();
$errors = array();
try {
    $obj_brand->brand_name = $_POST['brand_name'];
} catch (Exception $ex) {
    $errors['brand_name'] = $ex->getMessage();
} 
$keybra = $_GET['brandID'];
if(count($errors) == 0)
{
    try {
        $obj_brand->update_brand($keybra);
        $update_msg = "Brand is Updated";
        $_SESSION['update_msg'] = $update_msg;
        //$obj_product->upload_product_image($_FILES['product_image']['tmp_name']);
} catch (Exception $ex) {
        $errors['msg_err'] =$ex->getMessage();
}
    header("Location:../brands.php");
}
else
{
    $update_msg = "Failed";
    $_SESSION['msg'] =$update_msg;
    $_SESSION['errors'] = $errors;
    $_SESSION['obj_brand'] = serialize($obj_brand);
    header("Location:../brands.php");
}

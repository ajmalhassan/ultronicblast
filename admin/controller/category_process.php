<?php
session_start();
require_once '../../models/category.php';
$obj_category = new Category();
$errors = array();
try {
    $obj_category->category_name = $_POST['category_name'];
} catch (Exception $ex) {
    $errors['category_name'] = $ex->getMessage();
}
try {
    $obj_category->category_image_name = $_POST['category_name'];
} catch (Exception $ex) {
    $errors['category_image_name'] = $ex->getMessage();
}
try {
    $obj_category->category_image = $_FILES['category_image'];
} catch (Exception $ex) {
  $errors['category_image'] = $ex->getMessage();  
}

if(count($errors) == 0)
{
    try {
        $obj_category->add_category();
        $msg = "New Category Inserted";
        $_SESSION['msg'] = $msg;
        $obj_category->upload_category_image($_FILES['category_image']['tmp_name']);
        header("Location:../categories.php");
        $_SESSION['msg'] = $ex->getMessage();
    } catch (Exception $ex) {
        $_SESSION['msg'] = $ex->getMessage();
        header("Location:../categories.php");
    }
}
else
{
    $msg = "*Check Your Error";
    $_SESSION['msg'] = $msg;
    $_SESSION['errors'] = $errors;
    $_SESSION['obj_category'] = serialize($obj_category);
    header("Location:../categories.php");
}




<?php
session_start();
require_once '../../models/slider.php';
$obj_slider = new Slider();
$errors = array();

try {
    $obj_slider->slide_name = $_POST['slide_name'];
} catch (Exception $ex) {
    $errors['slide_name'] = $ex->getMessage();
}
try {
    $obj_slider->slide_link = $_POST['slide_link'];
} catch (Exception $ex) {
    $errors['slide_link'] = $ex->getMessage();
}
try {
    $obj_slider->slide_description = $_POST['slide_description'];
} catch (Exception $ex) {
    $errors['slide_description'] = $ex->getMessage();
}
try {
    $obj_slider->slide_image = $_FILES['slide_image'];
} catch (Exception $ex) {
    $errors['slide_image'] = $ex->getMessage();
}
$keyslide = $_GET['slideID'];
if((count($errors)) == 0)
{
    try {
        //Slider::count_slides();
        $obj_slider->update_slider($keyslide);
        $obj_slider->upload_slide_image($_FILES['slide_image']['tmp_name']);
        $msg = "update Slide";
        $_SESSION['msg'] = $msg;
        header("Location:../display_slider.php");
        
    } catch (Exception $ex) {
       header("Location:../display_slider.php");
       $_SESSION['msg'] = $ex->getMessage();
    }

}
 else {
    $msg = "* Check your Error";
    $_SESSION['msg'] = $msg;
    $_SESSION['errors'] = $errors;
    $_SESSION['obj_slider'] = serialize($obj_slider);
    header("Location:../dusplay_slider.php");
}


    


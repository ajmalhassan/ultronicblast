<?php
session_start();
require_once '../../models/category.php';
$obj_category = new Category();
$errors = array();
try {
    $obj_category->category_name = $_POST['category_name'];
} catch (Exception $ex) {
    $errors['category_name'] = $ex->getMessage();
}

if(count($errors) == 0)
{
    try {
        $keycat = $_GET['categoryID'];
        $obj_category->update_category($keycat);
        $update_msg = "$obj_category->category_name is Updated";
        $_SESSION['update_msg'] = $update_msg;
        //$obj_product->upload_product_image($_FILES['product_image']['tmp_name']);
} catch (Exception $ex) {
        $errors['msg_err'] =$ex->getMessage();
}
    header("Location:../categories.php");
}
else
{
    $update_msg = "Failed to update category";
    $_SESSION['update_msg'] =$update_msg;
    $_SESSION['errors'] = $errors;
    $_SESSION['obj_category'] = serialize($obj_category);
    header("Location:../categories.php");
}

<?php
session_start();
require_once '../../models/product.php';
$obj_product = new product();
$errors = array();
try {
    $obj_product->product_name = $_POST['product_name'];
} catch (Exception $ex) {
    $errors['product_name'] = $ex->getMessage();
}
try {
    $obj_product->product_image_name = $_POST['product_name'];
} catch (Exception $ex) {
    $errors['product_image_name'] = $ex->getMessage();
}
try {
    $obj_product->description = $_POST['product_description'];
} catch (Exception $ex) {
    $errors['description'] = $ex->getMessage();
}
try {
    $obj_product->unit_price = $_POST['unit_price'];
} catch (Exception $ex) {
    $errors['unit_price'] = $ex->getMessage();
}
try {
    $obj_product->quantity = $_POST['quantity'];
} catch (Exception $ex) {
    $errors['quantity'] = $ex->getMessage();
}
try {
    $obj_product->product_discount = $_POST['product_discount'];
} catch (Exception $ex) {
    $errors['product_discount'] = $ex->getMessage();
}
try {
    $obj_product->product_image = $_FILES['product_image'];
} catch (Exception $ex) {
  $errors['product_image'] = $ex->getMessage();  
}
$keypro = $_GET['productID'];
if(count($errors) == 0)
{
    try {
        $obj_product->update_product($keypro);
        $msg = "$obj_product->product_name Updated";
        $_SESSION['msg'] = $msg;
        $obj_product->upload_product_image($_FILES['product_image']['tmp_name']);
} catch (Exception $ex) {
        $errors['msg_err'] =$ex->getMessage();
}
    header("Location:../display_products.php");
    //echo ("<script> alert('dkfkdskf');</scropt>");
}
else
{
    $msg = "Failed";
    $_SESSION['msg'] =$msg;
    $_SESSION['errors'] = $errors;
    $_SESSION['obj_product'] = serialize($obj_product);
    header("Location:../update_product.php");
}

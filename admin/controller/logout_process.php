<?php
session_start();
require_once '../../models/admin.php';
$obj_admin = unserialize($_SESSION['obj_admin']);
try {
    $obj_admin->logout();
    $_SESSION['msg'] = "You have logged out";
} catch (Exception $ex) {
    $_SESSION['msg'] = $ex->getMessage();
}
header("Location:../msg.php");



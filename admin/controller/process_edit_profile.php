<?php
session_start();
require_once '../../models/admin.php';
$obj_admin = unserialize($_SESSION['obj_admin']);
$errors = array();
try {
    $obj_admin->admin_name = $_POST['admin_name'];
} catch (Exception $ex) {
    $errors['admin_name'] = $ex->getMessage();
}
try {
    $obj_admin->first_name = $_POST['first_name'];
} catch (Exception $ex) {
    $errors['first_name'] = $ex->getMessage();
}
try {
    $obj_admin->last_name =$_POST['last_name'];
} catch (Exception $ex) {
    $errors['last_name'] = $ex->getMessage();
}


if(count($errors) == 0)
{
    try {
        $obj_admin->update_account();
        $msg = "Update Admin Profile";
        $_SESSION['msg'] = $msg;
        //$_SESSION['msg'] = $ex->getMessage();
        header("Location:../edit_profile.php");
    } catch (Exception $ex) {
        $_SESSION['msg'] = $ex->getMessage();
        header("Location:../edit_profile.php");
    }
}
else
{
    $msg = "*Check Your Error";
    $_SESSION['msg'] = $msg;
    $_SESSION['errors'] = $errors;
    //$_SESSION['obj_admin'] = serialize($obj_admin);
    header("Location:../edit_profile.php");
}




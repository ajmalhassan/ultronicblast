<?php
session_start();
require_once '../../models/Order.php';

if(isset($_SESSION['obj_order']))
{
    $obj_order = unserialize($_SESSION['obj_order']);
}
 else {
     $obj_order = new Order();
}
if (isset($_GET['action'])) {
   
    switch ($_GET['action']) {
        
        case "remove_order":
            $key = ($_GET['order_id']);
            $obj_order->delete_order($key);
            break;
    }
}
$_SESSION['obj_order'] = serialize($obj_order);
header("Location:" . $_SERVER['HTTP_REFERER']);
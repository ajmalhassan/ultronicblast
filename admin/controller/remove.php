<?php
session_start();
require_once '../../models/product.php';
require_once '../../models/admin.php';
require_once '../../models/brand.php';
require_once '../../models/category.php';
require_once '../../models/slider.php';

if(isset($_SESSION['obj_pro']))
{
    $obj_pro = unserialize($_SESSION['obj_pro']);
}
 else {
     $obj_pro = new product();
}
if(isset($_SESSION['obj_brand']))
{
    $obj_brand = unserialize($_SESSION['obj_brand']);
}
 else {
     $obj_brand = new Brand();
}
if(isset($_SESSION['obj_cat']))
{
    $obj_cat = unserialize($_SESSION['obj_cat']);
}
 else {
     $obj_cat = new Category();
}
if(isset($_SESSION['obj_slider']))
{
    $obj_slider = unserialize($_SESSION['obj_slider']);
}
 else {
     $obj_slider = new Slider();
}

if (isset($_GET['action'])) {
    switch ($_GET['action']) {
        
        case "remove_product":
            $key = ($_GET['productID']);
            $obj_pro->delete_product($key);
            break;
        case "remove_brand":
            $key = ($_GET['brandID']);
            $obj_brand->delete_brand($key);
            break;
        case "remove_category":
            $key = ($_GET['categoryID']);
            $obj_cat->delete_category($key);
            break;
        case "remove_slide":
            $key = ($_GET['slideID']);
            $obj_slider->delete_slide($key);
            break;
    }
}
$_SESSION['obj_pro'] = serialize($obj_pro);
$_SESSION['obj_brand'] = serialize($obj_brand);
$_SESSION['obj_cat'] = serialize($obj_cat);
$_SESSION['obj_slider'] = serialize($obj_slider);
header("Location:" . $_SERVER['HTTP_REFERER']);
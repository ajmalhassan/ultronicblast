<?php
session_start();
require_once '../../models/admin.php';
$obj_admin = unserialize($_SESSION['obj_admin']);
//echo ("<pre>");
//print_r($_FILES);
//echo("</pre>");
//die;


try {
    $obj_admin->profile_image = $_FILES['profile_image'];
    $obj_admin->upload_profile_image($_FILES['profile_image']['tmp_name']);
    $msg_img = "<span style='color:green'>Profile image has been updated</span>";
    $_SESSION['msg_img'] = $msg_img;
    header("Location:../edit_profile.php");
} catch (Exception $ex) {
    $_SESSION['msg_img'] = $ex->getMessage();
    header("Location:../edit_profile.php");
}




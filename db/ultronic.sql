-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 06, 2018 at 10:54 AM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ultronic`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `admin_id` int(11) NOT NULL,
  `first_name` varchar(55) NOT NULL,
  `last_name` varchar(55) NOT NULL,
  `admin_name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `profile_image` varchar(100) NOT NULL,
  `reset_code` varchar(32) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `signup_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`admin_id`, `first_name`, `last_name`, `admin_name`, `email`, `password`, `profile_image`, `reset_code`, `is_active`, `signup_date`) VALUES
(8, 'ajo', 'hassan', 'ajmalhassan', 'ajmalhassan651@gmail.com', '55f9aaf9a436d5ced2f82428487f35c4432f41f3', 'ajmalhassan.png', '', 1, '2018-04-13 08:44:51'),
(9, 'ali', 'ali', 'ali', 'ali@gmail.com', 'e697ef18d3fa82e0fcd427a989a86c694b547c64', 'ali.png', '', 1, '2018-04-05 04:54:53'),
(10, 'umar', 'farroq', 'umar', 'umar@gmail.com', '2aef4089abde60fa99f4d361c0d46ce1c665f8bd', 'umar.png', '', 1, '2018-04-05 05:05:46'),
(13, 'asma', 'khan', 'asma khan', 'asam@gmail.com', 'a73fca507c2c79bed0104cd712800efc2bad03c1', 'asma khan.png', '', 1, '2018-04-05 14:51:01'),
(14, 'salman', 'ahmad', 'salmanahmad', 'salman@gmail.com', 'fb2628eb37e4bc3e3e121217aa1c6cc148794a85', 'salmanahmad.png', 'a0fd8d0a6f84a95f83040199b8a5c09c', 1, '2018-04-05 15:14:16'),
(15, 'Ajmal', 'hassan', 'Ajmal hassan', 'ajmalhassan651@gmail.com', '55f9aaf9a436d5ced2f82428487f35c4432f41f3', 'Ajmal hassan.png', '', 1, '2018-04-06 10:06:03'),
(16, 'sdjdksJ', 'SDKJFKA', 'ajmal', 'ajmalhassan651@gmail.com', '55f9aaf9a436d5ced2f82428487f35c4432f41f3', 'ajmal.png', '', 1, '2018-05-01 13:50:08'),
(17, 'kdsjfkjJ', 'kdjfkadj', 'admin', 'admin@gmail.com', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'admin.png', '', 1, '2018-05-01 13:51:20'),
(18, 'nawaz', 'nawaz', 'HAQ', 'HAQ@gmail.com', 'ffc10f49c841751129bf9c550a658f0c8702ea79', '.png', '', 1, '2018-05-02 08:10:17');

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `brand_id` int(11) NOT NULL,
  `brand_name` varchar(55) NOT NULL,
  `register_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`brand_id`, `brand_name`, `register_time`) VALUES
(24, 'Ugreen', '2018-06-05 08:00:53');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(55) NOT NULL,
  `category_image` varchar(255) NOT NULL,
  `register_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_id`, `category_name`, `category_image`, `register_time`) VALUES
(36, 'Mobiles', 'Mobiles.png', '2018-06-05 08:00:39'),
(37, 'Cable', 'Cables.png', '2018-06-05 19:31:12'),
(38, 'Charger', 'Charger.png', '2018-06-05 19:31:29'),
(39, 'kkjs', 'kkjs.png', '2018-06-06 10:27:42');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `date` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `product_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `comment` text NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `date`, `user_name`, `product_id`, `email`, `comment`, `status`) VALUES
(1, 1355233212, 'ajmal hassan', 80, 'ajmalhassan651@gmail.com', 'Well quality product', 'approve'),
(2, 1355233212, 'ajmal hassan', 80, 'ajmalhassan651@gmail.com', 'Well quality product', 'approve'),
(3, 1527401828, 'fkajf', 56, 'dfkajf', 'kjdfasdh', 'approve'),
(4, 1528052546, 'fkja', 79, 'abc@xyz.com', 'fkasdfl', 'approve'),
(5, 1528052869, 'sdkf', 79, 'abc@xyz.com', 'dsfksdlf', 'approve'),
(6, 1528080994, 'kfjkaj', 80, 'abc@xyz.com', 'dlfdf;aks', 'approve'),
(7, 1528089409, 'ajaml', 80, 'ajmalhassan651@gmail.com', 'ajmal', 'approve');

-- --------------------------------------------------------

--
-- Table structure for table `orderitems`
--

CREATE TABLE `orderitems` (
  `order_item_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `unit_price` int(55) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orderitems`
--

INSERT INTO `orderitems` (`order_item_id`, `order_id`, `product_id`, `unit_price`, `quantity`) VALUES
(1, 9, 78, 800, 1),
(2, 11, 78, 800, 1),
(3, 11, 79, 400, 1),
(4, 11, 77, 150, 1),
(5, 12, 78, 800, 1),
(6, 12, 79, 400, 1),
(7, 12, 77, 150, 1),
(8, 13, 79, 400, 1),
(9, 14, 78, 800, 1),
(10, 14, 77, 150, 2),
(11, 15, 75, 1200, 1),
(12, 16, 77, 150, 1),
(13, 17, 73, 2000, 1),
(14, 18, 79, 400, 1),
(15, 20, 79, 400, 1),
(16, 21, 78, 800, 1),
(17, 22, 78, 800, 1),
(18, 23, 74, 2500, 1),
(19, 23, 79, 400, 1),
(20, 24, 75, 1200, 1),
(21, 25, 80, 400, 1),
(22, 25, 79, 400, 1),
(23, 25, 73, 2000, 1),
(24, 26, 73, 2000, 1),
(25, 27, 68, 88, 1),
(26, 28, 68, 88, 1),
(27, 29, 70, 600, 1),
(28, 30, 78, 800, 1),
(29, 31, 80, 400, 1),
(30, 31, 81, 88, 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `order_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `total_quantity` int(11) NOT NULL,
  `total_price` int(11) NOT NULL,
  `address` text NOT NULL,
  `order_date` datetime NOT NULL,
  `order_status` tinyint(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `user_id`, `total_quantity`, `total_price`, `address`, `order_date`, `order_status`) VALUES
(1, 27, 0, 800, '0', '2018-05-09 04:46:38', 1),
(2, 27, 0, 800, '0', '2018-05-09 04:50:10', 1),
(3, 27, 0, 1350, 'okara pakistan', '2018-05-09 04:52:19', 1),
(4, 27, 0, 1350, 'kjdfkljaldfl', '2018-05-09 05:01:33', 1),
(5, 27, 0, 1350, 'kjfsdkfjsa', '2018-05-09 05:03:26', 1),
(6, 27, 0, 1350, 'kjfsdkfjsa', '2018-05-09 05:10:21', 1),
(7, 27, 0, 1350, 'sdjkfjkasdfjlasjd', '2018-05-09 05:18:01', 1),
(8, 27, 0, 1350, 'kdjfkasdfal', '2018-05-09 05:18:46', 1),
(9, 27, 0, 1350, 'kdjfkasdfal', '2018-05-09 05:18:58', 1),
(10, 27, 0, 1350, 'kdjfkasdfal', '2018-05-09 05:21:31', 1),
(11, 27, 0, 1350, 'ksdjfkasdfj', '2018-05-09 05:25:44', 1),
(12, 27, 0, 1350, 'kjdkgal', '2018-05-09 05:29:05', 1),
(13, 27, 0, 400, 'djslfjasljf', '2018-05-09 05:41:10', 1),
(14, 27, 3, 1100, 'kdfjkasdfj', '2018-05-09 06:59:40', 1),
(15, 27, 1, 1200, 'kldfjdfad', '2018-05-10 04:15:14', 1),
(16, 27, 1, 150, 'klsdljfasfkl', '2018-05-10 04:19:59', 1),
(17, 27, 1, 2000, 'lkdlfdlfas', '2018-05-10 04:22:03', 1),
(18, 27, 1, 400, 'kldjfldfjla', '2018-05-10 04:25:07', 1),
(19, 27, 0, 0, 'kldjfldfjla', '2018-05-10 04:27:47', 1),
(20, 27, 1, 400, 'dkljlkfasdj', '2018-05-10 04:28:28', 1),
(21, 27, 1, 800, 'klsdjflsdfa', '2018-05-10 04:31:47', 1),
(22, 27, 1, 800, 'klfjsdfjslk', '2018-05-10 04:42:11', 1),
(23, 27, 2, 2900, 'Okara pakistan', '2018-05-10 13:23:38', 1),
(24, 27, 1, 1200, 'Okara pakistan', '2018-05-16 06:02:09', 1),
(25, 27, 3, 2800, 'Okara pakistan', '2018-05-19 07:14:31', 1),
(26, 27, 1, 2000, 'Okara pakistan', '2018-05-19 07:24:15', 1),
(27, 32, 1, 88, 'dkfjasdkj', '2018-05-21 06:25:35', 1),
(28, 31, 1, 88, 'sdkjfdjklf', '2018-05-21 12:57:14', 0),
(29, 31, 1, 600, 'sdkjfdjklf', '2018-05-21 18:56:07', 1),
(30, 31, 1, 800, 'sdkjfdjklf', '2018-05-21 18:57:39', 0),
(31, 31, 2, 488, 'sdkjfdjklf', '2018-05-21 19:02:30', 1);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `productID` int(11) NOT NULL,
  `product_name` varchar(225) NOT NULL,
  `description` text NOT NULL,
  `short_description` text NOT NULL,
  `unit_price` decimal(10,2) NOT NULL,
  `sale_price` decimal(10,2) NOT NULL,
  `quantity` int(11) NOT NULL,
  `product_discount` int(11) DEFAULT '0',
  `product_features` text NOT NULL,
  `views` int(11) NOT NULL,
  `product_image` varchar(225) NOT NULL,
  `product_image2` varchar(225) NOT NULL,
  `product_image3` varchar(225) NOT NULL,
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `brandID` int(11) NOT NULL,
  `categoryID` int(11) NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`productID`, `product_name`, `description`, `short_description`, `unit_price`, `sale_price`, `quantity`, `product_discount`, `product_features`, `views`, `product_image`, `product_image2`, `product_image3`, `featured`, `brandID`, `categoryID`, `status`) VALUES
(83, 'Product 1', '<p>Good quality </p>\r\n<p>Good quality </p>\r\n<p>Good quality </p>\r\n<p>Good quality </p>\r\n<p>Good quality </p>\r\n<p>Good quality </p>\r\n<p>Good quality </p>\r\n<p>Good quality </p>\r\n<p>Good quality </p>\r\n<p>Good quality </p>', '<p>Good quality </p>\r\n<p>Good quality </p>\r\n<p>Good quality </p>\r\n<p>Good quality </p>\r\n<p>Good quality </p>\r\n<p>Good quality </p>\r\n<p>Good quality </p>\r\n<p>Good quality </p>\r\n<p>Good quality </p>\r\n<p>Good quality </p>\r\n<p>Good quality </p>\r\n<p>Good quality </p>\r\n<p>Good quality </p>\r\n<p>Good quality </p>\r\n<p>Good quality </p>\r\n<p>Good quality </p>\r\n<p>Good quality </p>\r\n<p>Good quality </p>\r\n<p>Good quality </p>\r\n<p>Good quality </p>\r\n<p>Good quality </p>\r\n<p>Good quality </p>\r\n<p>Good quality </p>\r\n<p>Good quality </p>\r\n<p>Good quality </p>\r\n<p>Good quality </p>\r\n<p>Good quality </p>\r\n<p>Good quality </p>\r\n<p>Good quality </p>\r\n<p>Good quality </p>\r\n<p>Good quality </p>', '3000.00', '2400.00', 23, 20, '', 3, 'product_1.jpg', 'product_12.jpg', 'product_13.jpg', 0, 24, 36, '');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `slide_id` int(11) NOT NULL,
  `slide_name` varchar(55) NOT NULL,
  `slide_link` varchar(100) NOT NULL,
  `slide_des` varchar(100) NOT NULL,
  `slide_image` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`slide_id`, `slide_name`, `slide_link`, `slide_des`, `slide_image`) VALUES
(29, 'slideone', 'http://www.google.com', '<p><strong>Description One</strong></p>', 'slideone.png');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(10) NOT NULL,
  `first_name` varchar(55) NOT NULL,
  `last_name` varchar(55) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `mobile_number` char(16) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(40) NOT NULL,
  `address` text NOT NULL,
  `profile_image` varchar(100) NOT NULL,
  `signup_date` datetime NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `reset_code` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `first_name`, `last_name`, `user_name`, `mobile_number`, `email`, `password`, `address`, `profile_image`, `signup_date`, `is_active`, `reset_code`) VALUES
(28, 'Ajmal', 'hassan', 'ajmal hassan', '3014812075', 'ajmalhassan651@gmail.com', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'Okara', 'ajmal hassan.png', '2018-05-21 05:00:36', 1, ''),
(29, 'abc', 'abc', 'Ajaml hassan', '3014812075', 'abc@xyz.com', 'fa6e853cacca6db7fbd380384118d41a25548c53', 'dsfsdafj', 'abc@xyz.com.png', '2018-05-21 05:32:17', 1, ''),
(30, 'sdfaj', 'ksjld', 'ksdjafjk', '3014812075', 'aaa@gmail.com', '7e240de74fb1ed08fa08d38063f6a6a91462a815', 'ksdjfjasdk', 'aaa@gmail.com.png', '2018-05-21 05:41:41', 1, ''),
(31, 'dasjfkkaf', 'asdkfja', 'ksdjafj', '3014812075', 'sss@gmail.com', 'bf9661defa3daecacfde5bde0214c4a439351d4d', 'sdkjfdjklf', 'sss@gmail.com.png', '2018-05-21 05:47:26', 1, ''),
(32, 'kldjfkJ', 'KJFJKLFJ', 'KLSDJKLFJ', '+923014812075', 'ddd@gmail.com', '9c969ddf454079e3d439973bbab63ea6233e4087', 'dkfjasdkj', 'ddd@gmail.com.png', '2018-05-21 06:06:15', 1, '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`brand_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orderitems`
--
ALTER TABLE `orderitems`
  ADD PRIMARY KEY (`order_item_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`productID`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`slide_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `brand_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `orderitems`
--
ALTER TABLE `orderitems`
  MODIFY `order_item_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `productID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;
--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `slide_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

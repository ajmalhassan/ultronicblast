<?php
require_once 'views/top.php';
require_once 'models/product.php';
?>


<body>

<div class="super_container">
	<!-- Header -->
	<header class="header trans_300">
		<!-- Top Navigation -->
		<?php
                    require_once 'views/top_nav.php';
                   // <!-- Main Navigation -->
                    require_once 'views/main_nav.php';
                ?>
	</header>

	<div class="fs_menu_overlay"></div>
<!--	<div class="hamburger_menu">-->


	<?php
                //slider
               // require_once 'views/slider.php';
                //Banner
                //require_once 'views/banner.php';
                //New Arrivals
                ?>

<div class="new_arrivals">
    <div class="container">
        <div class="row">
        <div class="col text-center">
            <div class="section_title new_arrivals_title">
                    <h2>New Arrivals</h2>
            </div>
        </div>
        </div>

<div class="row">
<div class="col">
<div class="product-grid" data-isotope='{ "itemSelector": ".product-item", "layoutMode": "fitRows" }'>

        <!--Display Product-->
        <?php
        $products = product::get_products();
        foreach ($products as $p)
        {
            echo ("
       <div class='product-item men'>
    <div class='product discount product_filter'>
        <div class='product_image'>
            <img src='".BASE_URL."admin/products/$p->product_name/$p->product_image' alt='$p->product_name'>
        </div>
        <div class='favorite favorite_left'></div>
        <div class='product_bubble product_bubble_right product_bubble_red d-flex flex-column align-items-center'><span>-$$p->product_discount</span></div>
        <div class='product_info'>
            <h6 class='product_name'><a href='".BASE_URL."detail.php?product_id=$p->productID' target='_blank'>$p->product_name</a></h6>
            <div class='product_price'>Rs.$p->sale_price<span>Rs.$p->unit_price</span></div>
        </div>
    </div>
    <div class='red_button add_to_cart_button'><a href='#'>add to cart</a></div>
</div> 
 ");
        }
        ?>

</div>
    
    
    <nav aria-label="Page navigation example">
        <ul class="pagination">
          <li class="page-item"><a class="page-link" href="#">Previous</a></li>
          <li class="page-item"><a class="page-link" href="#">1</a></li>
          <li class="page-item"><a class="page-link" href="#">2</a></li>
          <li class="page-item"><a class="page-link" href="#">3</a></li>
          <li class="page-item"><a class="page-link" href="#">Next</a></li>
        </ul>
    </nav>
</div>
</div>
    </div>
	</div>


                <?php
                //Deal of the week
                //require_once './views/weekly_deal.php';
                
                ?>
<!--Best Sellers-->
<div class="best_sellers">
<div class="container">
    <div class="row">
            <div class="col text-center">
                    <div class="section_title new_arrivals_title">
                            <h2>Best Sellers</h2>
                    </div>
            </div>
    </div>
<div class="row">
    <div class="col">
        <div class="product_slider_container">
            <div class="owl-carousel owl-theme product_slider">

                    <!-- Slides -->

<?php
$productsSlides =  product::get_products();
foreach ($productsSlides as $p)
{
     echo("
         <div class='owl-item product_slider_item'>
    <div class='product-item'>
        <div class='product discount'>
            <div class='product_image'>
                    <img src='".BASE_URL."admin/products/$p->product_name/$p->product_image' alt='$p->product_name'>
            </div>
            <div class='favorite favorite_left'></div>
            <div class='product_bubble product_bubble_right product_bubble_red d-flex flex-column align-items-center'><span>-$$p->product_discount</span></div>
            <div class='product_info'>
                <h6 class='product_name'><a href='".BASE_URL."detail.php?product_id=$p->productID' target='_blank'>$p->product_name</a></h6>
                <div class='product_price'>$$p->unit_price<span>$590.00</span></div>
            </div>
        </div>
    </div>
</div>
 "); 
}
    
?>

                
            </div>

                <!-- Slider Navigation -->

                <div class="product_slider_nav_left product_slider_nav d-flex align-items-center justify-content-center flex-column">
                        <i class="fa fa-chevron-left" aria-hidden="true"></i>
                </div>
                <div class="product_slider_nav_right product_slider_nav d-flex align-items-center justify-content-center flex-column">
                        <i class="fa fa-chevron-right" aria-hidden="true"></i>
                </div>
        </div>
    </div>
</div>
        </div>
	</div>
                <?php
                require_once 'views/news_letter.php';
                require_once 'views/footer.php';
             
        
	
	

	
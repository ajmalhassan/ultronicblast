<?php
require_once 'views/top.php';
?>
</head>

<body>

<div class="super_container">
	<!-- Header -->
	<header class="header trans_300">
		<!-- Top Navigation -->
		<?php
                    require_once 'views/top_nav.php';
                   // <!-- Main Navigation -->
                    require_once 'views/main_nav.php';
                ?>
	</header>
	<div class="fs_menu_overlay"></div>
        <br><br><br><br><br><br><br><br><br>
<div class="container">
<!--registration form-->
<form action="controller/process_forget_password.php" method="post" enctype="multipart/form-data">
    <div class="row">
        <div class="col-md-offset-3 col-md-6 form-design">
            <h3 style="color: red;">Recover Password</h3>
            <?php
            if(isset($_SESSION['obj_user'])){
                $obj_user = unserialize($_SESSION['obj_user']);
            }
            else{
                $obj_user = new User();
            }
            if(isset($_SESSION['msg'])){
                $msg = $_SESSION['msg'];
                echo("<span>$msg</span>");
                unset($_SESSION['msg']);
            }
            if(isset ($_SESSION['errors'])){
                $errors = $_SESSION['errors'];
                unset($_SESSION['errors']);
            }
            ?>
           <div class="row">
            <div class="col-md-12 form-group">
                <label>User Name</label>
                <input type="text" name="user_name" class="form-control control-form focusedInput" placeholder="User Name">
                
            </div>
               <div class="col-md-12 form-group">
                <label>Email</label>
                <input type="email" name="email" class="form-control control-form focusedInput" placeholder="example@xyz.com">
                
            </div>
                <div class="col-md-6 form-group">
                    <label>New Password</label>
                    <input type="password" name="new_password" class="form-control control-form focusedInput" placeholder="New Password">
                </div>
               <div class="col-md-6 form-group">
                    <label>Confirm Password</label>
                    <input type="password" name="password2" class="form-control control-form focusedInput" placeholder="Retype Password">
                    <?php
                    if(isset($errors['retype_password']))
                    {
                        echo($errors['retype_password']);
                    }
                    ?>
                </div>
            <div class="col-md-12 form-group">
                <input type="submit" class="btn btn-sm btn-danger" value="Submit">
                
            </div>
               <a href="<?php BASE_URL?>register.php" style="margin-left: 15px; float: left;">create account</a>
               <a href="<?php BASE_URL?>login.php" style="margin-left: 15px;">login</a>
        </div>
        </div>   
    </div>
  
</form>
</div>

	<?php
                require_once 'views/news_letter.php';
                require_once 'views/footer.php';
                
        
	
	

	
<?php
require_once 'views/top.php';
?>
</head>
<body>

<div class="super_container">

	<!-- Header -->

	<header class="header trans_300">

		<!-- Top Navigation -->

		<?php
                    require_once 'views/top_nav.php';
                    require_once 'views/main_nav.php';
                ?>

	</header>

	<div class="fs_menu_overlay"></div>

	<!-- Hamburger Menu -->

	<?php
                require_once 'views/mobile_view.php';
        ?>
<div class="fs_menu_overlay"></div>
        <br><br><br><br><br><br><br><br><br>
<div class="container">
    <?php
        if(isset($_SESSION['errors']))
        {
            $errors = $_SESSION['errors'];
            unset($_SESSION['errors']);
        }
        if(isset($_SESSION['obj_user']))
        {
            $obj_user = unserialize($_SESSION['obj_user']);
        }
        else
        {
            $obj_user = new User();
        }
    ?>
<!--registration form-->
<div>
    <form action="controller/process_login.php" method="post" enctype="multipart/form-data">
    <div class="row">
        <div class="col-lg-4"></div>
        <div class="form-design col-xs-12 col-lg-4">
            <h3 style="color: red;">Login</h3>
            <h5>
                <?php
                 if(isset($_SESSION['msg']))
                 {
                     $msg = $_SESSION['msg'];
                     echo($msg);
                     unset($_SESSION['msg']);
                 }
                ?>
            </h5>
           <div class="row">
            <div class="col-xs-12 col-md-12 form-group">
                <label>Email</label>
                <input type="email" name="email" class="form-control control-form focusedInput" placeholder="example@xyz.com">
                <span class="error-color">
                    <?php
                        if(isset($errors['email']))
                        {
                            echo($errors['email']);
                        }
                    ?>
                </span>
            </div>
               
            <div class="col-xs-12 col-md-12 form-group">
                <label>Password</label>
                <input type="password" name="password" class="form-control control-form focusedInput" placeholder="Password">
                <span class="error-color">
                    <?php
                    if(isset($errors['password']))
                    {
                        echo($errors['password']);
                    }
                    ?>
                </span>
            </div>
            <div class="col-xs-12 col-md-12 form-group">
                <label class="checkbox1"><input type="checkbox" name="remember" ><i> </i>Remember</label>                
            </div>
               
            <div class="col-xs-12 col-md-12 form-group">
                <input type="submit" class="btn btn-sm btn-danger" value="Login">
                
            </div>
               <a href="<?php BASE_URL?>register.php" style="margin-left: 15px; float: left;">create account</a>
               <a href="<?php BASE_URL?>forgetpassword.php" style="margin-left: 15px;">forget password?</a>
        </div>
        </div>   
    </div>
  
</form>
</div>
</div>

<?php
    require_once 'views/news_letter.php';
    require_once 'views/footer.php';
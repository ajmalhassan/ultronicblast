<?php
require_once 'views/top.php';
require_once 'models/user.php';
?>
</head>

<body>

<div class="super_container">
	<!-- Header -->
	<header class="header trans_300">
		<!-- Top Navigation -->
		<?php
                    require_once 'views/top_nav.php';
                   // <!-- Main Navigation -->
                    require_once 'views/main_nav.php';
                ?>
	</header>
	<div class="fs_menu_overlay"></div>
        <br><br><br><br><br><br><br><br><br>
<div class="container">
<!--registration form-->
<?php
    if(isset($_SESSION['errors']))
    {
        $errors = $_SESSION['errors'];
        unset($_SESSION['errors']);
        
    }
    if(isset($_SESSION['obj_user']))
    {
        $obj_user = unserialize($_SESSION['obj_user']);
        unset($_SESSION['obj_user']);
    }
    else
    {
        $obj_user = new User();
    }
?>
<form action="controller/process_user.php" method="post" enctype="multipart/form-data">
    <div class="row">
        <div class="col-md-offset-3 col-md-6 form-design">
            <!--<h3 style="color: red;">Create account</h3>-->
            <h5 class="error-color">
                <?php
                    if(isset($_SESSION['msg']))
                    {
                        $msg = $_SESSION['msg'];
                        echo($msg);
                        unset($_SESSION['msg']);
                    }
                ?>
            </h5>
            <h4>Please create account <small style="color: red;">It's free and always will be.</small></h4>
           <div class="row">
               
            <div class="col-md-6 form-group">
               <label>First Name</label>
               <input type="text" name="first_name" value="<?php echo($obj_user->first_name);?>" class="form-control control-form focusedInput" placeholder="First Name">
              <span>
                  <?php
                    if(isset($errors['first_name']))
                    {
                        echo ($errors['first_name']);
                    }
                  ?>
              </span>
            </div>
            <div class="col-md-6 form-group">
               <label>Last Name</label>
               <input type="text" name="last_name" value="<?php echo($obj_user->last_name);?>" class="form-control control-form focusedInput" placeholder="Last Name">
              <span>
                  <?php
                  if(isset($errors['last_name']))
                  {
                      echo ($errors['last_name']);
                  }
                  ?>
              </span>
            </div>
            <div class="col-md-12 form-group">
              <label>User Name</label>
              <input type="text" name="user_name" value="<?php echo($obj_user->user_name);?>" class="form-control control-form focusedInput" placeholder="User Name">
              <span>
                  <?php
                  if(isset($errors['user_name']))
                  {
                      echo ($errors['user_name']);
                  }
                  ?>
              </span>
            </div>
               <div class="col-md-6 form-group">
                <label>Mobile Number</label>
                <input type="text" name="mobile_number" value="<?php echo($obj_user->mobile_number);?>" class="form-control control-form focusedInput" placeholder="+92">
                <span>
                  <?php
                  if(isset($errors['mobile_number']))
                  {
                      echo ($errors['mobile_number']);
                  }
                  ?>
              </span>
            </div>
            <div class="col-md-6 form-group">
                <label>Email</label>
                <input type="email" name="email" value="<?php echo($obj_user->email);?>" class="form-control control-form focusedInput" placeholder="example@xyz.com">
                <span class="error-color">
                  <?php
                  if(isset($errors['email']))
                  {
                      echo ($errors['email']);
                  }
                    elseif (isset($errors['check_email']))
                      {
                        echo ($errors['check_email']);
                      }
                  ?>
              </span>
            </div>
            <div class="col-md-6 form-group">
                <label>Password</label>
                <input type="password" name="password" class="form-control control-form focusedInput" placeholder="Password">
                <span class="error-color">
                  <?php
                  if(isset($errors['password']))
                  {
                      echo ($errors['password']);
                  }
                  ?>
              </span>
            </div>
            <div class="col-md-6 form-group">
                <label>Confirm-Password</label>
                <input type="password" name="password2" class="form-control control-form focusedInput" placeholder="Retype-Password">
                <span class="error-color">
                  <?php
                  if(isset($errors['retype_password']))
                  {
                      echo ($errors['retype_password']);
                  }
                  ?>
              </span>
            </div>
            <div class="col-md-12">
                <label>Address</label>
                <textarea name="address" class="form-control control-form focusedInput" style="resize: none;"><?php echo($obj_user->address);?></textarea>
                <span class="error-color">
                  <?php
                  if(isset($errors['address']))
                  {
                      echo ($errors['address']);
                  }
                  ?>
              </span>
            </div>
               <div class="col-md-6 form-group form-group" style="margin-bottom: 0;">
            <label> </label>
            <input type="file" name="profile_image" value="<?php echo($obj_user->profile_image);?>">
            <span class="error-color">
                  <?php
                  if(isset($errors['profile_image']))
                  {
                      echo ($errors['profile_image']);
                  }
                  ?>
              </span>
            <p class="help-block">Only allow .png, .jpg image</p>
            </div>
            <div class="col-md-6 form-group">
                
            </div>
            <div class="col-md-6 form-group">
                <input type="submit" class="btn btn-sm btn-danger" value="Submit">
                <a href="<?php BASE_URL?>login.php" style="margin-left: 15px;">Already account?</a>
            </div>
        </div>
        </div>   
    </div>
  
</form>
</div>

	<?php
                require_once 'views/news_letter.php';
                require_once 'views/footer.php';
                
        
	
	

	
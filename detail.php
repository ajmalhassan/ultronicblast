<?php
require_once 'views/top.php';
require_once 'models/product.php';
?>
</head>

<body>

<div class="super_container">

	<!-- Header -->

<header class="header trans_300">

    <!-- Top Navigation -->

<?php
require_once 'views/top_nav.php';
require_once 'views/main_nav.php';
?>


</header>

<div class="fs_menu_overlay"></div>

	<!-- Hamburger Menu -->

<?php
                require_once 'views/mobile_view.php';
        ?>
<?php
if(isset($_GET['product_id'])){
    $id = $_GET['product_id'];
    product::view_count($id);
}
$keypro = $_GET['product_id'];
$product_detail =  product::get_detail_product($keypro);

foreach ($product_detail as $pd){
    $str = $pd->short_description;
    if (strlen($str) > 100){
        $str = substr($str, 0, 200) . '...';
    }
   
    $productpath = pathinfo($pd->product_image);
    $image_name=$productpath['filename'];
    echo("
        <div class='container single_product_container'>
    <div class='row'>
        <div class='col'>
        <div class='breadcrumbs d-flex flex-row align-items-center'>
            <ul>
                <li><a href='#'>Home</a></li>
                <li><a href='#'><i class='fa fa-angle-right' aria-hidden='true'></i>Category Name</a></li>
                <li class='active'><i class='fa fa-angle-right' aria-hidden='true'></i>$pd->product_name</li>
            </ul>
        </div>
        </div>
 </div>
       <div class='row'>
<div class='col-lg-7'>
    <div class='single_product_pics'>
        <div class='row'>
            <div class='col-lg-3 thumbnails_col order-lg-1 order-2'>
                <div class='single_product_thumbnails'>
                    <ul>
                        <li><img src='".BASE_URL."admin/products/$image_name/$pd->product_image'  alt='' data-image='".BASE_URL."admin/products/$image_name/$pd->product_image'></li>
                        <li class='active'><img src='".BASE_URL."admin/products/$image_name/$pd->product_image2' alt='' data-image='".BASE_URL."admin/products/$image_name/$pd->product_image2'></li>
                        <li><img src='".BASE_URL."admin/products/$image_name/$pd->product_image3' alt='' data-image='".BASE_URL."admin/products/$image_name/$pd->product_image3'></li>
                    </ul>
                </div>
            </div>
            <div class='col-lg-9 image_col order-lg-2 order-1'>
                <div class='single_product_image'>
                  <div class='single_product_image_background' style='background-image:url(admin/products/$image_name/$pd->product_image2)'></div>    
                </div>
            </div>
        </div>
    </div>
</div>
<div class='col-lg-5'>
<div class='product_details'>
    <div class='product_details_title'>
        <h5>$pd->product_name</h5>
        <p>$str</p>
    </div>
    <div class='free_delivery d-flex flex-row align-items-center justify-content-center'>
            <span class='ti-truck'></span><span>free delivery</span>
    </div>
    <div class='original_price'>Rs.$pd->unit_price </div>
    <div class='product_price'>Rs.$pd->sale_price</div>
        <ul class='star_rating'>
                <li><i class='fa fa-star' aria-hidden='true'></i></li>
                <li><i class='fa fa-star' aria-hidden='true'></i></li>
                <li><i class='fa fa-star' aria-hidden='true'></i></li>
                <li><i class='fa fa-star' aria-hidden='true'></i></li>
                <li><i class='fa fa-star-o aria-hidden='true'></i></li>
        </ul>
<div class='quantity d-flex flex-column flex-sm-row align-items-sm-center'>
    <form action='#' method='post'>
    <input class='btn btn-primary' type='submit' value='Add to Cart'/>

    </form>
</div>
</div>
</div>
</div>
           ");
} 
?>
</div>

<!-- Tabs -->

<div class="tabs_section_container">

<div class="container">
    <div class="row">
        <div class="col">
            <div class="tabs_container">
                <h2>Description</h2>
            </div>
        </div>
    </div>
<div class="row">
<div class="col">
    <!-- Tab Description -->
<div id="tab_1" class="tab_container active">
    <div class="row">
         <div class="col additional_info_col">
            
            <p><?php echo ($pd->description); ?></p>
            
        </div>
    </div>
</div>
    <hr>
    	<!-- Related Products -->
<?php
require_once 'views/related_products.php';
?>
	
    <h2>Reviews</h2>
     <div class="row">
         <?php
         $comments = Comment::get_comments($keypro);
         foreach ($comments as $comm){
           $comment_date = $comm->date;
//            $comment_day = $comment_date['day'];
                   
         
         ?>
<div class="col-lg-12 reviews_col">
<div class="user_review_container d-flex flex-column flex-sm-row">
   
    <div class="user">
        <div class="">
            <img class="img-circle" width="100px" height="100px" style=""  src="<?php echo($comm->profile_image);?>"/>
        </div>
    </div>
<div class="review">
    
    <div style="font-size:20px;"><?php echo($comm->user_name);?></div>
    <p><?php echo($comm->comment);?></p>
    <p style="color: #ddd"><?php echo(date('M-d-Y', $comment_date));?></p>
</div>
</div>
</div>
  <?php
         }
  ?>    
  <!-- Add Review -->
<div class="col-lg-6 add_review_col">
<?php
$_SESSION['product_id'] = $keypro;
//if(isset($_SESSION['user-id']))
//{
?>
    <div class="add_review">
        <form  id="review_form" action="controller/process_comment.php" method="post">
            <div>
                <h1>Add Review</h1>
                <input id="review_name" class="form_input input_name" type="text" name="name" placeholder="Name*" required="required" data-error="Name is required.">
                <input id="review_email" class="form_input input_email" type="email" name="email" placeholder="Email*" required="required" data-error="Valid email is required.">
            </div>
            <div>
                <textarea id="review_message" class="input_review" name="message"  placeholder="Your Review" rows="4" required data-error="Please, leave us a review."></textarea>
            </div>
            <div class="text-left text-sm-right">
                <input id="review_submit" type="submit" class="red_button review_submit_btn trans_300" value="Submit"/>
            </div>
        </form>
    </div>
    
    <?php
//}
?>
    

</div>

    </div>


</div>
    </div>
</div>

</div>


<?php
require_once 'views/news_letter.php';
?>

<!-- Footer -->
</div>
<?php
require_once 'views/footer.php';